# Networking Overview

## _**Organization of the course**_

* This course does not presuppose prior networking experience. 
***However***, networking is a very involved topic. Many students come to the course with prior networking coursework. For those students, some topics and the course structure may seem self explanatory.

* This course is designed for all students regardless of networking experience. 
Understanding a few *big ideas* of networking and a list of external references will greatly facilitate success in this module.

## _**Levels of Abstraction**_

Early in the course and throughout the course, a distinction has been made between *low* level languages such as assembly and C and *high* level languages such as Python. Using these terms (high and low), basically, categorizes programming languages by their proximity to machine code or natural human language. *Low level* languages such as Assembly are close to the instructions that the processor uses whereas *high level* languages such as Python more closely resemble human speech.

In a similar fashion, networking can be thought of at different levels of abstraction. This includes interactions with hardware on one end (i.e. pulses of electricity) and the actual human perceivable result (e.g. text in social media applications) on the other. As with the 'low' and 'high' level programming languages, there is no *absolute* guide to separating network layers.  

Commonly, and in this course, networking is discussed in terms of the [OSI model](https://www.geeksforgeeks.org/layers-of-osi-model/) and the [TCP/IP model](https://www.geeksforgeeks.org/tcp-ip-model/).  These models can be considered complementary.  


  
<img height=400 alt="The OSI Model" src="../assets/osi_model.png">



The OSI model organizes things at the most basic physical level such as copper wire to the highest level, i.e. the application layer, which is commonly how users experience data sent over the network (e.g. Napster, Lynx, Twitter, World of War Craft, Firefox, and so on).   
  
  
| Physical to <br>application | Layer<br>Name                                                    | Example                            |  
|-----------------------------|------------------------------------------------------------------|------------------------------------|  
| 1                           | [Physical](https://en.wikipedia.org/wiki/Physical_layer)         | Ethernet cable                     |  
| 2                           | [Data Link](https://en.wikipedia.org/wiki/Data_link_layer)       | Mac addresses (also some switches) |  
| 3                           | [Network](https://en.wikipedia.org/wiki/Network_layer)           | Routers                            |  
| 4                           | [Transport](https://en.wikipedia.org/wiki/Transport_layer)       | TCP / UDP                          |  
| 5                           | [Session](https://en.wikipedia.org/wiki/Session_layer)           | different sessions                 |  
| 6                           | [Presentation](https://en.wikipedia.org/wiki/Presentation_layer) | PNGs                               |  
| 7                           | [Application](https://en.wikipedia.org/wiki/Application_layer)   | Firefox                            |  
  


The OSI model can be thought of as going from the physical level to the application level or the application to the physical level. 

| Physical to <br>application | Layer<br>Name | Application to<br>Physical | Layer <br>Name |
|-----------------------------|---------------|----------------------------|----------------|
| 1                           | Physical      | 7                          | Application    |
| 2                           | Data Link     | 6                          | Presentation   |
| 3                           | Network       | 5                          | Session        |
| 4                           | Transport     | 4                          | Transport      |
| 5                           | Session       | 3                          | Network        |
| 6                           | Presentation  | 2                          | Data Link      |
| 7                           | Application   | 1                          | Physical       |  
  


The [TCP/IP model](https://www.tutorialspoint.com/OSI-vs-TCP-IP-Reference-Model) is similar and is more acceptable to some programmers and IT people in terms of accuracy and utility than the OSI Model.  Sometimes the OSI model is described as a **theoretical** model, whereas the TCP/IP model is described as a **practitioner** model.  Moreover, the TCP/IP model is concerned primarily with the TCP ([transmission control protocol](https://en.wikipedia.org/wiki/Transmission_Control_Protocol)) and IP ([Internet Protocol](https://en.wikipedia.org/wiki/Internet_Protocol)) protocols that power much of what people consider to be the Internet. 

  

The TCP/IP model has fewer layers than the OSI model.   

| OSI Layer    | TCP/IP         |
|--------------|----------------|
| Application  | Application    |
| Presentation | ""             |
| Session      | ""             |
| Transport    | Transport      |
| Network      | Internet       |
| Data Link    | Network Access |
| Physical     | ""             |

---

This unit may be taught starting at the lowest levels (i.e. physical and data link ) or beginning with higher levels (i.e. Application and Presentation). No approach is inherently better or worse than the other. 
  

# **Common topics include:***  
## Before beginning, you should be able to identify the most common networking terms listed [here](https://www.digitalocean.com/community/tutorials/an-introduction-to-networking-terminology-interfaces-and-protocols), [here](https://www.geeksforgeeks.org/basics-computer-networking/), and [here](https://www.ece.uvic.ca/~itraore/elec567-13/notes/dist-03-4.pdf)  

### Important terms to know are
- [Network](https://www.khanacademy.org/computing/code-org/computers-and-the-internet/internet-works/v/what-is-the-internet)  - connections across computers
- [Router](https://www.cisco.com/c/en/us/solutions/small-business/resource-center/networking/network-switch-vs-router.html) - translates / manages addresses across networks
- [Switch](https://www.cisco.com/c/en/us/solutions/small-business/resource-center/networking/network-switch-how.html) - directs network traffic on a network
- [IP Address](https://whatismyipaddress.com/ip-basics) - address on the internet e.g. 8.8.8.8 
- [Port](https://www.tutorialspoint.com/what-is-network-port) - different locations reserved for different means to connect to computers. For example, unencrypted webpages use port 80, encrypted web pages port 443, [ssh](https://www.digitalocean.com/community/tutorials/ssh-essentials-working-with-ssh-servers-clients-and-keys) port 22, and [more](https://web.mit.edu/rhel-doc/4/RH-DOCS/rhel-sg-en-4/ch-ports.html). 
- [Socket](https://www.geeksforgeeks.org/socket-in-computer-network/) - [sockets](https://medium.com/swlh/understanding-socket-connections-in-computer-networking-bac304812b5c)  can be most simply described as the combination of an IP address i.e. `96.126.116.118` and port i.e. `3000`   
- [Protocol](https://medium.com/@sadatnazrul/intro-to-computer-networking-and-internet-protocols-8f03710ca409) - the way in which applications speak to each other over the network. This analogous to people speaking different languages. 
- [WAN](https://www.cisco.com/c/en/us/products/switches/what-is-a-wan-wide-area-network.html) - the ***wide area networking** - most commonly describes ***the internet***. [SIPRnet](https://en.wikipedia.org/wiki/SIPRNet) is another example of a WAN.  
- [LAN](https://www.cisco.com/c/en/us/products/switches/what-is-a-lan-local-area-network.html) - a smaller, separate network such as in a household, school, or business 
- [127.0.0.1](https://medium.com/@hackersleaguebooks/theres-no-place-like-127-0-0-1-explained-1e6af9368e32) - is basically the computer's address for itself. 
- [DNS](https://www.cloudflare.com/learning/dns/what-is-dns/) - essentially the system that is used to translate hostnames, such as [attack.mitre.org](https://attack.mitre.org/) to `185.199.111.153`
- [ARP](https://www.geeksforgeeks.org/how-address-resolution-protocol-arp-works/) - translates hardware i.e. [MAC addresses](https://www.oit.uci.edu/mobile/registration/find-your-mac-address/) to IP addresses 
- [TCP](https://www.educative.io/edpresso/what-is-tcp) - the *transmission control protocol*  is the protocol most responsible for networking in what is commonly considered *the internet*. It is best known for the [three way handshake](https://www.geeksforgeeks.org/tcp-3-way-handshake-process/) that is used to ensure connectivity between sender and receiver  
- [UDP](https://www.geeksforgeeks.org/differences-between-tcp-and-udp/) - the [user datagram protocol](https://www.geeksforgeeks.org/user-datagram-protocol-udp/) is a networking protocol that doesn't not order information and does not check to see that it has been received.    
- [Octet](http://penta2.ufrgs.br/trouble/ts_ip.htm) - Standard IPv4 addresses are organized into segments of 8 bits i.e. octets.  For example `205.166.94.16` split into ***[octets](https://www.browserling.com/tools/ip-to-bin)*** is `11001101.10100110.01011110.00010000`

- [Netmask](https://www.hacksplaining.com/glossary/netmasks) - the bits used to specify how many addresses are available in a [subnet](https://www.geeksforgeeks.org/finding-network-id-of-a-subnet-using-subnet-mask/). The host ID and available client IDs [are calculated](https://networkengineering.stackexchange.com/questions/7106/how-do-you-calculate-the-prefix-network-subnet-and-host-numbers) by performing a bitwise `AND` with the subnet mask and the binary IP address.  
- [NAT](https://computer.howstuffworks.com/nat.htm) - ***Network Address Translation*** is the magic that allows the internet to work as we know it. It is the means by which one [(WAN) IP address](https://www.whatismyip.net/) can be shared i.e. [translated](https://tools.ietf.org/html/rfc1631), across a local area network. 


### It is ***recommended*** that you install and *look at* the following software / applications before beginning ###


***Windows and Linux***

- [VirtualBox](https://www.virtualbox.org/wiki/Downloads) in order to run Virtual Machines. [Host refers to the OS of your current system]  

- [ping](https://www.geeksforgeeks.org/ping-command-in-linux-with-examples/) - to check internet connectivity. It is typically installed by default

- [scapy](https://scapy.readthedocs.io/en/latest/installation.html) is a Python library that allows for easy manipulation of networking data at a low level  

**Linux**
In Linux  - try out the following commands and use `man` to take a closer look at them:
- `netstat` 
- `ip` 
- `arp`
- `route`
- `traceroute`

Also, you will want to install:
- [netcat](https://www.tecmint.com/netcat-nc-command-examples/) - the ***Swiss Army Knife*** of networking, which is typically run with `nc`
- [wireshark](https://www.wireshark.org/docs/wsug_html_chunked/ChapterIntroduction.html) - a very useful tool for visually inspecting [network packets](https://www.cloudflare.com/learning/network-layer/what-is-a-packet/) 

**Windows**
- [netcat](https://www.tecmint.com/netcat-nc-command-examples/) - the ***Swiss Army Knife*** of networking, which is typically run with `nc` is available for Windows [here](https://nmap.org/download.html)
- [wireshark](https://www.wireshark.org/docs/wsug_html_chunked/ChapterIntroduction.html) - a very useful tool for visually inspecting [network packets](https://www.cloudflare.com/learning/network-layer/what-is-a-packet/) . The Windows version is available [here](https://www.wireshark.org/download.html)
- [WSL](https://docs.microsoft.com/en-us/windows/wsl/about) - the ***Windows Subsystem for Linux*** is a very useful tool for running Linux commands in Windows. Installation instructions are [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10) 

---

# Network addressing and network traffic
- For network addressing it is necessary to understand 
  * [IPv4](https://realpython.com/python-ipaddress-module/) - basically, older (standard) Internet addresses e.g. **192.168.0.1**  
  * [IPv6](https://ipv6.he.net/) - newer addressing system that allows for *many* (1028 time ) more addresses e.g. **2001:200:dff:fff1:216:3eff:feb1:44d7**  

  For more information on IPv6 tools see [here](https://ipv6.he.net/presentations.php).
  "Available" blocks of IPv4 addresses ran out a few years ago. So industry is *slowly* transitioning to IPv6. 

---

# Sockets

[Sockets](https://www.cs.rutgers.edu/~pxk/rutgers/notes/sockets/) are how network connections are established. To get a sense of how important *sockets* are for network programming - try the command `man -k sockets` in Linux. 
This module uses the implementation of the Python [sockets library](https://realpython.com/python-sockets/) that is based on the C [sockets library](https://www.binarytides.com/socket-programming-c-linux-tutorial/)  
Regardless of language, most [socket]() programs will follow a similar flow:
 - Create a [socket](https://realpython.com/python-sockets/#reference) -frequently using [IPv4](https://docs.python.org/3/library/socket.html#socket.AF_INET) and [TCP](https://docs.python.org/2/howto/sockets.html#creating-a-socket)
 - Likely [bind](https://pythontic.com/modules/socket/bind) to a host and port   
 - [Listen](https://pythonprogramming.net/python-binding-listening-sockets/) or [send](https://pythontic.com/modules/socket/send) on that socket  
 - [send](https://stackoverflow.com/questions/21233340/sending-string-via-socket-python) and [recv](https://stackoverflow.com/questions/42415207/send-receive-data-with-python-socket) data
 - [close](https://stackoverflow.com/questions/409783/socket-shutdown-vs-socket-close) the socket when done  

 In addition to [tcp](https://docs.python.org/3/library/socket.html#socket.SOCK_STREAM), [udp](https://wiki.python.org/moin/UdpCommunication) will also be used with [sockets](https://pymotw.com/2/socket/udp.html) 

---

## OSI Layer 2 - data encapsulation
In the OSI Model, [Layer 2](https://osi-model.com/data-link-layer/) is the lowest level above hardware. It is referred to as the [data link](https://en.wikipedia.org/wiki/Data_link_layer) layer.
This layer is important, because it is where hardware values i.e. [MAC](https://en.wikipedia.org/wiki/MAC_address) addresses are pointed at by [ARP](https://stackoverflow.com/questions/35348012/make-arp-request-on-python), the [Address Resolution Protocol](https://en.wikipedia.org/wiki/Address_Resolution_Protocol).  It is the first level of [encapsulation](https://medium.com/@davidlares/raw-sockets-with-python-sniffing-and-network-packet-injections-486043061bd5) for network traffic.  


As Layer 2 is such a low level, i.e. sending with a [layer 2 frame](https://iplab.naist.jp/class/2018/materials/hands-on/layer-2-raw-socket/), it is important to recognize differences in data [ordering](https://www.gta.ufrj.br/ensino/eel878/sockets/htonsman.html) e.g. [endianness](https://pythontic.com/modules/socket/byteordering-coversion-functions) in network transmissions at this level.  

Layer 2 is immensely important to understand for [ARP Spoofing](https://www.tutorialspoint.com/python_penetration_testing/python_penetration_testing_arp_spoofing.htm) and [reverse ARP](https://stackoverflow.com/questions/33526112/how-to-send-a-reverse-arp-using-scapy)  

---

## OSI layer 3 - Routing and Routing Protocols

[Layer 3](https://python.astrotech.io/network/transport/osi-model.html) in the OSI Model is commonly referred to as the [Network Layer](https://en.wikipedia.org/wiki/Network_layer). This is the layer at which [packets](https://en.wikipedia.org/wiki/Network_packet) are sent i.e. [routed](https://python.astrotech.io/network/transport/routing.html) to separate machines and networks. 
It is possible to [craft](https://pypi.org/project/packet/) a [packet](https://stackoverflow.com/questions/5448277/creating-a-ipv4-packet-header-in-python) from [scratch](https://null-byte.wonderhowto.com/how-to/create-packets-from-scratch-with-scapy-for-scanning-dosing-0159231/) using [Python](https://medium.com/@NickKaramoff/tcp-packets-from-scratch-in-python-3a63f0cd59fe). 

[ICMP](https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol), familiar through [ping](https://www.dnsstuff.com/ping-monitoring-tools#:~:text=Linux%20users%20can%20open%20the,%5Binsert%20IP%20address%5D.%E2%80%9D), is used at this level. 
Similarly, [NAT](https://networkengineering.stackexchange.com/questions/3145/what-layer-of-the-osi-model-does-nat-work), i.e. the magic that makes networks possible, also occurs at this level. 

---  

## OSI Layer 4 - TCP & UDP
[Layer 4](https://sites.google.com/site/osimodellayers/layer-4---transport), described as the [transport layer](https://en.wikipedia.org/wiki/Transport_layer), is where much of network communication happens. It is at this level that [TCP](https://www.ionos.com/digitalguide/server/know-how/introduction-to-tcp/) and [UDP]() make *internet* networking possible. 
When working with layer 4, it is important to understand TCP's [three-way handshake](https://en.wikipedia.org/wiki/Handshaking#TCP_three-way_handshake) for ensuring that data was received i.e.
 - [syn](https://www.sciencedirect.com/topics/computer-science/three-way-handshake) - a *synchronize* message is sent to the server
 - [syn-ack](https://www.guru99.com/tcp-3-way-handshake.html) - the server acknowledges that message 
 - and [ack](https://blogs.akamai.com/sitr/2019/07/anatomy-of-a-syn-ack-attack.html) - the client *acknowledges and a connection is established  

A ***[syn flood](https://en.wikipedia.org/wiki/SYN_flood)*** is one of the most common types of cyber attacks.  

---  

## OSI [Layer 7](https://fcit.usf.edu/network/chap2/chap2.htm) - DNS, [HTTP](https://superuser.com/questions/984182/what-are-the-detailed-osi-model-steps-involved-in-connecting-to-a-website), SMTP, DHCP, and friends.
[Layer 7](https://www.cloudflare.com/learning/ddos/what-is-layer-7/) is the [application layer](https://en.wikipedia.org/wiki/Application_layer) of the OSI model. This is where many networking applications are defined. 

Among the key networking applications it is important to have at least a basic understanding of:
- [DNS](https://en.wikipedia.org/wiki/Domain_Name_System) - The *Domain Name System* is essentially what translates domain names to IP addresses. [dig](https://www.tecmint.com/10-linux-dig-domain-information-groper-commands-to-query-dns/) is a common tool used to look up [DNS information](https://en.wikipedia.org/wiki/List_of_DNS_record_types). 
- [HTTP](https://www.cloudflare.com/learning/ddos/glossary/hypertext-transfer-protocol-http/) - *Hyper Text Transport Protocol* the protocol used to communicate across servers and clients for the *World Wide Web*.  
- [SMTP](https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol) - The *Simple Mail Transfer Protocol* was developed and is used for email  
- [DHCP](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol) - *Dynamic Host Configuration Protocol* is used to assign IP addresses from a ***DHCP Server***,e.g. a router, to a ***DHCP Client***, e.g. a laptop, on a local area network. 

---  


## Cryptography 
[Cryptography](https://docs.python-guide.org/scenarios/crypto/) is used to make readable texts / information not immediately readable to the casual observer. Information is *encrypted* and *decrypted* using different types of processes and keys.  
Some of the most basic and common approaches to cryptography include:
- [Caesar cipher](https://en.wikipedia.org/wiki/Caesar_cipher) - basically a shifting of letters. For example, a->c, b->d, c->e, etc. would be one of the most basic types of [Caesar Cipher](https://www.tutorialspoint.com/cryptography_with_python/cryptography_with_python_caesar_cipher.htm). [rot 13](https://en.wikipedia.org/wiki/ROT13) is a *very* common Caesar Cipher used in [Linux](http://manpages.ubuntu.com/manpages/bionic/man6/caesar.6.html). 
- [Base 64](https://en.wikipedia.org/wiki/Base64) - is designed to translate binary data to ASCII i.e. plain text. It has the advantage that it can be used for [basic encryption](https://www.tutorialspoint.com/cryptography_with_python/cryptography_with_python_base64_encoding_and_decoding.htm). 
- [xor](https://www.geeksforgeeks.org/xor-cipher/) - Similarly, ***exclusive OR*** is a binary logical process that can be used for encryption. Namely, a key is selected and then all data are encrypted through `XOR` by that key - and decrypted through the reverse process.    

---  

## Advanced Network Programming Functions
Progressing with networking, certain topics will become increasingly important.
[Threading and concurrency](https://realpython.com/python-concurrency/) - i.e. the ability to maintain multiple connections to different hosts simultaneously becomes important

[struct](https://stackoverflow.com/questions/14620632/python-socket-sending-structhaving-c-stuct-as-example) - more advanced programming will require defining and specifying what types of data are being sent and how they are sent 

[JSON](https://www.w3schools.com/whatis/whatis_json.asp) - [JSON](https://docs.python.org/3/library/json.html) is one of the most common methods for transmitting data structures, similar to databases, as plain text. 

---  

  
If you would like additional practice with Networking, [NetAcad](https://www.netacad.com/portal/web/self-enroll/c/course-1003736) offers a free into course to packet tracer. 

---
### The overarching goals of this unit are:

* Perform socket programming for TCP and UDP in Python without supervision.
* Understand and explain fundamental concepts of networking, at both a high level and byte level.
* View network traffic and explain what is occurring at a high level and at a byte level.
* Be able to independently gather missing information from proper sources to solve a \(socket programming\) problem.
* Be able to analyze and/or debug networking issues with Wireshark, Netcat, and Python in the absence of information.

## _**Environment**_

* The majority of the labs will use IPv4 using Python and BSD Sockets.
* We will also cover IPv6, raw sockets, and socket programming in C throughout the course.
* We will be using multiple Linux Virtual Machines to simulate a network environment.
  * \(Windows does not support BSD Sockets.\)
* If you need assistance with Linux, let us know during the labs.

#### To access the Basic Networking information slides please click [here](slides)
