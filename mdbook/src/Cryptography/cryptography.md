# About cryptography
The term crypto has become overloaded recently with the introduction of all currencies, such as Bitcoin, Ethereum, and Litecoin. When we refer to crypto as a form of protection, we are referring to the concept of cryptography applied to communication links, storage devices, software, and messages used in a system. Cryptography has a long and important history in protecting critical systems and sensitive information.

During World War II, the Germans used Enigma machines to encrypt communications, and the Allies went to great lengths to crack the encryption. Enigma machines used a series of rotors that transformed plaintext to ciphertext, and by understanding the position of the rotors, the Allies were able to decrypt the ciphertext into plaintext. This was a momentous achievement but took significant manpower and resources. Today it is still possible to crack certain encryption techniques; however, it is often more feasible to attack other aspects of cryptographic systems, such as the protocols, the integration points, or even the libraries used to implement cryptography.

Cryptography has a rich history; however, nowadays, you will come across new concepts, such as blockchain, that can be used as a tool to help secure the IoT. Blockchain is based on a set of well-known cryptographic primitives. Other new directions in cryptography include quantum-resistant algorithms, which hold up against a theorized onslaught of quantum computers and quantum key distributions. They use protocols such as BB84 and BB92 to leverage the concepts of quantum entanglement and create good-quality keys for using classical encryption algorithms.

---

# Caesar Cipher

 Creating a Caesar encryption.

What is Caesar Cipher?

In cryptography, Caesar cipher is one of the simplest and most widely known encryption techniques. It is also known with other names like Caesar’s cipher, the shift cipher, Caesar’s code or Caesar shift. This encryption technique is used to encrypt plain text, so only the person you want can read it. The method is named after Julius Caesar, who used it in his private correspondence.

In this encryption technique, to encrypt our data,  we have to replace each letter in the text by a some other letter at a fixed difference. Let’s say, there is a letter ‘T’ then with a right shift of 1 it will be ‘U’ and with a left shift of 1 it will become ‘S’.  So here, the difference is 1 and the direction will also be same for a text. Either we can use left shift or right, not both in same text.



## Example

Suppose we have text “the crazy programmer” to be encrypted. Then what we can do is replace each of letter present in the text by a another letter having fixed difference. Lets say we want right shift by 2 then each letter of the above text have to replaced by the letter, positioned second from the letter.
```
Plaintext: the crazy programmer

Ciphertext: vjg etcba rtqitcoogt
```
Now user can’t  read this text until he/she have the decrypt key.  Decrypt key is nothing just the knowledge about how we shifted those letters while encrypting it. To decrypt this we have to left shift all the letters by 2.

That was the basic concept of Caesar cipher.

---

# Python Bitwise XOR (^) Operator
XOR (eXclusive OR) returns 1 if one operand is 0 and another is 1. Otherwise, it returns 0.

##  Python Bitwise Operators – XOR Operators
```
0^0	0
0^1	1
1^0	1
1^1	0
```
Let’s take a few examples.
```
>>> 6^6
```
Here, this is the same as 0b110^0b110. This results in 0b000, which is binary for 0.
```
>>> 6^0
6
```
This is equivalent to 0b110^0b000, which gives us 0b110. This is binary for 6.
```
>>> 6^3
5
```
Here, 0b110^0b011 gives us 0b101, which is binary for 5.

---

# Base64

Python’s Base64 module provides functions to encode binary data to Base64 encoded format and decode such encodings back to binary data.

It implements Base64 encoding and decoding as specified in RFC 3548.

 ## Demonstrate how to perform Base64 encoding in Python.

### Python Base64 Encoding Example
You can use the b64encode() function provided by the base64 module to perform Base64 encoding. It accepts a bytes-like object and returns the Base64 encoded bytes -
```python
import base64

data = "abc123!?$*&()'-=@~"

# Standard Base64 Encoding
encodedBytes = base64.b64encode(data.encode("utf-8"))
encodedStr = str(encodedBytes, "utf-8")

print(encodedStr)
```

###  Output
```
YWJjMTIzIT8kKiYoKSctPUB+
```

---

# Crypto Challenges

## Challenge 1 – the Caesar cipher
Your challenge is to decipher this string: MYXQBKDEVKDSYXC

## Challenge 2 – base64

Decode this: VGhpcyBpcyB0b28gZWFzeQ==



## Challenge 2.1 - base64

Decode this: VWtkc2EwbEliSFprVTBJeFl6SlZaMWxUUW5OaU1qbDNVSGM5UFFvPQo=
             

Hint: several rounds of Base64 were used. 

## Challenge 2.2  Convert hex to base64
The string:

49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d

Should produce:

I'm killing your brain like a poisonous mushroom


## Challenge 3 – XOR

### The first challenge is here:

Decipher this: kquht}

Key is a single digit
*The digit is type string

###  Here's a longer example that is in a hexadecimal format:

Decipher this string: 70155d5c45415d5011585446424c

Key is two digits of ASCII 
*The digits are type string

---
