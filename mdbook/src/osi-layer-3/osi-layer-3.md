## OSI Layer 3
    - KSATS(A0630, A0631, A0632, K0071, K0072, K0081, K0082, K0087, K0088, K0615)

### Overview

* Introduction to Internet Protocol
* Layer 3 Devices
* IPv4 Addresses
* IPv4 Address Classes
* Subnet Mask
* CIDR
* Routing Protocols
* Routing 
* IPv4
* NAT and PAT
* IPv6
* IPv6 Addresses
* IPv6 Address Types
* Unicast
* Multicast
* Anycast
* NDP
* ICMP
* Ping
* ICMP Errors
* ICMP Header
* ARP
* ARP Header
* RARP
* Scapy Library

### Objectives

* Understand Unicast/Multicast/Broadcast/Anycast and explain their differences
* Relate Switching/Routing concepts
* Apply a fundamental understanding of IPv4 Addressing
* Have a fundamental understanding of CIDR Notation
* Apply an understanding of IPv6 Addressing
* Implement data interchange formats such as JSON, Struct libraries \(Python\)
* Implement and manipulate packets with the utilization of the Scapy Library

---
#### To access the OSI layer 3 slides please click [here](slides)

---

## Intro to Internet Protocol

Ethernet addressing is limited to networks sharing the same physical medium \(i.e. the broadcast domain\). In order to connect multiple distinct networks, a new technology is needed.

The Internet Protocol \(IP\) is the Layer 3 protocol that we use to address hosts and route traffic across networks.

There are two main types, IPv4 and IPv6.

IPv4 was adopted by IETF in 1981. IPv4 is the common version of IP we are used to dealing with.

Initially, every device was to have it's own IPv4 address.

The explosion of the Internet dramatically increased the number of connected devices so IPv4 was modified slightly to curb address exhaustion.

These modifications included NAT, PAT, and subnetting.

The supply of IPv4 has been exhausted \(most recently North America in Sept 2015\).

---

## Layer 3 Devices

The Network Layer consists of hardware primarily devoted to Routing packets.

**Routers** -  networking device that forwards data packets between computer networks.  A packet is typically forwarded from one router to another router through the networks that constitute an internetwork until it reaches its destination node. Routers contain microprocessors that "make decisions" where to send packets based on the Ethernet \(MAC\) Address and how it's configured.

**Bridge-Routers \(Brouters\)** -  Network devices that work as a bridge and as a router. The Brouter routes packets for known protocols and simply forwards all other packets as a bridge would.

**Layer 3 Switches** - Designed for hardware based switching which is solely based on \(destination\) IP address stored in the header of IP datagrams.  Layer forwarding is performed by specialized ASICs – it is faster than routers, but they usually lack some of the advanced functionalities of routers.

---

## IPv4 Addresses

IPv4 Addresses are represented in “dotted decimal” notation.

IPv4 addresses are 32 bits, so each decimal number is represented by a single byte.

You can represent an IP using an unsigned 32 bit data type..

There are 2^8 possible values for an octet ranging from 0-255.

In total, based on the quad-dotted notation there are approximately **4.3 billion** possible IPv4 combinations. However, almost 600 million IPv4 addresses have been reserved for special purposes, allowing approximately **3.7 billion** left for general usage.

In the Internet addressing architecture, the Internet Engineering Task Force \(IETF\) and the Internet Assigned Numbers Authority

\(IANA\) have reserved various Internet Protocol \(IP\) addresses for special purposes.

IPv4

![](../assets/ipv4.PNG)

IPv6

![](../assets/ipv6.PNG)

---

## IPv4 Address Classes

**Class A** - first bit is 0 \(First octet 1-127\)

**Class B** - first TWO bits are 10 \(First octet is 128-191\)

**Class C** - first THREE bits are 110 \(First octet is 192-223\)

**Class D** - first FOUR are 1110 \(First octet 224-239\) Used only for multicasting and thus no network/host specified

**Class E** - The following IPs 240.0.0.0 to 255.255.255.254. Used for experimentation only. No network/host specified

![](../assets/ip_address_range.png)

---


## Subnet Mask

Subnet masks were introduced to identify the **NETWORK** and **HOST** bits of an IP.

Subnet masks are a special kind of IPv4 address used in tandem with a normal IPv4 addresses to provide that context.

They are used when configuring interfaces of network devices \(PC's, Routers, etc\). They are NOT transmitted with regular network traffic.

Subnet masks are a contiguous block of 1's followed by any remaining bits set to contiguous 0's. The contiguous bits may span several octets.

Mixing 1's and 0's together is not allowed. This means that the only valid numbers in a subnet mask are: \(0, 128, 192, 224, 240, 248, 252, 254, 255\)

`255 .255 .248 .0`

`11111111 .11111111 .11111000 .0`

All bits in the IP address that are masked with 1's determine the NETWORK, and all 0's determine the HOST

## Class A Subnets

![](../assets/subnet.png)

If you bitwise AND an IP and a subnet mask, you will get the network IP.

### **IP:**    172.16.237.18

### **SM:**   255.255.248.0

### **IP:**                `10101100.00010000.11101 | 101.00010010`

### **SM:**              `11111111.11111111.11111 | 000.00000000`

### **Network IP:** `10101100.00010000.11101 | 000.00000000`

### Network IP: 172.16.232.0

## Class B Subnets

![](../assets/subnetb.png)

## Class C Subnets

![](../assets/subnetc.png)

---


## CIDR

CIDR notation is a quick way of writing a subnet mask that allows for advanced networking.

After the IP address put a slash followed by the number of network bits.

192.168.1.2 with a mask 255.255.224.0 = 11111111 .11111111 .11100000 .00000000

There are 19 contiguous 1's, so the CIDR notation is 192.168.1.2/19.

CIDR notation is a compact representation of an IP address and its associated routing prefix.

---


## Routing Protocols

#### Routers operate on Layer 3.

They ignore Layer 2 addresses for decision making.

* Multiple Collision Domains
* Multiple Broadcast Domains

Routing protocols allow neighboring routers to collaborate dynamically.

There are two main types: distance-vector and link-state. 

**Interior gateway protocols type 1**, _**link-state**_ routing protocols, such as OSPF and IS-IS

**Interior gateway protocols type 2**, _**distance-vector**_ routing protocols, such as Routing Information Protocol, RIPv2, IGRP and EIGRP.

**Distance-vector** protocols attempt to calculate the "distance" between networks. Usually this is the total number of hops, or the sum of all weights on a path.

**Link-state** protocols are more concerned with speed and current state of the connections. A longer path with a faster travel time will be prioritized over a path with less hops.

Hybrid protocols also exist.

Each routing protocol defines the metric\(s\) used to calculate _weights_.

The _**“best”**_ weight is used by a router to decide where to send the packet. Weights are usually represented as an integer, and the lowest number is considered best.

**Weights** are calculated individually on each router for each known network. The weight for the exact same network may be different on different routers.

Routers have routing tables that map a **NETWORK**, **WEIGHT**, and the **NEXT HOP ADDRESS**.

Routing tables are populated by the information exchanges dictated by the specific routing protocol being used on that router and it's neighbors.

* Neighbors advertise what NETWORKS they know about, and their WEIGHTS.
* The NEXT HOP will either be the IP of the router that advertised the best path to a destination, OR it will be the locally connected network.

Static routes may also be set, with arbitrary weights, by a network administrator.  

---


## Routing

Packets are routed independently of each other, even if they are to the same destination.

Traffic destined for a private IP address will not be routed onto the public internet without special configurations.

Broadcast traffic will not be routed outside of that network.

When a router receives a packet, the destination IP of a packet is compared to the networks in the routing table.

* If the network is directly connected, send the packet to that network.
* If the network is known, but not connected, send it to the NEXT HOP specified in the routing table with the 'best' weight.
* Otherwise send it to the DEFAULT GATEWAY.

---


## IPv4

![](../assets/ethernet-frame-explained.png)

* Version - Set to 4 for IPv4
* Header Length – Size of IP header
* Differentiated Services Code Point \(DSCP\) – Formerly “Type of Service \(TOS\)”, it was redefined by RFC 2474. Used for new technologies that require real time data streamed over the network. We will not deal with this in this class
* Explicit Congestion Notification – Formerly part of TOS, used to indicate network congestion.

##       _\(NOTE: This is not the same as TCP's congestion handling\)_

* Total Length – Size of packet in bytes, including header and data
* Identification – Identifies a group of IP fragments
* Flags – Fragment flags

Bit Indicator RFC 791 Definition

0xx Reserved

x0x May Fragment

x1x Do Not Fragment

xx0 Last Fragment

xx1 More Fragments

from: [http://www.wildpackets.com/resources/compendium/tcp\_ip/ip\_fragmentation](http://www.wildpackets.com/resources/compendium/tcp_ip/ip_fragmentation)

* Fragment offset – offset of fragment from original packet
* Time to Live – Decremented first thing at each hop, packet is discarded when TTL is 0
* Protocol – Protocol used in data portion
* Header checksum – Error Checking
* Source / Destination IP – Do not change during routing

IPv4 has the ability for optional headers, but they are typically not used.

Seeing them in IPv4 is worth an investigation if you are an analyst/admin.

![](../assets/irlvy.jpg)

---

## NAT and PAT
## NAT - Network Address Translation

* Modifies an address inside your network, to a global IP \(may be single address or from a pool\).
* Used to conserve IPv4 addresses.

## PAT - Port Address Translation

* Maps public IP to port number for each device on the network and translates traffic in router.
* Used to conserve IPv4 addresses.


---

## IPv6

IPv6 is the latest version. It returns to the cleaner design of IP that IPv4 had before attempts to curb exhaustion happened.

Differences from IPv4:

* Back to single address per host, address translation is no longer needed
* Multicasting, QoS, IPSec, and Encryption is built in
* IPv6 can auto-configure itself in a local network

![](../assets/ip4-vs-ip6.png)

Version \(4 bits\) – Set 6 for IPv6.

Traffic Class \(8 bits\) – Categorizes traffic for QoS purposes. ECN is last 3 bits..

Flow Label \(20 bits\) – Request special handling by routers. May not be honored.

Payload Length \(16 bits\) – Size of payload in OCTETS. Including any extension headers.

Next Header \(8 bits\) – Layer 4 header of payload. Uses same values as IPv4's protocol field.

Hop Limit \(8 bits\) – Same as TTL.

Source / Destination \(128 bits\) - Do not change during routing.

#### IPv6 Headers are identified by number. They are not required to be used, however they provide useful features that were absent or poorly implemented in IPv4.

0 = Hop-by-Hop Options - Options that need to be examined by all devices on the path.

60 = Destination Options \(before routing header\) - Options that need to be examined only by the destination of the packet.

43 = Routing - Methods to specify the route for a datagram \(used with Mobile IPv6\).

44 = Fragment - Contains parameters for fragmentation of datagrams.

51 = Authentication Header \(AH\) - Contains information used to verify the authenticity of most parts of the packet.

50 = Encapsulating Security Payload \(ESP\) - Carries encrypted data for secure communication.

135 = Mobility \(currently without upper-layer header\) - Parameters used with Mobile IPv6.

---

## IPv6 Addresses

IPv6 addresses are 128 bits long and represented by groups of 1-4 hex characters separated by a **:** \(aka a "hextet“, or more formally a “hexadectet”\).

* 2001:0db8:0000:0000:0000:ff00:0042:8329

Using the recommendations in RFC5952 section 4.2 we can shorten that IPv6 address.

Leading 0's in a grouping may be omitted.

* 2001:db8:0:0:0:ff00:42:8329

It may contain at MOST a single :: which represents the largest contiguous block \(longer than 16 bits\) of 0's in the address

* 2001:db8::ff00:42:8329 

If multiple contiguous blocks are the same length, shorten the left most one

* 2001:0:0:AAAA:0:0:B:CC becomes 2001::AAAA:0:0:B:CC

If the contiguous 0's only encompass a single hextet, do NOT replace it with a ::

* The 2nd hextet in A:0:BBBB:CCC:D0:22:FF:4412 should never be reduced with a ::

The Layer 2 \(Ethernet\) multicast address for IPv6 is 33:33:00:00:00:01

---


## IPv6 Address Types

Generally there are three types of addresses:

* Unicast
* Multicast
* Anycast

There are also several scopes:

* Global \("Public" in IPv4\)
* Site Local \("Private" in IPv4\)
* Link Local

There is no IPv6 broadcast address. Multicast addresses fulfill that role.

The type of address is specified by the value of the first hextet.

| IPv6 | Description | IPv4 Equivalent |
| :--- | :--- | :--- |
| ::/128 | Uspecified/Default | 0.0.0.0 |
| ::1/128 | Loopback | 127.0.0.1 |
| fec0::/7 | Site Local, not routed | 10.0.0.0/8, 192.168.0.0/16 |
| fe80::/10 | Link Local, not routed | 169.254.0.0/16 |
| ff00::/8 | Multicast | Class D |
| 2001::/32 | Teredo, Allows IPv6 over IPv4 | n/a |
| 2002::/16 | 6 to 4, Allows IPv6 over IPv4 | n/a |

NOTE: This list is not exhaustive.

[http://www.iana.org/assignments/ipv6-multicast-addresses/ipv6-multicast-addresses.xhtml](http://www.iana.org/assignments/ipv6-multicast-addresses/ipv6-multicast-addresses.xhtml)

---

## Unicast

A global unicast address can be broken down as follows:

![](../assets/unicast.png)

[https://mrncciew.com/2013/04/05/ipv6-basics/](https://mrncciew.com/2013/04/05/ipv6-basics/)

---


## Multicast

## A global multicast address can be broken down as follows:

![](../assets/ipv6multicast.png)

[https://mrncciew.com/2013/04/05/ipv6-basics/](https://mrncciew.com/2013/04/05/ipv6-basics/)

[http://www.txv6tf.org/wp-content/uploads/2013/07/Martin-IPv6-Multicast-TM-v3.pdf](http://www.txv6tf.org/wp-content/uploads/2013/07/Martin-IPv6-Multicast-TM-v3.pdf)

| Address | Scope | Meaning |
| :--- | :--- | :--- |
| FF01::1 | Node-Local | All Nodes |
| FF01::2 | Node-Local | All Routers |
| FF02::1 | Link-Local | All Nodes |
| FF02::2 | Link-Local | All Routers |
| FF02::5 | Link-Local | OSPFv3 Routers |
| FF02::6 | Link-Local | OSPFv3 DR Routers |
| FF02::1:FFXX:XXXX | Link-Local | Solicited Node |

NOTE: This list is not exhaustive.

---


## Anycast

Anycast addressing does not operate like traditional addressing.

Anycast addresses are in the global unicast range. The same global unicast address is assigned to multiple interfaces \(perhaps on several different nodes\).

Traffic destined to the anycast address will be delivered to the "nearest" host with that address. It's a trick to rely on a routing protocol which determines the "closest" host that was configured with the address.

Don't use an anycast address as a source address.

This seems strange. Why do it?

Some services are run on multiple servers \(DNS or NTP for example\). You would assign anycast addresses where multiple servers provide the same service but only one reply is required to use the service.

---

## NDP

Neighbor Discovery Protocol is a collection of ICMPv6 messages for auto configuration, discovery, and awareness.

NDP uses the ICMP Header format. If you compare the ICMP header to that of the RFC4861 you will notice similarities.

Read the RFC! It tells you how to fill out values in previous headers!

**Neighbor Solicitations** - Map a known IPv6 address to a layer 2 address

**Neighbor Advertisements** - Sent in response to a Neighbor Solicitation or you can broadcast your own layer 2 address to the network unsolicited

**Router Solicitation** - Request a Router Advertisement

**Router Advertisements** - Sent in response to a Router Solicitation. It basically says here are all networks I know about.

* An RA also tells you if the sender is the first hop for that network, or if the network is actually considered "on-link" \(i.e. directly connected because multiple prefixes on the same LAN\)

**Redirect** - If you see traffic destined somewhere, and you know a better route to a given host you can send this to have the sender redirect their traffic

---


## ICMP

The Internet Control Message Protocol is a supporting or "helper" protocol in the Internet protocol suite. It is used by network devices, including routers, to send error messages and operational information indicating, for example, that a requested service is not available or that a host or router could not be reached.

Many messages have been deprecated, reserved, or are obsolete.

Each message has a Type, and each type may have several Codes.

Type 0 Echo Reply:

* Ping

Type 3 Destination Unreachable:

* Code 0 Destination Network Unreachable
* Code 1 Destination Host Unreachable
* Code 3 Destination Port Unreachable \(NOTE: See Layer 4 for slight modification regarding TCP\)

Type 8 - Echo Request:

* Ping

Type 11 Time Exceeded:

* Code 0 TTL Exceeded
* Code 1 Fragment Reassembly Time Exceeded

Type 30 Traceroute:

* Deprecated

---

## Ping

Ping is ubiquitous. It shows the connectivity status between two hosts.

It is an example of ICMP used for information purposes.

Host A initiates a request to Host B:

* ICMP Type 8

Host B receives it, and sends a response to A:

* ICMP Type 0

If the packet cannot be delivered to a host, an ICMP message is generated and returned to the sender.

The ICMP Type and Code specifies the error.

Part of the original packet is also included to help the sender know which transmission caused the error.

---

## ICMP Errors

Below, I've captured some anecdotal reasons you will see some common ICMP errors. They are NOT absolutes, but just things to think about should you encounter them.

**Network unavailable typically means there is no route to the network.**

**Host unreachable is usually one of two reasons:**

* Layer 1 is broken, host physically offline.
* A security feature is preventing access to the host \(firewall, proxy, etc\).

**Port Unreachable is usually one of two reasons:**

* A security feature is preventing access to that port \(firewall, proxy, etc\).
* A service is likely not running on that port\*

**TTL Exceeded**

* Something is unusual about the route to the host that makes the path quite long.

**Reassembly Time Exceeded**

* I have never encountered this, however a severely degraded connection with multiple varying MTUs could theoretically cause this.

---

## ICMP Header

![](../assets/icmp_packet.png)

![](../assets/1mport.png)

---


# Address Resolution Protocol

![](../assets/arpdiscovery.png)

ARP: [https://tools.ietf.org/html/rfc826](https://tools.ietf.org/html/rfc826)

EtherType: [http://www.iana.org/assignments/ieee-802-numbers/ieee-802-numbers.xhtml](http://www.iana.org/assignments/ieee-802-numbers/ieee-802-numbers.xhtml)

Address Resolution Protocol \(ARP\) is a protocol for mapping an Internet Protocol address \(IP address\) to a physical machine address that is recognized in the local network. 

For example, in IP Version 4 an address is 32 bits long. In an Ethernet local area network, however, addresses for attached devices are 48 bits long \(The physical machine address is also known as a Media Access Control or MAC address\). A table, usually called the ARP cache, is used to maintain a correlation between each MAC address and its corresponding IP address. ARP provides the protocol rules for making this correlation and providing address conversion in both directions.

## How ARP Works

When an incoming packet destined for a host machine on a particular local area network arrives at a gateway, the gateway asks the ARP program to find a physical host or MAC address that matches the IP address. The ARP program looks in the ARP cache and, if it finds the address, provides it so that the packet can be converted to the right packet length and format and sent to the machine. If no entry is found for the IP address, ARP broadcasts a request packet in a special format to all the machines on the LAN to see if one machine knows that it has that IP address associated with it. A machine that recognizes the IP address as its own returns a reply so indicating. ARP updates the ARP cache for future reference and then sends the packet to the MAC address that replied.

---

## ARP Header

![](../assets/arp-6-638.jpg)

**Hardware Type** - The physical medium of transmission. 1 for Ethernet, 6 for 802.11 \(WiFi\).

**Protocol Type** - Uses same constants as EtherType. Identifies the Layer 3 address that needs to be mapped to an Ethernet address.

**Hardware Address Length** - Size of Layer 2 address defined in Hardware type.

**Protocol Address Length** - Size of Layer 3 address defined in Protocol Type.

**Op Code** - 1 for Request, 2 for Reply. Other codes exist for protocols that make use of an Arp Header

**Sender Hardware Address, Sender Protocol Address** - The sender's addresses.

**Target Hardware Address, Target Protocol Address** - The target's addresses, The hardware address is filled in upon receipt of the ARP Request.


---

## Reverse Address Resolution Protocol

**RARP** \(Reverse Address Resolution Protocol\) is a protocol by which a physical machine in a local area network can request to learn its IP address from a gateway server's Address Resolution Protocol \(ARP\) table or cache. A network administrator creates a table in a local area network's gateway router that maps the physical machine addresses \(or Media Access Control - MAC address\) to corresponding Internet Protocol addresses. When a new machine is set up, its RARP client program requests from the RARP server on the router to be sent its IP address. Assuming that an entry has been set up in the router table, the RARP server will return the IP address to the machine which can store it for future use.

---
