


---

## Review

* Developers will Understand Unicast/Multicast/Broadcast/Anycast and explain their differences
* Developers will relate Switching/Routing concepts
* Developers will apply their fundamental understanding of IPv4 Addressing
* Developers will have a fundamental understanding of CIDR Notation
* Developers will apply their understanding of IPv6 Addressing
* Developers will implement data interchange formats such as JSON, Struct libraries \(Python\)
* Developers will manipulate multiple sockets in a single client or server

---
---

## Summary

* Introduction to Internet Protocol
* Layer 3 Devices
* IPv4 Addresses
* IPv4 Address Classes
* Subnet Mask
* CIDR
* Routing Protocols
* Routing 
* IPv4
* NAT and PAT
* IPv6
* IPv6 Addresses
* IPv6 Address Types
* Unicast
* Multicast
* Anycast
* NDP
* ICMP
* Ping
* ICMP Errors
* ICMP Header


---

**Complete Performance Lab** 3-1

