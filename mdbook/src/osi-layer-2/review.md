---

# Review

* Apply knowledge of Broadcast and Collision Domain concepts \(include all “layer 2 concepts\) in identification /maintenance of domain users
* Able to create code using C28 POSIX API/BSD Sockets
* Set socket options

---

# Summary

* Data Encapsulation and The TCP/IP Model
* Introduction to Ethernet
* Layer 1 Devices
* Layer 2 Devices
* MAC Addresses
* Switches
* Ethernet Header
* Broadcast vs Collision Domains
* ARP
* ARP Header
* RARP


\*\*\*\*

---

**Continue to Performance Lab:** 2-1

|[Lab 2-1](lab-2-1.md)|