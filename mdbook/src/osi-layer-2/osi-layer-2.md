
# OSI Layer 2
    - KSATS(K0072,K0086,K0089,K0611,K0697)


## **Overview**

* Data Encapsulation and The TCP/IP Model
* Introduction to Ethernet
* Layer 1 Devices
* Layer 2 Devices
* MAC Addresses
* Switches
* Ethernet Header
* Broadcast vs Collision Domains
* ARP
* ARP Header
* RARP

## Objectives

* Apply knowledge of Broadcast and Collision Domain concepts \(include all “layer 2 concepts\) in identification /maintenance of domain users
* Able to create code using the POSIX API/BSD Sockets
* Set socket options

---
## To access the OSI layer 2 slides please click [here](slides)

---

## Data Encapsulation and The TCP/IP Model

![](../assets/encapsulation.PNG)

**Data Encapsulation:**  The process of taking data from one protocol and translating it into another protocol, so the data can continue across the network.   During encapsulation, each layer builds a protocol data unit \(PDU\) by adding a header \(and sometimes a footer\) containing control information to the PDU from the layer above.

![OSI vs TCP/IP](../assets/OSI4.png)

* **Layer 4\) Application:** Telnet, SSH, FTP, POP3, SMTP, HTTP, etc...
* **Layer 3\) Transport:** TCP and UDP
* **Layer 2\)** **Internet:**  IP Addressing and Routing
* **Layer 1\)** **Network Access Layer:**  Ethernet, Wi-Fi, "Hardware", etc...

---


## Intro to Ethernet

![](../assets/physical.PNG)

 Ethernet is a family of technologies used to connect LANs and WANs. It has used several different physical mediums over time from coaxial, to twisted pair, and now fiber.

The Ethernet protocol is the foundation of sending and receiving traffic and is the lowest level of network communication we will discuss in class.

The term Ethernet generally refers to a standard published in 1982 by Digital Equipment Corp., Intel Corp., and Xerox Corp. It is the predominant form of local area network technology used with TCP/IP today. It uses an access method called CSMA/CD, which stands for Carrier Sense, Multiple Access with Collision Detection. It operates at 10 Mbits/sec and uses 48-bit addresses.

A few years later the IEEE \(Institute of Electrical and Electronics Engineers\) 802 Committee published a sightly different set of standards. 802.3 covers an entire set of CSMA/CD networks....

In the TCP/IP world, the encapsulation of IP datagrams is defined in RFC 894 \[Hornig 1984\] for Ethernets and in RFC 1042 \[Postel and Reynolds 1988\] for IEEE 802 networks. The Host Requirements RFC requires that every Internet host connected to a 10 Mbits/sec Ethernet cable:

1. Must be able to send and receive packets using RFC 894 \(Ethernet\) encapsulation. 
2. Should be able to receive RFC 1042 \(IEEE 802\) packets intermixed with RFC 894 packets. 
3. May be able to send packets using RFC 1042 encapsulation. If the host can send both types of packets, the type of packet sent must be configurable and the configuration option must default to RFC 894 packets. 


---

## Layer 1 Devices

Dedicated Layer 1 devices are mostly extinct in today's environments. As technology advanced, the functionality became obsolete or was rolled into layer 2 devices. Most of Layer 1 consists of data transfer equipment or media, such as: Cables \(Cat5, Cat6, Coax, Fiber Optic etc..\), WiFi, Bluetooth, USB, etc..

**Repeater** - Has 2 ports. Takes received data from one side and retransmits it on the other. It allows the physical transmission medium to be extended.

**Hub** - A multi-port repeater. Data is received on one port, and retransmitted out ALL other ports \(except port it was received on\)

*  **Single Collision Domain**
*  **Single Broadcast Domain**

---


## Layer 2 Devices

**Bridges** have two ports and connect two independent PHYSICAL networks. These days, you are most likely to encounter a bridge in virtualization software or a system's network interfaces. \(e.g. bridging vs NAT mode for VMs, or combining two interfaces in Linux \)

**Switches** are multiport bridges and are still prevalent networks today. Switches are typically the lowest level device that makes decisions on where to send traffic. The MAC \(Media Access Control\) address is what switches use to make those decisions.

* **Multiple Collision Domain**

  \(Each physical connection to a switch port is a collision domain\)

* **Single Broadcast Domain**

---

## MAC Addresses

48 bits long, usually represented as 6 groups of hex \(e.g.aa:bb:cc:dd:ee:ff\).

Manufacturers 'burn in' a MAC address to each port on a device, however most modern equipment allows the MAC to be changed in software as well.

**Organizational Unique Identifier** - \(OUI\) First 3 bytes.

* OUIs specific to each manufacturer \(VMWare OUI 00:50:56, Intel OUIs 00:02:B3, 00:03:47, 00:04:23\)
* Majority of the time OUIs are enforced. The MAC address of a device will be unique within the network
* LSBit in 1st byte in OUI determines if address is unicast \(0\) or muilticast\(1\)
* 2ndLSBit in 1st byte in OUI determines if the mac is globally unique \(0\), or locally administered \(1\)

**Host ID** - Last 3 bytes

* Randomly decided by manufacturer
* Ideally there are no collisions within a physical network

**Special MAC Addresses**

* The Layer 2 BROADCAST address for IPv4 and ARP is FF:FF:FF:FF:FF:FF
* The Layer 2 MULTICAST address for IPv6 is 33:33:00:00:00:01

---
## Switches

Switches operate using MAC addresses found in the Ethernet header of the traffic.

Switches have CAM \(Content Addressable Memory\) tables that map a physical port to MAC addresses.

* Switches populate a CAM table by looking at the Ethernet header and determining the SOURCE address of traffic arriving on that port.
* It is possible the table maps multiple MAC Addresses to the same physical port \(e.g. Switch port is connected to a hub with multiple hosts\).

Switches check the DESTINATION MAC address against the MAC table.

* If the address is mapped to a port, forward it out that port.
* If no mapping, forward it out ALL ports EXCEPT the one it was received on - Traffic usually gets a response, and the MAC table will be updated normally when that traffic is seen.

If the DESTINATION MAC is a broadcast address, then forward it out ALL ports EXCEPT the one it was received on.

---

## Ethernet Header

![](../assets/ethernet-frame-explained.png)

**Source MAC Address** - Originator of the traffic.

**Destination MAC Address** - The Ethernet address of the intended recipient. Must be in the same broadcast domain, and may be a broadcast address.

**Ethernet Type** \(aka EtherType\) - What is being transmitted in the Ethernet payload.

* ARP, IPv4, IPv6, etc...
* See references page...

---

## MTU and Fragmentation

Maximum Transmission Unit is the maximum size of the payload on a given link-layer \(Layer 2\) protocol. \(1500 bytes for Ethernet\).

The max size of an IP packet is 65535 bytes \(unless jumbogram extension header is used\).

IP packets that are bigger than the link-layer protocol MTU are FRAGMENTED into several frames, and the recipient is responsible for reassembly.

MTU can vary at each hop. This means it is possible for every node on the path to the destination to further fragment.

* Senders usually try to discover the smallest MTU on the path and shrink  their own transmissions appropriately.

Packets larger than MTU are dropped and an ICMP message is returned to the sender indicating the packet is too big.

---

## Broadcast vs Collision Domains

![](../assets/data-transfer-domains.jpg)

**Collision domain**

A collision domain is, as the name implies, a part of a network where packet collisions can occur. A collision occurs when two devices send a packet at the same time on the shared network segment. The packets collide and both devices must send the packets again, which reduces network efficiency. Collisions are often in a hub environment, because each port on a hub is in the same collision domain. By contrast, each port on a bridge, a switch or a router is in a separate collision domain.

## **Broadcast domain**

A broadcast domain is a domain in which a broadcast is forwarded. A broadcast domain contains all devices that can reach each other at the data link layer \(OSI layer 2\) by sending a broadcast. All ports on a hub or a switch are by default in the same broadcast domain. All ports on a router are in different broadcast domains and routers don’t forward broadcasts from one broadcast domain to another.


---



