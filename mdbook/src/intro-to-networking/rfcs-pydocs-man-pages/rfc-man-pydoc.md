# RFC - Request For Comments


[https://www.ietf.org/rfc.html](https://www.ietf.org/rfc.html)

Originally \(in the ARPANET days\), an RFC was a semi-formal document of ideas shared to get comments from peers. Now they are issued by the Internet Engineering Task Force \(IETF\) to formally define an accepted specification.

RFCs describe and define the history, implementation, formats, and use of protocols. They are the authoritative source of information regarding protocols.

You should not 100% rely on vendor implementations or internet posts if you have questions about a protocol.

Microsoft intentionally implemented parts of the HTML/web protocols incorrectly around the era of Internet Explorer 5 and 6.

Posters on forums, even Stack Overflow, can say something that sounds correct/interpreted as correct, even if the RFC specifies otherwise.

Stack Overflow is still a highly useful site, just use it to supplement/validate your understanding of the RFC.


# What is an RFC?

Each RFC defines a monograph or memorandum that engineers or experts in the field have sent to the Internet Engineering Task Force (IETF) organization, the most important technical collaboration consortium on the internet, so that it can be valued by the rest of the community

RFCs cover a wide range of standards, and TCP/IP is just one of these. Each RFC has a number; IPv4 is documented by RFC 791.

The most important IPs are defined by RFC, such as the IP protocol that's detailed in RFC 791, FTP in RFC 959, or HTTP in RFC 2616


# Intro to Web Scraping by getting RFCs

**Using urllib**


```python
#!/usr/bin/env python3

# Using urllib to download RFCs

import sys, urllib.request
try:
    rfc_number = int(sys.argv[1])
except (IndexError, ValueError):
    print('Must supply an RFC number as first argument')
    sys.exit(2)
template = 'http://www.rfc-editor.org/rfc/rfc{}.txt'
url = template.format(rfc_number)
rfc_raw = urllib.request.urlopen(url).read()
rfc = rfc_raw.decode()
print(rfc)

```

```
$ python RFC_download_urllib.py 2324
```

**Using Requests**

```python
#Using requests to download RFCs

#!/usr/bin/env python3

import sys, requests
try:
    rfc_number = int(sys.argv[1])
except (IndexError, ValueError):
    print('Must supply an RFC number as first argument')
    sys.exit(2)
template = 'http://www.rfc-editor.org/rfc/rfc{}.txt'
url = template.format(rfc_number)
rfc = requests.get(url).text
print(rfc)
```

**Using the Socket module**

```python
#!/usr/bin/env python3

import sys, socket
try:
    rfc_number = int(sys.argv[1])
except (IndexError, ValueError):
    print('Must supply an RFC number as first argument')
    sys.exit(2)

host = 'www.rfc-editor.org'
port = 80
sock = socket.create_connection((host, port))

req = ('GET /rfc/rfc{rfcnum}.txt HTTP/1.1\r\n'
'Host: {host}:{port}\r\n'
'User-Agent: Python {version}\r\n'
'Connection: close\r\n'
'\r\n'
)

req = req.format(rfcnum=rfc_number,host=host,port=port,version=sys.version_info[0])
sock.sendall(req.encode('ascii'))
rfc_bytes = bytearray()

while True:
 buf = sock.recv(4096)
 if not len(buf):
     break
 rfc_bytes += buf
rfc = rfc_bytes.decode('utf-8')
print(rfc)
```



---

# Man Pages

- [http://man7.org/linux/man-pages/index.html](http://man7.org/linux/man-pages/index.html%29%29\)

- [https://www.kernel.org/doc/man-pages](https://www.kernel.org/doc/man-pages/%29%29\)

The BSD socket API is POSIX compliant and is the standard on Linux machines.

There are man pages that describe each system call.

The man pages can be accessed via a terminal in Linux, or via Google.

Man pages have different numbers for different sections. I typically link to man 7 pages which in turn link to the man 2 pages for specific calls.

* 7 describes the higher level operations
* 2 describes the system calls for C \(which are reused in Python\)

## e.g.

* man 7 socket describes sockets and the associated function calls.
* man 2 &lt;function name&gt; would provide details of a specific call for sockets

---

# Pydocs

 [https://docs.python.org/2/library/index.html](https://docs.python.org/2/library/index.html%29%29)

Python documentation can be accessed online.

Each module has it's own page describing the functions, function parameters, constants, and example usage. These will be linked in the references slide in each slide deck.

The search function on Pydocs will let you search for a specific function to see it's parameters and return values. It does require you to know the module.

* e.g. accept\(\) can be found by searching socket.accept\(\)

---

**RFC:**

[https://tools.ietf.org/html/rfc6458](https://tools.ietf.org/html/rfc6458)

**PyDocs:**

[https://docs.python.org/2/library/socket.html](https://docs.python.org/2/library/socket.html)

**Man Pages:**

[http://man7.org/linux/man-pages/man7/socket.7.html](http://man7.org/linux/man-pages/man7/socket.7.html)

[http://man7.org/linux/man-pages/man7/ip.7.html](http://man7.org/linux/man-pages/man7/ip.7.html)

[http://man7.org/linux/man-pages/man7/ipv6.7.html](http://man7.org/linux/man-pages/man7/ipv6.7.html)
