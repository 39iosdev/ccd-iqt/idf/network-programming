


# Wireshark
   - KSATS(K0759,K0594,A0116,A0604,A0605,A0606,A0607,A0608)

#### To access the WireShark slides please click [here](slides)

### Reference:  [https://www.wireshark.org/docs/wsug\_html\_chunked/](https://www.wireshark.org/docs/wsug_html_chunked/)

Wireshark is a GUI based protocol analyzer. It works on live traffic and PCAP files.
---

![](../../assets/wireshark-home-59512deb3df78cae8135d3cd.png)

---
When you first launch Wireshark a welcome screen similar to the one shown above should be visible, containing a list of available network connections on your current device. In this example, you'll notice that the following connection types are shown: _Bluetooth Network Connection_, _Ethernet_, _VirtualBox Host-Only Network_, _Wi-Fi_. Displayed to the right of each is an EKG-style line graph that represents live traffic on that respective network.

To begin capturing packets, first select one or more of these networks by clicking on your choice\(s\) and using the _Shift_ or _Ctrl_ keys if you'd like to record data from multiple networks simultaneously. Once a connection type is selected for capturing purposes, its background will be shaded in either blue or gray. Click on _Capture_ from the main menu, located towards the top of the Wireshark interface. When the drop-down menu appears, select the _Start_ option.

You can also initiate packet capturing via one of the following shortcuts.

* **Keyboard:**

   Press _Ctrl + E_

* **Mouse:**

   To begin capturing packets from one particular network, simply double-click on its name

* **Toolbar:**

   Click on the blue shark fin button, located on the far left-hand side of the Wireshark toolbar

The live capture process will now begin, with packet details displayed in the Wireshark window as they are recorded. Perform one of the actions below to stop capturing.

* **Keyboard:**

   Press _Ctrl + E_

* **Toolbar:**

   Click on the red stop button, located next to the shark fin on the Wireshark toolbar



---

## Wireshark Installation and Setup.

In order to install the Wireshark GUI from repositories, simply type…

```$ sudo dnf install wireshark-qt```

…into a terminal. This will install both Qt and the CLI version of Wireshark. At this point, you can use Wireshark as root, but it is generally considered a bad practice. Therefore, we will set up permissions for regular users to capture on network interfaces \(see below about security implications\).

## Setting permissions

During installation, a system group called `wireshark` was created. Users in this group can capture network traffic. All you need to do is to add your user account into the group like this, substituting your username for _username_:

```$ sudo usermod -a -G wireshark username```

Then log out, back in again, and you are ready to go!



---

# Analyzing Packets

Now that you've recorded some network data it's time to take a look at the captured packets. As shown in the screenshot below, the captured data interface contains three main sections: The packet list pane, the packet details pane, and the packet bytes pane.

## **There are three windows in Wireshark:**

1. The **traffic** window which shows the packets in order of receipt.
2. The **packet** window which shows the packet \(and frame\) breakdowns of the selected packet from the traffic window.
3. The _**hexdump**_ ****window which shows the raw bytes of the highlighted section highlighted in the packet window.

![](../../assets/wireshark-captured-data-panes-59512e265f9b58f0fc7b1f17.png)

**Packet List**

The packet list pane, located at the top of the window, shows all packets found in the active capture file. Each packet has its own row and corresponding number assigned to it, along with each of these data points.

* **Time:**

   The timestamp of when the packet was captured is displayed in this column, with the default format being the number of seconds \(or partial seconds\) since this specific capture file was first created. To modify this format to something that may be a bit more useful, such as the actual time of day, select the _Time Display Format_ option from Wireshark's _View_ menu - located at the top of the main interface.

* **Source:**

   This column contains the address \(IP or other\) where the packet originated.

* **Destination:**

   This column contains the address that the packet is being sent to.

* **Protocol:**

   The packet's protocol name \(i.e., TCP\) can be found in this column.

* **Length:**

   The packet length, in bytes, is displayed in this column.

* **Info:**

   Additional details about the packet are presented here. The contents of this column can vary greatly depending on packet contents.

When a packet is selected in the top pane, you may notice one or more symbols appear in the first column. Open and/or closed brackets, as well as a straight horizontal line, can indicate whether or not a packet or group of packets are all part of the same back-and-forth conversation on the network. A broken horizontal line signifies that a packet is not part of said conversation.

**Packet Details**

The details pane, found in the middle, presents the protocols and protocol fields of the selected packet in a collapsible format. In addition to expanding each selection, you can also apply individual Wireshark filters based on specific details as well as follow streams of data based on protocol type via the details context menu – accessible by right-clicking your mouse on the desired item within this pane.

**Packet Bytes**

At the bottom is the packet bytes pane, which displays the raw data of the selected packet in a hexadecimal view. This [hex dump](https://www.lifewire.com/xxd-linux-command-unix-command-4097149) contains 16 hexadecimal bytes and 16 ASCII bytes alongside the data offset.

Selecting a specific portion of this data automatically highlights its corresponding section in the packet details pane and vice versa. Any bytes that cannot be printed are instead represented by a period.

You can choose to show this data in bit format as opposed to hexadecimal by right-clicking anywhere within the pane and selecting the appropriate option from the context menu.


---

---

# Filters

## **Click the expression button to the right of the Filter bar OR type it in yourself:**

* eth.addr==aa:bb:cc:dd:ee:ff
* ip.addr== 127.0.0.1
* ip.src== 127.0.0.2
* ipv6.dst == ::1
* tcp.port== 1337
* tcp.sport== 80
* udp.dport== 53

## **You can also filter on protocol:**

* arp
* icmpv6

![](../../assets/wireshark-display-filters-59512e443df78cae8136b049.png)

One of the most important feature sets in Wireshark is its filter capabilities, especially when you're dealing with files that are significant in size. Capture filters can be set before the fact, instructing Wireshark to only record those packets that meet your specified criteria.

Filters can also be applied to a capture file that has already been created so that only certain packets are shown. These are referred to as display filters.

Wireshark provides a large number of predefined filters by default, letting you narrow down the number of visible packets with just a few keystrokes or mouse clicks. To use one of these existing filters, place its name in the _Apply a display filter_ entry field \(located directly below the Wireshark toolbar\) or in the _Enter a capture filter_ entry field \(located in the center of the welcome screen\).

There are multiple ways to achieve this. If you already know the name of your filter, simply type it into the appropriate field. For example, if you only wanted to display TCP packets you would type _tcp_. Wireshark's autocomplete feature will show suggested names as you begin typing, making it easier to find the correct moniker for the filter you're seeking.

Another way to choose a filter is to click on the bookmark-like icon positioned on the left-hand side of the entry field. This will present a menu containing some of the most commonly-used filters as well as an option to _Manage Capture Filters_ or _Manage Display Filters_. If you choose to manage either type an interface will appear allowing you to add, remove or edit filters.

You can also access previously-used filters by selecting the down arrow, located on the right-hand side of the entry field, which displays a history drop-down list.

Once set, capture filters will be applied as soon as you begin recording network traffic. To apply a display filter, however, you'll need to click on the right arrow button found on the far-right hand side of the entry field.

---

# Coloring Rules

![](../../assets/wireshark-colors-59512e8f3df78cae8137715b.png)

While Wireshark's capture and display filters allow you to limit which packets are recorded or shown on the screen, its colorization functionality takes things a step further by making it easy to distinguish between different packet types based on their individual hue. This handy feature lets you quickly locate certain packets within a saved set by their row's color scheme in the packet list pane.

Wireshark comes with about 20 default coloring rules built in; each which can be edited, disabled or deleted if you wish. You can also add new shade-based filters through the coloring rules interface, accessible from the _View_ menu. In addition to defining a name and filter criteria for each rule, you are also asked to associate both a background color and a text color.

Packet colorization can be toggled off and on via the _Colorize Packet List_ option, also found within the _View_ menu.

[https://www.lifewire.com/wireshark-tutorial-4143298](https://www.lifewire.com/wireshark-tutorial-4143298)

---



