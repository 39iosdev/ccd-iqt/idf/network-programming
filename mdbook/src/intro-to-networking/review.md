


---

## Review

* Developers will have a thorough understanding of Ports and Protocols
* Developers will apply knowledge of Bitwise Protocol and binary operators: AND, OR, XOR
* Developers will have a thorough understanding of RFCs, Pydocs, and Man Pages
* Developers will use Wireshark to view and filter network traffic at any layer of the OSI model
* Developers will use NetCat to interact with, and debug networking code
* Developers will use Wireshark to identify and debug networking code
* Developers will use Ping
* Developers will use ifconfig/ipconfig

---
## Wireshark Review

Wireshark is an open-source packet analyzer, which is used for education, analysis, software development, communication protocol development, and network troubleshooting.

It is used to track the packets so that each one is filtered to meet our specific needs. It is commonly called as a sniffer, network protocol analyzer, and network analyzer. It is also used by network security engineers to examine security problems.

Wireshark is a free to use application which is used to apprehend the data back and forth. It is often called as a free packet sniffer computer application. It puts the network card into an unselective mode, i.e., to accept all the packets which it receives.

***Wireshark uses***

Wireshark can be used in the following ways:
```
It is used by network security engineers to examine security problems.
It allows the users to watch all the traffic being passed over the network.
It is used by network engineers to troubleshoot network issues.
It also helps to troubleshoot latency issues and malicious activities on your network.
It can also analyze dropped packets.
It helps us to know how all the devices like laptop, mobile phones, desktop, switch, routers, etc., communicate in a local network or the rest of the world.
```
## Ping and Tracert Review

The ping command is a very common method for troubleshooting the accessibility of devices. It uses a series of Internet Control Message Protocol (ICMP) Echo messages to determine:
```
Whether a remote host is active or inactive.

The round-trip delay in communicating with the host.

Packet loss.
```
The traceroute/tracert command is used to discover the routes that packets actually take when traveling to their destination. The device (for example, a router or a PC) sends out a sequence of User Datagram Protocol (UDP) datagrams to an invalid port address at the remote host.

Three datagrams are sent, each with a Time-To-Live (TTL) field value set to one. The TTL value of 1 causes the datagram to "timeout" as soon as it hits the first router in the path; this router then responds with an ICMP Time Exceeded Message (TEM) indicating that the datagram has expired.

