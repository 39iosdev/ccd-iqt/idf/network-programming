
## Ping
Another commonly used command is ping. ping is used to send a test packet, or echo packet, to a machine to find out if the machine is reachable and how long the packet takes to reach the machine. This useful diagnostic tool can be employed in elementary hacking techniques.

```
ping www.yahoo.com
```

This returns to you that a 32-byte echo packet was sent to the destination and returned. The TTL (Time to Live) item shows how many intermediary steps, or hops, the packet should take to the destination before giving up. Remember that the Internet is a vast conglomerate of interconnected networks. Your packet probably won’t go straight to its destination; it will take several hops to get there. As with IPConfig, you can type in ping -? to find out various ways you can refine your ping.


## Tracert
The next command is the tracert command. This command is more-or-less a deluxe version of ping. tracert not only tells you if the packet got to its destination and how long it took but also tells you all the intermediate hops it took to get there.

```
tracert to www.yahoo.com

```

***This same command can be executed in Linux or UNIX, but there it is called traceroute rather than tracert.***

With tracert, you can see (in milliseconds) the IP addresses of each intermediate step listed and how long it took to get to that step. Knowing the steps required to reach a destination can be very important, as you will see later in this book.Certainly there are other utilities that can be of use to you when working with network communications. However, the three we just examined—IPConfig, ping, and tracert—are the core utilities. These three utilities are absolutely essential to any network administrator, and you should commit them to memory.

## Additional tools to be aware of:
    * Netstat - Netstat is another interesting command. It is an abbreviation for network status. Essentially this command tells you what connections your computer currently has.
    * NSLookup - nslookup, which is an abbreviation for name server lookup, is used to connect with your network’s DNS server
    * ARP - Address Resolution Protocol (ARP) is used to map IP addresses to MAC addresses
        As with other commands, there are a variety of flags for ARP. Here are some of them:
```
    -a displays the current ARP cache table.

    /g does the same as /a.

    /d deletes a specific entry from the ARP cache table.

    /s adds a static entry to the ARP cache table. 
``` 
    * Route - The route command is used to view the IP routing table












