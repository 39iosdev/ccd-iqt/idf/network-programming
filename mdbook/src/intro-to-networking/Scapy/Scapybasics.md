
## What is Scapy?

Scapy is a powerful Python module for packet manipulation. It can decode and create packets for a wide variety of protocols. Scapy can be used for scanning, probing, and network discovery tasks inside Python programs

Understanding Scapy
Scapy (https://scapy.net) is one of the powerful Python tools that is used to capture, sniff, analyze, and manipulate network packets. It can also build a packet structure of layered protocols and inject a wiuthib stream into the network. You can use it to build a wide number of protocols on top of each other and set the details of each field inside the protocol, or, better, let Scapy do its magic and choose the appropriate values so that each one can have a valid frame. Scapy will try to use the default values for packets if not overridden by users. The following values will be set automatically for each stream:
```
The IP source is chosen according to the destination and routing table
The checksum is automatically computed
The source Mac is chosen according to the output interface
The Ethernet type and IP protocol are determined by the upper layer
```

Scapy can be programmed to inject a frame into a stream and to resend it. You can, for example, inject a 802.1q VLAN ID into a stream and resend it to execute attacks or analysis on the network. Also, you can visualize the conversation between two endpoints and graph it using Graphviz and ImageMagick modules.

Scapy has its own Domain-Specific Language (DSL) that enables the user to describe the packet that he wants to build or manipulate and to receive the answer in the same structure. This works and integrates very well with Python built-in data types, such as lists and dictionaries. We will see in examples that the received packets from the network are actually a Python list, and we can iterate the normal list functions over them.

Let’s see how it works. We first instantiate the IP class. Then, we instantiate it again and we provide a destination that is worth four IP addresses (/30 gives the netmask). Using a Python idiom, we develop this implicit packet in a set of explicit packets. Then, we quit the interpreter. As we provided a session file, the variables we were working on are saved, then reloaded:
```python
# ./run_scapy -s mysession
New session [mysession]
Welcome to Scapy (2.4.0)
>>> IP()
<IP |>
>>> target="www.target.com/30"
>>> ip=IP(dst=target)
>>> ip
<IP dst=<Net www.target.com/30> |>
>>> [p for p in ip]
[<IP dst=207.171.175.28 |>, <IP dst=207.171.175.29 |>,
 <IP dst=207.171.175.30 |>, <IP dst=207.171.175.31 |>]
>>> ^D
```
```python
# ./run_scapy -s mysession
Using session [mysession]
Welcome to Scapy (2.4.0)
>>> ip
<IP dst=<Net www.target.com/30> |>
```
Now, let’s manipulate some packets:
```python
IP()
<IP |>
a=IP(dst="172.16.1.40")
a
<IP dst=172.16.1.40 |>
a.dst
'172.16.1.40'
a.ttl
64
```
Let’s say I want a broadcast MAC address, and IP payload to ketchup.com and to mayo.com, TTL value from 1 to 9, and an UDP payload:
```python
>>> Ether(dst="ff:ff:ff:ff:ff:ff")
      /IP(dst=["ketchup.com","mayo.com"],ttl=(1,9))
      /UDP()
```
We have 18 packets defined in 1 line (1 implicit packet)

## Sensible default values
Scapy tries to use sensible default values for all packet fields. If not overridden,
```
IP source is chosen according to destination and routing table

Checksum is computed

Source MAC is chosen according to the output interface

Ethernet type and IP protocol are determined by the upper layer
```

Other fields’ default values are chosen to be the most useful ones:
```
TCP source port is 20, destination port is 80.

UDP source and destination ports are 53.

ICMP type is echo request.

```


























