

---

## OSI Layer 7
    - KSATS(K0075, K0612, K0613, K0614, K0616, K0617, K0696)

### Overview

* Introduction to DNS
* DNS Servers
* DNS Resource Records
* SOA Records
* DNS - A/AAAA Records
* DNS - PTR Records 
* DNS - CNAME Records
* DNS - MX Records
* DNS - SRV Records
* DNS Resolution
* Dig
* Dig - Output
* Introduction to HTTP
* HTTP Requests
* HTTP Responses
* HTTP Status Codes
* HTTPS
* SMTP
* DHCP

### Objectives

* Describe Hypertext Transfer Protocol/Secure \(HTTP/HTTPS\)
* Have a fundamental understanding of Domain Name System \(DNS\)
* Programmatically transfer and parse structured data
* Use dig to perform DNS queries

---
#### To access the OSI layer 7 slides please click [here](slides)


