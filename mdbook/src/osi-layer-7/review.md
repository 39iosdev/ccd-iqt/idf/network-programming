# Review

* Describe Hypertext Transfer Protocol/Secure \(HTTP/HTTPS\)
* Have a fundamental understanding of Domain Name System \(DNS\)
* Programmatically transfer and parse structured data
* Use dig to perform DNS queries

---

# Summary

* Introduction to DNS
* DNS Servers
* DNS Resource Records
* SOA Records
* DNS - A/AAAA Records
* DNS - PTR Records 
* DNS - CNAME Records
* DNS - MX Records
* DNS - SRV Records
* DNS Resolution
* Dig
* Dig - Output
* Introduction to HTTP
* HTTP Requests
* HTTP Responses
* HTTP Status Codes
* HTTPS
* SMTP
* DHCP

---

**Complete Performance Lab:** 5-1

---

|[Lab 5-1](lab-5-1.md)|
