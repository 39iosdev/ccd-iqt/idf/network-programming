## Intro to DNS

Domain names are for the benefit of humans.

IPs are difficult to remember, and if they change it is almost impossible to fully advertise the new IP to everyone. DNS maps domain names to IPs, and can advertise the services each domain offers \(HTTP, MAIL, etc\). This is the networking system in place that allows us to resolve human-friendly names to unique addresses.  The network requests supporting DNS lookups run over TCP and UDP, port 53 by default.

## Fully Qualified Domain Name \(Root-Level Domain\)

**FQDN -** the complete, or absolute, domain name of a host, which combines the network domain with the host's name.

* **mail3.example.com.**

A typical domain name used to access a website is NOT an FQDN, because that domain does not reference a non-specific host, but one of several that can serve webpages for that domain.

* **www.example.com** may ultimately be served by **webserver.example.com.** or **www2.example.com.**

A technically correct FQDN ends with a dot as a reference to the root domain.

* Sometimes software that calls for FQDN does not require the ending dot, but the trailing dot is required to conform to ICANN standards.

## Top-Level Domain {#top-level-domain}

A top-level domain, or TLD, is the most general part of the domain. The top-level domain is the furthest portion to the right \(as separated by a dot\). Common top-level domains are "com", "net", "org", "gov", "edu", and "io".

## Second-Level Domain

These are names registered to an individual or organization for use on the Internet. These names can be of Variable-length and are always based upon an appropriate top-level domain, depending on the type of organization or geographic location where a name is used.

## **Subdomain**

Additional names that an organization can create that are derived from the registered second-level domain name. For example: **"example.google.com"**.

## **Host or resource name**

Typically, the leftmost label of a DNS domain name identifies a specific resource, such as a computer on the network. The difference between a host name and a subdomain is that a host defines a computer or resource, while a subdomain extends the parent domain. It is a method of subdividing the domain itself.

![](../assets/dns_1.png)

---

## DNS Servers

The system is very simple at a high-level overview, but is very complex as you look at the details. Overall though, it is a very reliable infrastructure that has been essential to the adoption of the internet as we know it today.

## Root Servers

The Internet root name servers manage DNS server information for the Web's top-level domains \(TLD\) \(like ".com" and ".uk"\), specifically the names and IP addresses of the original \(called _authoritative_\) DNS servers responsible for answering queries about each TLD individually.

There are currently 13 root servers in operation. However, as there are an incredible number of names to resolve every minute, each of these servers is actually mirrored. The interesting thing about this set up is that each of the mirrors for a single root server share the same IP address. When requests are made for a certain root server, the request will be routed to the nearest mirror of that root server. DNS servers are installed and maintained by private businesses and Internet governing bodies around the world. Similarly, organizations can deploy DNS on their private networks separately, on the smaller scale.

If a request for "www.wikipedia.org" is made to the root server, the root server will not find the result in its records. It will check its zone files for a listing that matches "www.wikipedia.org". It will not find one.

It will instead find a record for the "org" TLD and give the requesting entity the address of the name server responsible for "org" addresses.

## TLD Servers {#tld-servers}

The requester then sends a new request to the IP address \(given to it by the root server\) that is responsible for the top-level domain of the request.

So, to continue our example, it would send a request to the name server responsible for knowing about "org" domains to see if it knows where "www.wikipedia.org" is located.

Once again, the requester will look for "www.wikipedia.org" in its zone files. It will not find this record in its files.

However, it will find a record listing the IP address of the name server responsible for "wikipedia.org". This is getting much closer to the answer we want.

## Domain-Level Name Servers {#domain-level-name-servers}

At this point, the requester has the IP address of the name server that is responsible for knowing the actual IP address of the resource. It sends a new request to the name server asking, once again, if it can resolve "www.wikipedia.org".

The name server checks its zone files and it finds that it has a zone file associated with "wikipedia.org". Inside of this file, there is a record for the "www" host. This record tells the IP address where this host is located. The name server returns the final answer to the requester.

## Resolving Name Server

When a DNS request is made it is usually sent to a Resolving Name Server. They are intermediaries for the users with cached queries to resolve addresses quickly. If the request is not cached, it is then passed to the Root Servers and proceeds to go through the process of "resolving" the request.

## Zone Files

A zone file is a simple text file that contains the mappings between domain names and IP addresses. This is how the DNS system finally finds out which IP address should be contacted when a user requests a certain domain name.

Zone files reside in name servers and generally define the resources available under a specific domain, or the place that one can go to get that information.

However, don't confuse Zones with Domains, a zone will start as a storage for a single DNS domain name. If you add other domains below the domain where the zone was created, these domains can either be part of the same zone or belong to another zone. If you add a subdomain, the subdomain can either be included and managed as part of the original zone record, or delegated to another zone created for the subdomain.

---

## DNS Resource Records

Within a zone file, records are kept. In its simplest form, a record is basically a single mapping between a resource and a name. These can map a domain name to an IP address, define the name servers for the domain, define the mail servers for the domain, etc.

## Resource Record Types

Different types of records contain different types of host information. For example, an Address record provides the name-to-address mapping for a given host, while a Start of Authority \(SOA\) record specifies the start of authority for a given zone.

A DNS zone must contain several types of resource records for DNS to function properly. Other records can be present, but the following are required for standard DNS:

* **Name server \(NS\)**---Binds a domain name with a hostname for a specific name server The DNS zone must contain NS records for each primary and secondary name server in the zone. The DNS zone must contain NS records to link the zone to higher- and lower-level zones within the DNS hierarchy.
* **Start of Authority \(SOA\)**---Indicates the start of authority for the zone. The name server must contain one SOA record specifying its zone of authority.
* **Canonical name \(CNAME\)**---Specifies the canonical or primary name for the owner. The owner name is an alias.
* **Address \(A\)**---Provides the IP address for the zone.

| RR Type | Field Differences |
| :--- | :--- |
| A | IPv4 Address, eDirectory context, comments, and version |
| AAAA | IPV6 address |
| AFSDB | Subtype and hostname fields |
| CNAME | Domain name of aliased host |
| HINFO | CPU and OS fields of up to 256 characters each |
| ISDN | ISDN address and subaddress fields |
| MB | Mailbox address domain name |
| MG | Mail group member domain name |
| MINFO | Responsible mailbox and error message mailbox |
| MR | Mail rename mailbox |
| MX | Reference and exchange fields |
| NS | DNS server domain name |
| PTR | Domain Name |
| PX | Preference, Map 822 \(domain name\), and Map x400 fields \(domain name in X.400 syntax\) |
| RP | Responsible person's mailbox and TXT RR domain name |
| RT | Preference and Intermediate fields |
| SRV | Service, proto, priority, weight, port, and target fields |
| TXT | Text field for up to 256 characters in multiple strings |
| WKS | Protocol and bit map fields |
| X25 | PSDN address |

## DNS Record Format

**Owner** - Name of domain

**TTL** - TTL in seconds

**Class** - Protocol family to use, almost always IN

**Type** - Type of record being returned

**RDATA** - Data of the record

---

## SOA Records

The **Start of Authority**, or SOA, record identifies information about the domain and is a mandatory record in all zone files. It must be the first real record in a file \(although $ORIGIN or $TTL specifications may appear above\). It is also one of the most complex.

This also serves as the authoritative copy that keeps secondary DNS servers up to date.

```text
domain.com.  IN SOA ns1.domain.com. admin.domain.com. (                                            
                                12083   ; serial number
                                3h      ; refresh interval
                                30m     ; retry interval
                                3w      ; expiry period
                                1h      ; negative TTL
)
```

In addition to the DNS record fields, it contains more information:

**Authoritative server** - Primary DNS for the zone.

**Responsible Person** - Email address of admin, with @ replaced by .

**Serial Number** - Current "version number", used by secondary DNS to determine whether they should update.

**Refresh** - Number of seconds between each secondary DNS checks for updates.

**Retry** - Number of seconds to wait for secondary DNS to re-try a zone transfer.

**Expire** - TTL of zone transfer for secondary DNS.

**Minimum TTL** - The minimum TTL for all records in the zone.**domain.com.**: This is the root of the zone. This specifies that the zone file is for the `domain.com.` domain. Often, you'll see this replaced with `@`, which is just a placeholder that substitutes the contents of the `$ORIGIN` variable we learned about above.

* **IN SOA**: The "IN" portion means internet \(and will be present in many records\). The SOA is the indicator that this is a Start of Authority record.
* **ns1.domain.com.**: This defines the primary master name server for this domain. Name servers can either be master or slaves, and if dynamic DNS is configured one server needs to be a "primary master", which goes here. If you haven't configured dynamic DNS, then this is just one of your master name servers.
* **admin.domain.com.**: This is the email address of the administrator for this zone. The "@" is replaced with a dot in the email address. If the name portion of the email address normally has a dot in it, this is replace with a "\" in this part \([your.name@domain.com](mailto:your.name@domain.com) becomes your\name.domain.com\).
* **12083**: This is the serial number for the zone file. Every time you edit a zone file, you must increment this number for the zone file to propagate correctly. Slave servers will check if the master server's serial number for a zone is larger than the one they have on their system. If it is, it requests the new zone file, if not, it continues serving the original file.
* **3h**: This is the refresh interval for the zone. This is the amount of time that the slave will wait before polling the master for zone file changes.
* **30m**: This is the retry interval for this zone. If the slave cannot connect to the master when the refresh period is up, it will wait this amount of time and retry to poll the master.
* **3w**: This is the expiry period. If a slave name server has not been able to contact the master for this amount of time, it no longer returns responses as an authoritative source for this zone.
* **1h**: This is the amount of time that the name server will cache a name error if it cannot find the requested name in this file

---

## DNS – A and AAAA Records

Both of these records map a host to an IP address. The "A" record is used to map a host to an IPv4 IP address, while "AAAA" records are used to map a host to an IPv6 address.

The general format of these records is this:

```text
host     IN      A       172.16.48.1 (IPv4_address)
```

```text
host     IN      AAAA    2001:db8::ff00:42:8329 (IPv6_address)
```

---

## DNS – PTR Records

The PTR records are used to define a name associated with an IP address. PTR records are the inverse of an A or AAAA record. PTR records are unique in that they begin at the `.arpa` root and are delegated to the owners of the IP addresses. The Regional Internet Registries \(RIRs\) manage the IP address delegation to organization and service providers. The Regional Internet Registries include APNIC, ARIN, RIPE NCC, LACNIC, and AFRINIC.

Here is an example of a PTR record for 111.222.333.444 would look like:

```text
444.333.222.111.in-addr.arpa.   33692   IN  PTR host.example.com.
```

This example of a PTR record for an IPv6 address shows the _nibble_ format of the reverse of Google's IPv6 DNS Server `2001:4860:4860::8888`

```text
8.8.8.8.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.6.8.4.0.6.8.4.1.0.0.2.ip6.arpa. 86400IN PTR google-public-dns-a.google.com.
```

## There are several things of note:

* **The IP is backwards.**
* **The string 'in-addr.arpa.' is appended to the reversed IP.**
* **'in-addr.arpa.' ends with a . \(meaning it's the fully qualified domain name\)**
* **The IPv6 address is not in a traditional format**  [**http://rdns6.com/hostRecord**](http://rdns6.com/hostRecord%29%29%29%29%20\)

---

## DNS - CNAME

CNAME records define an alias for canonical name for your server \(one defined by an A or AAAA record\).

For instance, we could have an A name record defining the "server1" host and then use the "www" as an alias for this host:

```text
server1     IN  A       111.111.111.111
www         IN  CNAME   server1
```

According to RFC 2181, there must be only one canonical name per alias.

---

## DNS - MX Records

MX records are used to define the mail exchanges that are used for the domain. This helps email messages arrive at your mail server correctly.

Unlike many other record types, mail records generally don't map a host to something, because they apply to the entire zone. As such, they usually look like this:

```text
        IN  MX  10  mail1.domain.com.
        IN  MX  50  mail2.domain.com.
mail1   IN  A       111.111.111.111
mail2   IN  A       222.222.222.222
```

Note that there is no host name at the beginning.

Also note that there is an extra number in there. This is the preference number that helps computers decide which server to send mail to if there are multiple mail servers defined. Lower numbers have a higher priority.

The MX record should generally point to a host defined by an A or AAAA record, and not one defined by a CNAME.

In this example, the "mail1" host is the preferred email exchange server.

We could also write that like this:

```text
        IN  MX  10  mail1
        IN  MX  50  mail2
mail1   IN  A       111.111.111.111
mail2   IN  A       222.222.222.222
```

---

## DNS - SRV Records

These allow domains to identify the services offered and the hosts responsible for providing the services:

**Service** - name of the service \(http, telnet, etc\)

**Proto** - Usually the Layer 4 protocol

**Domain** - Domain this applies to

**TTL/Class** - Same as other DNS records

**Priority** - Lower number = higher priority. Higher priortity hosts get contacted first

**Weight** - Used to load balance hosts with identical Priorities

**Port** - Port of service

**Target** - FQDN for service host

## `_http._tcp.reskit.com. IN SRV 0 0 80 webserver1.noam.reskit.com.`

## `_http._tcp.reskit.com. IN SRV 10 0 80 webserver2.noam.reskit.com.`

---

## DNS Resolution

* Resolving addresses are performed with DNS Request messages.
* DNS requests may be iterative or recursive. At almost every step, the cache of the current system is checked before attempting another query to reduce the load on higher servers.
* Recursive requests allow the recipient of a DNS query to make it's own DNS query. That recipient can make it's own query if needed. This is the typical method.
* Iterative requests mean the host expects the DNS server to reply immediately, without asking any other DNS servers. The reply will either be from the cache, or a referral to another name server. Iterative requests may then be made by the initial host if needed.
* If at any point there is a known resolution, it is passed back down immediately. No need to bother anyone else in the chain. This answer will be cached as it goes back down.

![](../assets/dns_2.png)

![](../assets/dns_3.png)

---

# DNS Tiers - ASCII View

| **Root Servers \(A - M\)** |
| :---: |
| **\|** |
| **\|** |
| **V** |
| **TLD \(e.g. com, .net, .io, etc\)** |
| **\|** |
| **\|** |
| **V** |
| **Domain Name Server** |

---

# **DNS Request ASCII View**

**This is an example based upon the common home user. Remember, cache is checked at each step.**

| **Browser** |
| :---: |
| **\|** |
| **\|** |
| **V** |
| **Home Router** |
| **\|** |
| **\|** |
| **V** |
| **ISP DNS** |
| **\|** |
| **\|** |
| **V** |
| **Whoever the ISP DNS is configured to ask.** |
| **\|** |
| **\|** |
| **V** |
| **Root Server** |

---

---

## Dig - Output

Dig is a better DNS query tool. It will soon replace nslookup altogether. Comes standard for Linux and Unix.

### **Syntax:** `dig [@nameserver] domain record-type [+short]`

### **Normal Lookups:**

### `dig gov.hackistan AAAA`

### `dig gov.hackistan ANY`

### **Ask google's DNS specifically:**

### `dig @ns1.google.com gov.hackistan`

### **Reverse lookup:**

### `dig -x 314.42.13.37`

### **Dig, but less verbose:**

### `[user@localhostDesktop]$ dig +short google.com A google.com AAAA 216.58.218.206 2607:f8b0:4000:802::200e`

### **Dig, commands:**

### `dig -h`

```text
Usage:  dig [@global-server] [domain] [q-type] [q-class] {q-opt}
            {global-d-opt} host [@local-server] {local-d-opt}
            [ host [@local-server] {local-d-opt} [...]]
Where:  domain	  is in the Domain Name System
        q-class  is one of (in,hs,ch,...) [default: in]
        q-type   is one of (a,any,mx,ns,soa,hinfo,axfr,txt,...) [default:a]
                 (Use ixfr=version for type ixfr)
        q-opt    is one of:
                 -4                  (use IPv4 query transport only)
                 -6                  (use IPv6 query transport only)
                 -b address[#port]   (bind to source address/port)
                 -c class            (specify query class)
                 -f filename         (batch mode)
                 -i                  (use IP6.INT for IPv6 reverse lookups)
                 -k keyfile          (specify tsig key file)
                 -m                  (enable memory usage debugging)
                 -p port             (specify port number)
                 -q name             (specify query name)
                 -t type             (specify query type)
                 -u                  (display times in usec instead of msec)
                 -x dot-notation     (shortcut for reverse lookups)
                 -y [hmac:]name:key  (specify named base64 tsig key)
        d-opt    is of the form +keyword[=value], where keyword is:
                 +[no]aaflag         (Set AA flag in query (+[no]aaflag))
                 +[no]aaonly         (Set AA flag in query (+[no]aaflag))
                 +[no]additional     (Control display of additional section)
                 +[no]adflag         (Set AD flag in query (default on))
                 +[no]all            (Set or clear all display flags)
                 +[no]answer         (Control display of answer section)
                 +[no]authority      (Control display of authority section)
                 +[no]badcookie      (Retry BADCOOKIE responses)
                 +[no]besteffort     (Try to parse even illegal messages)
                 +bufsize=###        (Set EDNS0 Max UDP packet size)
                 +[no]cdflag         (Set checking disabled flag in query)
                 +[no]class          (Control display of class in records)
                 +[no]cmd            (Control display of command line)
                 +[no]comments       (Control display of comment lines)
                 +[no]cookie         (Add a COOKIE option to the request)
                 +[no]crypto         (Control display of cryptographic fields in records)
                 +[no]defname        (Use search list (+[no]search))
                 +[no]dnssec         (Request DNSSEC records)
                 +domain=###         (Set default domainname)
                 +[no]dscp[=###]     (Set the DSCP value to ### [0..63])
                 +[no]edns[=###]     (Set EDNS version) [0]
                 +ednsflags=###      (Set EDNS flag bits)
                 +[no]ednsnegotiation (Set EDNS version negotiation)
                 +ednsopt=###[:value] (Send specified EDNS option)
                 +noednsopt          (Clear list of +ednsopt options)
                 +[no]expire         (Request time to expire)
                 +[no]fail           (Don't try next server on SERVFAIL)
                 +[no]header-only    (Send query without a question section)
                 +[no]identify       (ID responders in short answers)
                 +[no]idnin          (Parse IDN names)
                 +[no]idnout         (Convert IDN response)
                 +[no]ignore         (Don't revert to TCP for TC responses.)
                 +[no]keepopen       (Keep the TCP socket open between queries)
                 +[no]mapped         (Allow mapped IPv4 over IPv6)
                 +[no]multiline      (Print records in an expanded format)
                 +ndots=###          (Set search NDOTS value)
                 +[no]nsid           (Request Name Server ID)
                 +[no]nssearch       (Search all authoritative nameservers)
                 +[no]onesoa         (AXFR prints only one soa record)
                 +[no]opcode=###     (Set the opcode of the request)
                 +[no]qr             (Print question before sending)
                 +[no]question       (Control display of question section)
                 +[no]rdflag         (Recursive mode (+[no]recurse))
                 +[no]recurse        (Recursive mode (+[no]rdflag))
                 +retry=###          (Set number of UDP retries) [2]
                 +[no]rrcomments     (Control display of per-record comments)
                 +[no]search         (Set whether to use searchlist)
                 +[no]short          (Display nothing except short
                                      form of answer)
                 +[no]showsearch     (Search with intermediate results)
                 +[no]sigchase       (Chase DNSSEC signatures)
                 +[no]split=##       (Split hex/base64 fields into chunks)
                 +[no]stats          (Control display of statistics)
                 +subnet=addr        (Set edns-client-subnet option)
                 +[no]tcp            (TCP mode (+[no]vc))
                 +timeout=###        (Set query timeout) [5]
                 +[no]topdown        (Do +sigchase in top-down mode)
                 +[no]trace          (Trace delegation down from root [+dnssec])
                 +trusted-key=####   (Trusted Key to use with +sigchase)
                 +tries=###          (Set number of UDP attempts) [3]
                 +[no]ttlid          (Control display of ttls in records)
                 +[no]ttlunits       (Display TTLs in human-readable units)
                 +[no]unknownformat  (Print RDATA in RFC 3597 "unknown" format)
                 +[no]vc             (TCP mode (+[no]tcp))
                 +[no]zflag          (Set Z flag in query)
        global d-opts and servers (before host name) affect all queries.
        local d-opts and servers (after host name) affect only that lookup.
        -h                           (print help and exit)
        -v                           (print version and exit)

```


# Dig - Output

![](../assets/dig1.PNG)

* **Header:** This displays the dig command version number, the global options used by the dig command, and some additional header information.
* **QUESTION SECTION:** This displays the question it asked the DNS. i.e. This is your input. Since we said ‘**dig redhat.com**’, and the default type dig command uses an A record, it indicates in this section that we asked for the A record of the redhat.com website.
* **ANSWER SECTION:** This displays the answer it receives from the DNS. i.e. This is your output. This displays the A record of redhat.com.
* **AUTHORITY SECTION:** This displays the DNS name server that has the authority to respond to this query. Basically this displays available name servers of redhat.com.
* **ADDITIONAL SECTION:** This displays the ip address of the name servers listed in the AUTHORITY SECTION.
* **Stats section**  - at the bottom displays a few dig command statistics including how much time it took to execute this query.

### `+nocomments` – Turn off the comment lines.

### `+noauthority`– Turn off the authority section.

### `+noadditional`– Turn off the additional section.

### `+nostats`– Turn off the stats section.

### `+noanswer` – Turn off the answer section \(Of course, you wouldn’t want to turn off the answer section\).

### `$ dig redhat.com +noall+answer`

### `; <<>> DiG 9.7.3-RedHat-9.7.3-2.el6 <<>> redhat.com +noall+answer`

### `;; global options: +cmd`

### `redhat.com. 60 IN A 209.132.183.81`

---


