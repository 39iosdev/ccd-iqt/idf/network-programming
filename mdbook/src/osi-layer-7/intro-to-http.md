
## Intro to HTTP

HTTP stands for **Hypertext Transfer Protocol**. It's the network protocol used to deliver virtually all files and data \(collectively called resources\) on the World Wide Web, whether they're HTML files, image files, query results, or anything else. Usually, HTTP takes place through TCP/IP sockets \(and this tutorial ignores other possibilities\).

A browser is an HTTP client because it sends requests to an HTTP server \(Web server\), which then sends responses back to the client. The standard \(and default\) port for HTTP servers to listen on is 80, though they can use any port.  \(HTTP a stateless protocol, i.e. not maintaining any connection information between transactions\).

**HTTP Requests are sent with several broad categories of information:**

* The resource requested and it's location \(index.html at example.com\).
* Information about the expected result \(type of data, language, etc\).
* Any supplemental data needed to process the request \(form data, parameters, etc\).

**HTTP Responses usually contain:**

* The result of the transaction \(status codes\).
* Metadata about the transaction \(date, webserver, content length and type\).
* The data.

### HTTP Line Breaks

* An HTTP line break is a carriage return \(CRLF\) followed by a newline \(\r\n\).
* A blank line with a single \r\n signifies the end of a header.

**This ends a request, and separates the data from the header in a response.**

**CR** and **LF** are control characters, respectively coded 0x0D \(13 decimal\) and 0x0A \(10 decimal\). They are used to mark a line break in a text file. As you indicated, Windows uses two characters the **CR LF** sequence; Unix only uses **LF** and the old MacOS \( pre-OSX MacIntosh\) used **CR.**

## HTTP 1.1

Like many protocols, HTTP is constantly evolving. HTTP 1.1 has recently been defined, to address new needs and overcome shortcomings of HTTP 1.0. Generally speaking, it is a superset of HTTP 1.0. Improvements include:

* Faster response, by allowing multiple transactions to take place over a single persistent connection.
* Faster response and great bandwidth savings, by adding cache support.
* Faster response for dynamically-generated pages, by supporting chunked encoding, which allows a response to be sent before its total length is known.
* Efficient use of IP addresses, by allowing multiple domains to be served from a single IP address.


---

---

## HTTP Requests

* The only MANDATORY parts of an HTTP Request are the METHOD, the URL, HTTP version, and the host \(and any data you need to send if POSTING\).
* We will only discuss the methods GET and POST because they are almost exclusively what you will encounter, however others do exist.

## Initial Request Line

The initial line is different for the request than for the response. A request line has three parts, separated by spaces: a method name, the local path of the requested resource, and the version of HTTP being used. A typical request line is:

> ```text
> GET /path/to/file/index.html HTTP/1.0
> ```

Notes:

* **GET** is the most common HTTP method; it says "give me this resource". \(Method names are always uppercase.\)
* The path is the part of the URL after the host name, also called the request URI \(a URI is like a URL, but more general\).
* The HTTP version always takes the form "**HTTP/x.x**", uppercase.

#### Example 1

`GET /hello.htm HTTP/1.1`

`User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)`

`Host: www.example.com`

`Accept-Language:en-us`

`Accept-Encoding:gzip, deflate`

`Connection: Keep-Alive`

#### Example 2

`POST /cgi-bin/process.cgi HTTP/1.1`

`User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)`

`Host: www.tutorialspoint.com`

`Content-Type: application/x-www-form-urlencoded`

`Content-Length: length`

`Accept-Language:en-us`

`Accept-Encoding:gzip, deflate`

`Connection: Keep-Alive`

**HTTP Methods \(Verbs\):**

GET is the default method used by the HTTP.

[There are 9 HTTP methods.](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods)

Some of them are:

1. GET — Fetch a URL
2. HEAD — Fetch information about a URL
3. PUT — Store to a URL
4. POST — Send form data to a URL and get a response back
5. DELETE — Delete a URL GET and POST \(forms\) are commonly used

REST APIs use GET, PUT, POST, and DELETE.

#### Safe methods

Some of the methods \(for example, HEAD, GET, OPTIONS and TRACE\) are, by convention, defined as safe, which means they are intended only for information retrieval and should not change the state of the server.

### POST

```text
POST /index.html HTTP/1.1
```

When a web browser sends a POST request from a web form element, the default Internet media type is "application/x-www-form-urlencoded". This is a format for encoding key-value pairs with possibly duplicate keys. Each key-value pair is separated by an '&' character, and each key is separated from its value by an '=' character. Keys and values are both escaped by replacing spaces with the '+' character and then using URL encoding on all other non-alphanumeric characters.

For example, the key-value pairs

```text
Name: Gareth Wylie
Age: 24
Formula: a + b == 13%!
```

are encoded as

```text
Name=Gareth+Wylie&Age=24&Formula=a+%2B+b+%3D%3D+13%25%21
```

Starting with HTML 4.0, forms can also submit data in [multipart/form-data](https://en.wikipedia.org/wiki/Multipart/form-data) as defined in [RFC 2388](https://tools.ietf.org/html/rfc2388) 

#### Examples:

A simple form using the default `application/x-www-form-urlencoded` content type:

```text
POST / HTTP/1.1
Host: foo.com
Content-Type: application/x-www-form-urlencoded
Content-Length: 13

say=Hi&to=Mom
```

A form using the `multipart/form-data` content type:

```text
POST /test.html HTTP/1.1 
Host: example.org 
Content-Type: multipart/form-data;boundary="boundary" 

--boundary 
Content-Disposition: form-data; name="field1" 

value1 
--boundary 
Content-Disposition: form-data; name="field2"; filename="example.txt" 

value2
```

---

---

## HTTP Responses

HTTP Responses conclude the transaction. Other transactions may be required to get other resources found on the page. Remember that \r\n separates the header from the actual data.

### Initial Response Line \(Status Line\)

The initial response line, called the status line, also has three parts separated by spaces: the HTTP version, a response status code that gives the result of the request, and an English reason phrase describing the status code. Typical status lines are:

> ```text
> HTTP/1.0 200 OK
> ```

or

> ```text
> HTTP/1.0 404 Not Found
> ```

Notes:

* The HTTP version is in the same format as in the request line, "**HTTP/x.x**".
* The status code is meant to be computer-readable; the reason phrase is meant to be human-readable, and may vary.

#### Example 1

`HTTP/1.1 200 OK`

`Date: Mon, 27Jul2009 12:28:53 GMT`

`Server: Apache/2.2.14 (Win32)`

`Last-Modified: Wed, 22 Jul 2009 19:15:56 GMT`

`Content-Length: 88`

`Content-Type: text/html`

`Connection: Closed`

#### Example 2

`HTTP/1.1 404 Not Found`

`Date: Sun, 18 Oct 2012 10:36:20 GMT`

`Server: Apache/2.2.14 (Win32)`

`Content-Length: 230`

`Connection: Closed`

`Content-Type: text/html; charset=iso-8859-1`


---
---

## HTTP Status Codes

The status code is a three-digit integer, and the first digit identifies the general category of response.

The first digit of the status code specifies one of five standard classes of responses. The message phrases shown are typical, but any human-readable alternative may be provided. Unless otherwise stated, the status code is part of the **HTTP/1.1** standard \([RFC 7231](https://tools.ietf.org/html/rfc7231)\).

So, if you can’t find the file that client is asking, then you send appropriate status code.

If the client has no permission to see the file, then you send appropriate status code.

[These are the list of status codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes) we can use.

Below are some common ones:

**100 Series, Informational**

* 100 Continue.

**200 Series, Successful responses**

* 200 OK - Your response will be in the data.

**300 Series, Redirection**

* 301 Moved Permanently - new URI likely provided in data.
* 307 Temporary Redirect - Use this new URI now and same METHOD to repeat the transaction.
* 308 Permanent Redirect - Use this new URI permanently and same METHOD to repeat the transaction.

**400 Series, Client Error**

* 400 Bad Request - Your request was not understood.
* 403 Forbidden - No access rights to URI
* 404 Not Found - No such URI
* 418 I'm a teapot - Coffee is poison!

**500 Series, Server Error**

* 500 Internal Server Error - Something probably errored/crashed while processing your request.
* 503 Server Unavailable - No server can handle the request at this time. May include a "Retry-After" key-value pair.

**A complete list of status codes is in the HTTP specification \(section 9 for HTTP 1.0, and section 10 for HTTP 1.1\).**

* **HTTP 1.0 \(RFC 1945\)--**[HTML](http://www.ics.uci.edu/pub/ietf/http/rfc1945.html), [text](http://www.ics.uci.edu/pub/ietf/http/rfc1945.txt), and [gzip'd PostScript](http://www.ics.uci.edu/pub/ietf/http/rfc1945.ps.gz)
* **HTTP 1.1 \(RFC 2616\)--**[HTML](http://www.w3.org/Protocols/rfc2616/rfc2616.html), [text](http://www.w3.org/Protocols/rfc2616/rfc2616.txt), [PostScript](ftp://ftp.isi.edu/in-notes/rfc2616.ps), and [PDF](http://www.w3.org/Protocols/HTTP/1.1/rfc2616.pdf)

---

---

## HTTPS

###  **HyperText Transfer Protocol Secure \(HTTPS\):**

An extension of the [Hypertext Transfer Protocol](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol) \(HTTP\) for secure communication over a computer network, and is widely used on the Internet.

HTTPS pages typically use one of two secure protocols to encrypt communications - SSL \(Secure Sockets Layer\) or TLS \(Transport Layer Security\). Both the TLS and SSL protocols use what is known as an 'asymmetric' Public Key Infrastructure \(PKI\) system. An asymmetric system uses two 'keys' to encrypt communications, a 'public' key and a 'private' key. Anything encrypted with the public key can only be decrypted by the private key and vice-versa.

When you request a HTTPS connection to a webpage, the website will initially send its SSL certificate to your browser. This certificate contains the public key needed to begin the secure session. Based on this initial exchange, your browser and the website then initiate the 'SSL handshake'. The SSL handshake involves the generation of shared secrets to establish a uniquely secure connection between yourself and the website.

When a trusted SSL Digital Certificate is used during a [HTTPS](https://www.instantssl.com/https-tutorials/what-is-https.html) connection, users will see a padlock icon in the browser address bar. When an Extended Validation Certificate is installed on a web site, the address bar will turn green.

### The major benefits of a HTTPS certificate are:

* Customer information, like credit card numbers, are encrypted and cannot be intercepted
* Visitors can verify you are a registered business and that you own the domain
* Customers are more likely to trust and complete purchases from sites that use HTTPS

---
