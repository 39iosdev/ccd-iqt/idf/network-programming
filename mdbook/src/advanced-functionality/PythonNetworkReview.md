

# Python Network Programming Review Practice

Sockets have a long history. Their use originated with ARPANET in 1971 and later became an API in the Berkeley Software Distribution (BSD) operating system released in 1983 called Berkeley sockets.

When the Internet took off in the 1990s with the World Wide Web, so did network programming. Web servers and browsers weren’t the only applications taking advantage of newly connected networks and using sockets. Client-server applications of all types and sizes came into widespread use.

Today, although the underlying protocols used by the socket API have evolved over the years, and we’ve seen new ones, the low-level API has remained the same.

The most common type of socket applications are client-server applications, where one side acts as the server and waits for connections from clients. This is the type of application that I’ll be covering in this tutorial. More specifically, we’ll look at the socket API for Internet sockets, sometimes called Berkeley or BSD sockets. There are also Unix domain sockets, which can only be used to communicate between processes on the same host

# Let's Practice some more.
--- 
# Challenge Labs #1 

1. Create a programming script that tests if an html page is found or absent from the server.

2. Create a programming script to download and display the content of robots.txt for wikipedia.org.

3. Now refactor the program in exercise 2 so that it accepts a website as an argument. 

4. Create a programming script to extract h1 tag from example.com.

5. Create a programming script to extract and display all the image links from https://en.wikipedia.org/wiki/United_States_Air_Force.

---  
  
  
# Challenge Labs 2

1. Create a port scanner.

2. Create a TCP Ack Scanner.

3. Create a DNS traceroute.

4. Create a keylogger.

5. Using the keylogger you just built send it to another host to log keystrokes.

