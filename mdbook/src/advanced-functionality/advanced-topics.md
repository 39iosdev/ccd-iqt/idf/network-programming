
# Advanced topics in Networking 
---

# Error Handling

**If any of the socket functions fail then python throws an exception called socket.error which must be caught.**

```text
#handling errors in python socket programs

import socket  #for sockets
import sys  #for exit

try:
    #create an AF_INET, STREAM socket (TCP)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error, msg:
    print 'Failed to create socket. Error code: ' + str(msg[0]) +' , Error message : ' + msg[1]
sys.exit();
print 'Socket Created'
```

All errors raise exceptions. The normal exceptions for invalid argument types and out-of-memory conditions can be raised; errors related to socket or address semantics raise the error[`socket.error`](https://docs.python.org/2/library/socket.html?highlight=pton#socket.error).

Non-blocking mode is supported through[`setblocking()`](https://docs.python.org/2/library/socket.html?highlight=pton#socket.socket.setblocking). A generalization of this based on timeouts is supported through[`settimeout()`](https://docs.python.org/2/library/socket.html?highlight=pton#socket.socket.settimeout).

The module[`socket`](https://docs.python.org/2/library/socket.html?highlight=pton#module-socket)exports the following constants and functions:

_exception_ `socket.error`

This exception is raised for socket-related errors. The accompanying value is either a string telling what went wrong or a pair`(errno,string)`representing an error returned by a system call, similar to the value accompanying[`os.error`](https://docs.python.org/2/library/os.html#os.error). See the module[`errno`](https://docs.python.org/2/library/errno.html#module-errno), which contains names for the error codes defined by the underlying operating system.

Changed in version 2.6:[`socket.error`](https://docs.python.org/2/library/socket.html?highlight=pton#socket.error)is now a child class of[`IOError`](https://docs.python.org/2/library/exceptions.html#exceptions.IOError).

_exception_ `socket.herror`

This exception is raised for address-related errors, i.e. for functions that use\_h\_errno\_in the C API, including[`gethostbyname_ex()`](https://docs.python.org/2/library/socket.html?highlight=pton#socket.gethostbyname_ex)and[`gethostbyaddr()`](https://docs.python.org/2/library/socket.html?highlight=pton#socket.gethostbyaddr).

The accompanying value is a pair`(h_errno,string)`representing an error returned by a library call._string\_represents the description of\_h\_errno_, as returned by the`hstrerror()`C function.

_exception_`socket.gaierror`

This exception is raised for address-related errors, for[`getaddrinfo()`](https://docs.python.org/2/library/socket.html?highlight=pton#socket.getaddrinfo)and[`getnameinfo()`](https://docs.python.org/2/library/socket.html?highlight=pton#socket.getnameinfo). The accompanying value is a pair`(error,string)`representing an error returned by a library call._string\_represents the description of\_error_, as returned by the`gai_strerror()`C function. The  _error\_value will match one of the\`EAI_\*\`constants defined in this module.

_exception_`socket.timeout`

This exception is raised when a timeout occurs on a socket which has had timeouts enabled via a prior call to`settimeout()`. The accompanying value is a string whose value is currently always “timed out”.


---


# JSON Module

JSON = JavaScript Object Notation. It's a series of key-value pairs. The key-value pairs may be nested.

* A data encoding commonly used on the web when interacting with Javascript 
* Sometime preferred over XML because it's less verbose and faster to parse 
* Syntax is almost identical to a Python dict

```text
{
"recipe" : 
    {

    "title" : "Famous Guacomole",

    "description" : "A southwest favorite!",

    "ingredients" : \[

        {"num": "2", "item":"Large avocados, chopped"},

        {"num": "1/2", "units":"C", "item":"White onion, chopped"},

        {"num": "1", "units":"tbl", "item":"Fresh squeezed lemon juice"},

        {"num": "1", "item":"Jalapeno pepper, diced"},

        {"num": "1", "units":"tbl", "item":"Fresh cilantro, minced"}, 

        {"num": "3", "units":"tsp", "item":"Sea Salt"}, 

        {"num": "6", "units":"bottles","item":"Ice-cold beer"} 

    \],      

    "directions" : "Combine all ingredients and hand whisk to desired consistency. Serve and enjoy with ice-cold beers."  

    } 
}
```

* **Parsing a JSON document:**

`import json`

`doc = json.load(open("recipe.json"))`

* **Result is a collection of nested dict/lists:**

`ingredients = doc['recipe']['ingredients']`

`for item in ingredients:`

`# Process item`

`...`

* **Dumping a dictionary as JSON:**

`f = open("file.json","w")`

`json.dump(doc,f)`

**json.dumps\(\) creates a JSON string from the data passed in. It looks like a Python dictionary with quotes around it.**

`>>> import json`

`>>> data = {'foo':1, 'bar':'qwerty'}`

`>>>json.dumps(data)`

`'{"foo": 1, "bar": "qwerty"}'`

`>>> data`

`{'foo': 1, 'bar': 'qwerty'}`

`>>>`

**json.loads\(\) takes a JSON String and makes it onto a dictionary.**

`>>>jsonstr = json.dumps(data)`

`>>> type(json.loads(jsonstr))`

`<type 'dict'>`

---

---

# Struct Module

Struct allows you to pack values into specified data types/sizes and endianess. Packed data is represented by a string of hex bytes. Struct will also unpack data from the hex string and provide you a tuple of the values.

Packing is used to prepare structured binary data such as a protocol header or a message format. This data can then be referenced like a struct in C, or sent across the wire.

It uses a format string and variable arguments \(like print or printf in C\)

`>>> from struct import *`

`>>> pack('hhl', 1, 2, 3)`

`'\x00\x01\x00\x02\x00\x00\x00\x03'`

`>>> unpack('hhl', '\x00\x01\x00\x02\x00\x00\x00\x03')`

`(1, 2, 3)`

## Functions vs. Struct Class

There are a set of module-level functions for working with structured values, and there is also the Struct class \(new in Python 2.5\). Format specifiers are converted from their string format to a compiled representation, similar to the way regular expressions are. The conversion takes some resources, so it is typically more efficient to do it once when creating a Struct instance and call methods on the instance instead of using the module-level functions. All of the examples below use the Struct class.

## Packing and Unpacking

Structs support\_packing\_data into strings, and\_unpacking\_data from strings using format specifiers made up of characters representing the type of the data and optional count and endianess indicators. For complete details, refer to the standard library documentation.

In this example, the format specifier calls for an integer or long value, a two character string, and a floating point number. The spaces between the format specifiers are included here for clarity, and are ignored when the format is compiled.

```text
import struct 
import binascii

values = (1, 'ab', 2.7)
s = struct.Struct('I 2s f')
packed_data = s.pack(*values)

print 'Original values:', values
print 'Format string  :', s.format
print 'Uses           :', s.size, 'bytes'
print 'Packed Value   :', binascii.hexlify(packed_data)
```

The example converts the packed value to a sequence of hex bytes for printing with binascii.hexlify\(\), since some of the characters are nulls.

```text
$ python struct_pack.py

Original values: (1, 'ab', 2.7)
Format string  : I 2s f
Uses           : 12 bytes
Packed Value   : 0100000061620000cdcc2c40
```

If we pass the packed value to unpack\(\), we get basically the same values back \(note the discrepancy in the floating point value\).

```text
import struct
import binascii

packed_data = binascii.unhexlify('0100000061620000cdcc2c40')

s = struct.Struct('I 2s f')
unpacked_data = s.unpack(packed_data)
print'Unpacked Values:', unpacked_data
```

```text
$ python struct_unpack.py

Unpacked Values: (1, 'ab', 2.700000047683716)
```

### See also:

**struct:** The standard library documentation for this module.  [https://docs.python.org/2.7/library/struct.html](https://docs.python.org/2.7/library/struct.html%29%29\)

**array:** The array module, for working with sequences of fixed-type values.  [https://docs.python.org/2/library/array.html](https://docs.python.org/2/library/array.html%29%29\)

**binascii:**  The binascii module, for producing ASCII representations of binary data.

[https://docs.python.org/2/library/binascii.html](https://docs.python.org/2/library/binascii.html%29%29%29\)


---

---

# encode\(\)/decode\(\)

A Unicode string is a sequence of code points, which are numbers from 0 to 0x10ffff. This sequence needs to be represented as a set of bytes \(meaning, values from 0–255\) in memory. The rules for translating a Unicode string into a sequence of bytes are called an **encoding**. Python's default encoding is 'ASCII'.  However,  UTF-8 is probably the most commonly supported encoding.

UTF stands for “Unicode Transformation Format”, and the ‘8’ means that 8-bit numbers are used in the encoding. \(There’s also a UTF-16 encoding, but it’s less frequently used than UTF-8.\) UTF-8 uses the following rules:

1. If the code point is &lt;128, it’s represented by the corresponding byte value.
2. If the code point is between 128 and 0x7ff, it’s turned into two byte values between 128 and 255.
3. Code points &gt;0x7ff are turned into three- or four-byte sequences, where each byte of the sequence is between 128 and 255.

UTF-8 has several convenient properties:

1. It can handle any Unicode code point.
2. A Unicode string is turned into a string of bytes containing no embedded zero bytes. This avoids byte-ordering issues, and means UTF-8 strings can be processed by C functions such as `strcpy()` and sent through protocols that can’t handle zero bytes.
3. A string of ASCII text is also valid UTF-8 text.
4. UTF-8 is fairly compact; the majority of code points are turned into two bytes, and values less than 128 occupy only a single byte.
5. If bytes are corrupted or lost, it’s possible to determine the start of the next UTF-8-encoded code point and resynchronize. It’s also unlikely that random 8-bit data will look like valid UTF-8.

Converting from bytes to str and back.

`bstr = b'qwerty'`

`type(bstr)`

`<class 'bytes'>`

`str = bstr.decode('utf-8')`

`type(str)`

`<class 'str'>`

The method **encode\(\)** returns an encoded version of the string.

`bstr2 = str.encode('utf-8')`

`type(bstr2)`

`<class 'bytes'>`

**References:** [https://docs.python.org/2/howto/unicode.html](https://docs.python.org/2/howto/unicode.html)


---
---

# In-Memory Data Structures

Python includes several standard programming data structures as built-in types \(list, tuple, dictionary, and set\). Most applications won’t need any other structures, but when they do the standard library delivers.

## array

For large amounts of data, it may be more efficient to use an array instead of a list. Since the array is limited to a single data type, it can use a more compact memory representation than a general purpose list. As an added benefit, arrays can be manipulated using many of the same methods as a list, so it may be possible to replaces lists with arrays in to your application without a lot of other changes.

## Sorting

If you need to maintain a sorted list as you add and remove values, check out [heapq](https://www.geeksforgeeks.org/heap-queue-or-heapq-in-python/). By using the functions in [heapq](https://docs.python.org/3/library/heapq.html) to add or remove items from a list, you can maintain the sort order of the list with low overhead.

Another option for building sorted lists or arrays is [bisect](https://docs.python.org/3/library/bisect.html). [bisect](https://www.geeksforgeeks.org/bisect-algorithm-functions-in-python/) uses a binary search to find the insertion point for new items, and is an alternative to repeatedly sorting a list that changes frequently.

## Queue

Although the built-in list can simulate a queue using the insert\(\)and pop\(\)methods, it isn’t thread-safe. For true ordered communication between threads you should use a Queue.multiprocessing includes a version of a Queue that works between processes, making it easier to port between the modules.

## collections

Ther Python [collections](https://docs.python.org/3/library/collections.html) library includes implementations of several data structures that extend those found in other modules. For example, Deque is a double-ended queue, and allows you to add or remove items from either end. The [defaultdict](https://www.geeksforgeeks.org/defaultdict-in-python/) is a dictionary that responds with a default value if a key is missing. And [namedtuple](https://docs.python.org/3/library/collections.html#collections.namedtuple) extends the normal tuple to give each member item an attribute name in addition to a numerical index.

## Decoding Data

If you are working with data from another application, perhaps coming from a binary file or stream of data, you will find struct useful for decoding the data into Python’s native types for easier manipulation.

## Custom Variations

And finally, if the available types don’t give you what you need, you may want to subclass one of the native types and customize it. You can also start from scratch by using the abstract base classes defined in collections.

---

---

# Buffers

Working with binary packed data is typically reserved for highly performance sensitive situations or passing data into and out of extension modules. In such situations, you can optimize by avoiding the overhead of allocating a new buffer for each packed structure. The pack\_into\(\) and unpack\_from\(\) methods support writing to pre-allocated buffers directly.

```text
import struct
import binascii

s = struct.Struct('I 2s f')
values = (1, 'ab', 2.7)
print 'Original:', values

print
print 'ctypes string buffer'

import ctypes

b = ctypes.create_string_buffer(s.size)
print 'Before  :', binascii.hexlify(b.raw)

s.pack_into(b, 0, *values)
print 'After   :', binascii.hexlify(b.raw)
print 'Unpacked:', s.unpack_from(b, 0)

print
print 'array'

import array

a = array.array('c', '\0' *s.size)
print 'Before  :', binascii.hexlify(a)

s.pack_into(a, 0, *values)
print 'After   :', binascii.hexlify(a)
print 'Unpacked:', s.unpack_from(a, 0)
```

The  _size_  attribute of the Struct tells us how big the buffer needs to be.

```text
$ python struct_buffers.py

Original: (1, 'ab', 2.7)

ctypes string buffer
Before  : 000000000000000000000000
After   : 0100000061620000cdcc2c40
Unpacked: (1, 'ab', 2.700000047683716)

array
Before  : 000000000000000000000000
After   : 0100000061620000cdcc2c40
Unpacked: (1, 'ab', 2.700000047683716)
```
---


---

# Endianess

By default values are encoded using the native C library notion of “endianness”. It is easy to override that choice by providing an explicit endianness directive in the format string.

```text
import struct
import binascii

values = (1, 'ab', 2.7)
print 'Original values:', values

endianness = 
    [
    ('@', 'native, native'),
    ('=', 'native, standard'),
    ('<', 'little-endian'),
    ('>', 'big-endian'),
    ('!', 'network'),
    ]

for code, name in endianness:
    s = struct.Struct(code + ' I 2s f')
    packed_data = s.pack(*values)
    print
    print 'Format string  :', s.format, 'for', name
    print 'Uses           :', s.size, 'bytes'
    print 'Packed Value   :', binascii.hexlify(packed_data)
    print 'Unpacked Value :', s.unpack(packed_data)
```

```text
$ python struct_endianness.py

Original values: (1, 'ab', 2.7)

Format string  : @ I 2s f for native, native
Uses           : 12 bytes
Packed Value   : 0100000061620000cdcc2c40
Unpacked Value : (1, 'ab', 2.700000047683716)

Format string  : = I 2s f for native, standard
Uses           : 10 bytes
Packed Value   : 010000006162cdcc2c40
Unpacked Value : (1, 'ab', 2.700000047683716)

Format string  : < I 2s f for little-endian
Uses           : 10 bytes
Packed Value   : 010000006162cdcc2c40
Unpacked Value : (1, 'ab', 2.700000047683716)

Format string  : > I 2s f for big-endian
Uses           : 10 bytes
Packed Value   : 000000016162402ccccd
Unpacked Value : (1, 'ab', 2.700000047683716)

Format string  : ! I 2s f for network
Uses           : 10 bytes
Packed Value   : 000000016162402ccccd
Unpacked Value : (1, 'ab', 2.700000047683716)
```

## See also:

**WikiPedia: Endianness** - Explanation of byte order and endianness in encoding. \([https://en.wikipedia.org/wiki/Endianness](https://en.wikipedia.org/wiki/Endianness%29%29\)

---
---

# Reusing Socket Addresses

![](../assets/sockopt.PNG)

There might be situations when you force the program to terminate and you want to restart it immediately after that. As a result of the ﬁrst program execution, the socket might be in a TIME WAIT state. It cannot be immediately reused and an error will be raised.

This is the mechanism that should ensure that two different TCP sessions are not mixed up in the case that there are delayed packets belonging to the ﬁrst TCP session. Usually, this protection mechanism is not necessary because severe packet delays are not very likely in common networks. If you want to avoid seeing the 'address in use' error you can do it by setting the reuse address socket option or simply change the port number:

## `s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)`

**Reference:** [https://docs.python.org/2/library/socket.html](https://docs.python.org/2/library/socket.html)

---

---

# setsockopt\(\)/getsockopt\(\)

Using socket options gives you some advanced options for sockets to do interesting things.

Options have a LEVEL, OPTION NAME, and a VALUE.

* LEVEL can be the entire socket \(socket.SOL\_SOCKET\) or a specific protocol \(socket.IPPROTO\_IPV6\).
* The LEVEL specified determines what OPTION NAMEs are available to be set.
* VALUES are assigned to the specified OPTION NAME.

Unfortunately, options are scattered all over the place in documentation. Man pages are currently the best source or for a grouping of common ones, see the refs.

#### `socket.setsockopt`\(_level_,  _optname_,  _value_\)

Set the value of the given socket option \(see the Unix manual page_setsockopt\(2\)_\). The needed symbolic constants are defined in the[`socket`](https://docs.python.org/2/library/socket.html#module-socket)module \(`SO_*`etc.\). The value can be an integer or a string representing a buffer. In the latter case it is up to the caller to ensure that the string contains the proper bits \(see the optional built-in module[`struct`](https://docs.python.org/2/library/struct.html#module-struct)for a way to encode C structures as strings\).

#### `socket.getsockopt`\(_level_,  _optname_\[,  _buflen_\]\)

Return the value of the given socket option \(see the Unix man page_getsockopt\(2\)_\). The needed symbolic constants \(`SO_*`etc.\) are defined in this module. If\_buflen\_is absent, an integer option is assumed and its integer value is returned by the function. If\_buflen\_is present, it specifies the maximum length of the buffer used to receive the option in, and this buffer is returned as a string. It is up to the caller to decode the contents of the buffer \(see the optional built-in module[`struct`](https://docs.python.org/2/library/struct.html#module-struct)for a way to decode C structures encoded as strings\).

The default socket buffer size may not be suitable in many circumstances. In such circumstances, you can change the default socket buffer size to a more suitable value.

## How to do it...

Let us manipulate the default socket buffer size using a socket object's`setsockopt()`method.

First, define two constants:`SEND_BUF_SIZE`/`RECV_BUF_SIZE`and then wrap a socket instance's call to the`setsockopt()`method in a function. It is also a good idea to check the value of the buffer size before modifying it. Note that we need to set up the send and receive buffer size separately.

Listing 1.8 shows how to modify socket send/receive buffer sizes as follows:

```text
import socket

SEND_BUF_SIZE = 4096
RECV_BUF_SIZE = 4096

def modify_buff_size():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM )

    # Get the size of the socket's send buffer
    bufsize = sock.getsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF)
    print "Buffer size [Before]:%d" %bufsize

    sock.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
    sock.setsockopt(
            socket.SOL_SOCKET,
            socket.SO_SNDBUF,
            SEND_BUF_SIZE)
    sock.setsockopt(
            socket.SOL_SOCKET,
            socket.SO_RCVBUF,
            RECV_BUF_SIZE)
    bufsize = sock.getsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF)
    print "Buffer size [After]:%d" %bufsize

if __name__ == '__main__':
    modify_buff_size()
```

If you run the preceding script, it will show the changes in the socket's buffer size. The following output may be different on your machine depending on your operating system's local settings:

```text
$ python 1_8_modify_buff_size.py 
Buffer size [Before]:16384
Buffer size [After]:8192
```

## How it works...

You can call the`getsockopt()`and`setsockopt()`methods on a socket object to retrieve and modify the socket object's properties respectively. The`setsockopt()`method takes three arguments:`level`,`optname`, and`value`. Here,`optname`takes the option name and`value`is the corresponding value of that option. For the first argument, the needed symbolic constants can be found in the socket module \(`SO_*etc.`\).

### See Also:

[http://man7.org/linux/man-pages/man2/setsockopt.2.html](http://man7.org/linux/man-pages/man2/setsockopt.2.html)

[https://linux.die.net/man/2/getsockopt](https://linux.die.net/man/2/getsockopt)

---
---

# Network Byte Order

Network byte order is most-significant byte first.

Byte ordering at a host may differ.

`socket.ntohl(x)`

Convert 32-bit positive integers from network to host byte order. On machines where the host byte order is the same as network byte order, this is a no-op; otherwise, it performs a 4-byte swap operation.

`socket.ntohs(x)`

Convert 16-bit positive integers from network to host byte order. On machines where the host byte order is the same as network byte order, this is a no-op; otherwise, it performs a 2-byte swap operation.

`socket.htonl(x)`

Convert 32-bit positive integers from host to network byte order. On machines where the host byte order is the same as network byte order, this is a no-op; otherwise, it performs a 4-byte swap operation.

`socket.htons(x)`

Convert 16-bit positive integers from host to network byte order. On machines where the host byte order is the same as network byte order, this is a no-op; otherwise, it performs a 2-byte swap operation.

`socket.inet_aton(ip_string)`

Convert an IPv4 address from dotted-quad string format \(for example, ‘123.45.67.89’\) to 32-bit packed binary format, as a string four characters in length. This is useful when conversing with a program that uses the standard C library and needs objects of type`structin_addr`, which is the C type for the 32-bit packed binary this function returns.

* [`inet_aton()`](https://docs.python.org/2/library/socket.html#socket.inet_aton)also accepts strings with less than three dots; see the Unix manual page\_inet\(3\)\_for details.

If the IPv4 address string passed to this function is invalid,[`socket.error`](https://docs.python.org/2/library/socket.html#socket.error)will be raised. Note that exactly what is valid depends on the underlying C implementation of`inet_aton()`

* [`inet_aton()`](https://docs.python.org/2/library/socket.html#socket.inet_aton)does not support IPv6, and[`inet_pton()`](https://docs.python.org/2/library/socket.html#socket.inet_pton)should be used instead for IPv4/v6 dual stack support.

`socket.inet_ntoas(packed_ip)`

Convert a 32-bit packed IPv4 address \(a string four characters in length\) to its standard dotted-quad string representation \(for example, ‘123.45.67.89’\). This is useful when conversing with a program that uses the standard C library and needs objects of type`structin_addr`, which is the C type for the 32-bit packed binary data this function takes as an argument.

If the string passed to this function is not exactly 4 bytes in length,[`socket.error`](https://docs.python.org/2/library/socket.html#socket.error)will be raised.[`inet_ntoa()`](https://docs.python.org/2/library/socket.html#socket.inet_ntoa)does not support IPv6, and[`inet_ntop()`](https://docs.python.org/2/library/socket.html#socket.inet_ntop)should be used instead for IPv4/v6 dual stack support.

`socket.inet_pton(address_family, ip_string)`

Convert an IP address from its family-specific string format to a packed, binary format.[`inet_pton()`](https://docs.python.org/2/library/socket.html#socket.inet_pton)is useful when a library or network protocol calls for an object of type`structin_addr`\(similar to[`inet_aton()`](https://docs.python.org/2/library/socket.html#socket.inet_aton)\) or`structin6_addr`

Supported values for\_address\_family\_are currently[`AF_INET`](https://docs.python.org/2/library/socket.html#socket.AF_INET)and[`AF_INET6`](https://docs.python.org/2/library/socket.html#socket.AF_INET6). If the IP address string\_ip\_string\_is invalid,[`socket.error`](https://docs.python.org/2/library/socket.html#socket.error)will be raised. Note that exactly what is valid depends on both the value of\_address\_family\_and the underlying implementation of`inet_pton()`

Availability: Unix \(maybe not all platforms\).

`socket.inet_ntop(address_family, packed_ip)`

Convert a packed IP address \(a string of some number of characters\) to its standard, family-specific string representation \(for example,`'7.10.0.5'`or`'5aef:2b::8'`\)[`inet_ntop()`](https://docs.python.org/2/library/socket.html#socket.inet_ntop)is useful when a library or network protocol returns an object of type`structin_addr`\(similar to[`inet_ntoa()`](https://docs.python.org/2/library/socket.html#socket.inet_ntoa)\) or`structin6_addr`

Supported values for\_address\_family\_are currently[`AF_INET`](https://docs.python.org/2/library/socket.html#socket.AF_INET)and[`AF_INET6`](https://docs.python.org/2/library/socket.html#socket.AF_INET6). If the string\_packed\_ip\_is not the correct length for the specified address family,[`ValueError`](https://docs.python.org/2/library/exceptions.html#exceptions.ValueError)will be raised. A[`socket.error`](https://docs.python.org/2/library/socket.html#socket.error)is raised for errors from the call to[`inet_ntop()`](https://docs.python.org/2/library/socket.html#socket.inet_ntop)

Availability: Unix \(maybe not all platforms\).

---

---

## Socket Timeout

Sometimes, you need to manipulate the default values of certain properties of a socket library,for example, the socket timeout.

![](../assets/timeout.PNG)

## How to do it...

You can make an instance of a socket object and call a`gettimeout()`method to get the default timeout value and the`settimeout()`method to set a specific timeout value. This is very useful in developing custom server applications.

We first create a socket object inside a`test_socket_timeout()`function. Then, we can use the getter/setter instance methods to manipulate timeout values.

Listing 1.6 shows`socket_timeout`as follows:

```text
import socket

def test_socket_timeout():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print "Default socket timeout: %s" %s.gettimeout()
    s.settimeout(100)
    print "Current socket timeout: %s" %s.gettimeout()    

if __name__ == '__main__':
    test_socket_timeout()
```

After running the preceding script, you can see how this modifies the default socket timeout as follows:

```text
$ python 1_6_socket_timeout.py 
Default socket timeout: None
Current socket timeout: 100.0
```

## How it works...

In this code snippet, we have first created a socket object by passing the socket family and socket type as the first and second arguments of the socket constructor. Then, you can get the socket timeout value by calling`gettimeout()`and alter the value by calling the`settimeout()`method. The timeout value passed to the`settimeout()`method can be in seconds \(non-negative float\) or`None`. This method is used for manipulating the blocking-socket operations. Setting a timeout of`None`disables timeouts on socket operations.

---
---

## Socket Blocking

By default, TCP sockets are placed in a blocking mode. This means the control is not returned to your program until some specific operation is complete. For example, if you call the`connect()`API, the connection blocks your program until the operation is complete. On many occasions, you don't want to keep your program waiting forever, either for a response from the server or for any error to stop the operation. For example, when you write a web browser client that connects to a web server, you should consider a stop functionality that can cancel the connection process in the middle of this operation. This can be achieved by placing the socket in the non-blocking mode.

![](../assets/blocking.PNG)

### How to do it...

Let us see what options are available under Python. In the non-blocking mode, if any call to API, for example,`send()`or`recv()`, encounters any problem, an error will be raised. However, in the blocking mode, this will not stop the operation. We can create a normal TCP socket and experiment with both the blocking and non-blocking operations.

In order to manipulate the socket's blocking nature, we need to create a socket object first.

We can then call`setblocking(1)`to set up blocking or`setblocking(0)`to unset blocking. Finally, we bind the socket to a specific port and listen for incoming connections.

Listing 1.9 shows how the socket changes to blocking or non-blocking mode as follows:

```python
import socket

def test_socket_modes():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setblocking(1)
    s.settimeout(0.5)
    s.bind(("127.0.0.1", 0))

    socket_address = s.getsockname()
    print "Trivial Server launched on socket: %s" %str(socket_address)
    while(1):
        s.listen(1)

if __name__ == '__main__':
    test_socket_modes()
```

If you run this, it will launch a trivial server that has the blocking mode enabled as shown in the following command:

```console
$ python 1_9_socket_modes.py 
Trivial Server launched on socket: ('127.0.0.1', 51410)
```

### How it works...

In this example, we enable blocking on a socket by setting the value`1`in the`setblocking()`method. Similarly, you can unset the value`0`in this method to make it non-blocking.

---

