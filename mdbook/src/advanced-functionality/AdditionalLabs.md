
# Python Network labs

### Exercise 1:
Change the socket program socket1.py to prompt the user for the URL so it can read any web page. You can use split('/') to break the URL into its component parts so you can extract the host name for the socket connect call. Add error checking using try and except to handle the condition where the user enters an improperly formatted or non-existent URL.
```python
# socket1.py
import socket

mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mysock.connect(('data.pr4e.org', 80))
cmd = 'GET http://data.pr4e.org/romeo.txt HTTP/1.0\r\n\r\n'.encode()
mysock.send(cmd)

while True:
    data = mysock.recv(512)
    if len(data) < 1:
        break
    print(data.decode(),end='')

mysock.close()
```
### Exercise 2
Change your socket program so that it counts the number of characters it has received and stops displaying any text after it has shown 3000 characters. The program should retrieve the entire document and count the total number of characters and display the count of the number of characters at the end of the document.

### Exercise 3
Use urllib to replicate the previous exercise of (1) retrieving the document from a URL, (2) displaying up to 3000 characters, and (3) counting the overall number of characters in the document. Don’t worry about the headers for this exercise, simply show the first 3000 characters of the document contents

### Exercise 4
Change the urllinks.py program to extract and count paragraph (p) tags from the retrieved HTML document and display the count of the paragraphs as the output of your program. Do not display the paragraph text, only count them. Test your program on several small web pages as well as some larger web pages.
```python
# urllinks.py

import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = input('Enter - ')
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')

# Retrieve all of the anchor tags
tags = soup('a')
for tag in tags:
    print(tag.get('href', None))


```
### Exercise 5
Advanced) Change the socket program so that it only shows data after the headers and a blank line have been received. Remember that recv receives characters (newlines and all), not lines.


### Exercise 6

Consider the following client/server interaction:
• The client sends a positive integer number N to the server.
• The server replies by sending to the client the number of decimal places in the
received positive integer number N.
Write code for the server
```python
"""
This is the corresponding client script,
corresponding to your server.py script
"""
from socket import socket as Socket
from socket import AF_INET, SOCK_STREAM

HOSTNAME = 'localhost'  # on same host
PORTNUMBER = 12345      # same port number
BUFFER = 80             # size of the buffer

SERVER = (HOSTNAME, PORTNUMBER)
CLIENT = Socket(AF_INET, SOCK_STREAM)
CLIENT.connect(SERVER)
print('client sends 1234')
CLIENT.send('1234'.encode())
DATA = CLIENT.recv(BUFFER).decode()
print('client received', DATA)
CLIENT.close()
```

### Exercise 7 

Consider the following client/server interaction:
• The client sends a float F to the server.
• The server replies by sending to the client True if the received float F is positive
and False otherwise.
Write code for the server

```python 
"""
This is the corresponding client script,
corresponding to your script server.py.
"""
from socket import socket as Socket
from socket import AF_INET, SOCK_STREAM

HOSTNAME = 'localhost'  # on same host
PORTNUMBER = 12345      # same port number
BUFFER = 80             # size of the buffer

SERVER = (HOSTNAME, PORTNUMBER)
CLIENT = Socket(AF_INET, SOCK_STREAM)
CLIENT.connect(SERVER)
print('client sends 3.14')
CLIENT.send('3.14'.encode())
DATA = CLIENT.recv(BUFFER).decode()
print('client received', DATA)
CLIENT.close()


```













