"""
OSI MODEL: Open Systems Interconnection Model

7 layers  

1 - Physical  (Please)

* 2 - Data link (Do) *

* 3 - Network  (Not) *

* 4 - Transport (Throw) *

5 - Session (Sausage)

6 - Presentation (Pizza)

* 7 - Application (Away) *
* Layers concerning Network Programming


Layer 1 (Physical): Transmits raw bit stream (1s and 0s)over a physical medium (copper, fiber, air).
    Responsible for the physical calbe or wireless connection between network nodes
    and takes care of bit rate control.
    *Combined with layer 2 (Data link layer )to make the Network Access layer in the TCP/IP model
        TCP/IP does not take responsibility for sequencing and acknowledgment fucntions,
        leaving that to the transport layer
    Encapsulation: Bit or data stream

*Layer 2 (Data Link Layer)*: Defines the format of data on the network
    Establishes and terminates a connection between two physically connected nodes.
    It breaks up packets into frames and send them from source to destination.
    Composed of 2 parts.
        Logical Link Control (LLC) - Identifes network protocols, error checking, synchronizes frames.
        Media Access Control (MAC) - Uses MAC addresses to connect devices and define permissions 
        to transmit and receive data.
    This is where the hardware vlaues such as MAC addresses are pointed at by ARP.
    It is the first encapsulation for network traffic.
    *Combined with layer 1 (Physical layer)to make the Network Access layer in the TCP/IP model
        TCP/IP does not take responsibility for sequencing and acknowledgment fucntions,
        leaving that to the transport layer
    Encapsulation: Frame

*Layer 3 (Network Layer)*:  Decides which physical path the data will take
    The layer at which packets are sent/routed to separate machines and networks.  
    It is possible to craft a packet from scratch with python.
    ICMP (Ping), NAT is used at this level
    The network layer has two main functions. One is breaking up segments into network packets, 
    and reassembling the packets on the receiving end. The other is routing packets by discovering 
    the best path across a physical network. The network layer uses network addresses 
    (typically Internet Protocol addresses) to route packets to a destination node.
    *Corresponds to the Internet Layer of the TCP/IP model
    Encapsulation: Packet or Datagram

*Layer 4 (Transort Layer*): Transmits data using Transmission protocols including TCP and UDP
    The transport layer takes data transferred in the session layer and breaks
     it into “segments” on the transmitting end. It is responsible for reassembling the segments on the
    receiving end, turning it back into data that can be used by the session layer. 
    The transport layer carries out flow control, sending data at a rate that matches the connection 
    speed of the receiving device, and error control, checking if data was received incorrectly and if 
    not, requesting it again.
    Where much of network  communictation happens. Where TCP and UDP make internet networking possible.
    Three way TCP handshake is important here for ensuring dats is recieved.
        syn - a synchronize message is sent to the server
        syn-ack - the server acknowledges that message
        and ack - the client *acknowledges and a connection is established
    A syn flood is one of the most common types of cyber attacks
    Encapsulation: Segment

Layer 5 (Session Layer): Maintains connections and is responsible for controlling ports and sessions
    The session layer creates communication channels, called sessions, 
    between devices. It is responsible for opening sessions, ensuring they remain open and 
    functional while data is being transferred, and closing them when communication ends. 
    The session layer can also set checkpoints during a data transfer—if the session is 
    interrupted, devices can resume data transfer from the last checkpoint.
    *Part of the Application layer in the TCP/IP model
    Encapsulation: Data


Layer 6 (Presentation Layer):Ensures that data is in a usable format and is where data encryption ocurrs
    he presentation layer prepares data for the application layer. It defines how two devices 
    should encode, encrypt, and compress data so it is received correctly on the other end. 
    The presentation layer takes any data transmitted by the application layer and prepares 
    it for transmission over the session layer.
    *Part of the Application layer in the TCP/IP model

*Layer 7 (Applicaion Layer)*: Human-Computer interaction layer, where applications can access the network services
    The application layer is used by end-user software such as web browsers 
    and email clients. It provides protocols that allow software to send and receive information and present
    meaningful data to users. A few examples of application layer protocols are the 
    Hypertext Transfer Protocol (HTTP),
    File Transfer Protocol (FTP), 
    Post Office Protocol (POP), 
    Simple Mail Transfer Protocol (SMTP), 
    Domain Name System (DNS).
    Dynamic Host Configuration Protocol(DHCP)

*********************************************************************************************

TCP/IP Model

Application Layer: 
    OSI Layers 7, 6 and 5
    Protocols:
    HTTP, SMTP, Telnet, FTP, DNS, RIP SNMP

Transport Layer:
    OSI Layer 4
    Protocols:
    TCP, UDP

Internet Layer:
    OSI Layer 3
    Protococols:
    ARP, IP, ICMP

Network Access Layer:
    OSI Layer 2 and 3


"""