Hex	Decimal	Octal	Binary
0	 0     0	     0
1	 1	   1	     1
2	 2	   2	     10
3	 3	   3	     11
4	 4	   4	     100
5	 5	   5	     101
6	 6	   6	     110
7	 7	   7	     111
8	 8	   10	     1000
9	 9	   11	     1001
A	10	   12	     1010
B	11	   13	     1011
C	12	   14	     1100
D	13	   15	     1101
E	14	   16	     1110
F	15	   17	     1111
10	16	   20	     10000
20	32	   40	     100000
40	64	   100	     1000000
80	128	   200	     10000000
100	256	   400	     100000000
200	512	   1000	     1000000000
400	1024   2000	     10000000000