
## Introduction to Networking
### Definitions

Network: 

    A group of two or more computers or devices that communicate with one another.

Network interface:  

    A network interfaces is either a hardware or software component that
    interfaces between two pieces of equipment or protocol layers
    NIC - Network interface card: Hardware component
    that allows connection over a network.  It is a circuit board that provides 
    a dedicated network connection to the computer. Also called a network interface controller,
    network adapter or LAN adapter

LAN:

     Local Area Network (workstations in a room or office)
WAN: 

    Wide Area Network (extends over a large geographic area. Collection of LAN's)

IOT: 

    Internet of things: eb-enabled smart devices that use embedded systems, such as processors, sensors and communication hardware, to collect, send and act on data they acquire from their environments.


Network Topology: 

    Arrangement of devices in a network, can be logical and physical.
    Examples of different topology arrangements:
            Bus, Star, Mesh, Ring, Daisy Chain

Network Devices:

    Repeater:

        Has 2 ports. Takes data fom one port and retransmits it to another port  Allows psychical medium to be extended.  Layer 1 device

    Hub:

        Multi-port repeater.  It receives on one port and transmits out all the other ports except the one that received the transmission.  Layer 1 device.

    Switch: 

        Allows communication between devices within a network. 
        Within the OSI model, usually associated with layer 2.  Some switches have layer 3 capabilities.

    Bridge:

        Has two ports and connects two independent physical networks. Currently mostly used in virtualization software or network interfaces. Layer 2
    
    Router:

        Network device that forwards data packets between computer networks.
        Perform traffic directing function. 
        Within the OSI model, usually associated with layer 3

    Firewall:

        Network security system that monitors and controls incoming and outgoing
        network traffic based on predetermined security rules.  Considered
        a barrier between trusted internal network and untrusted external network such
        as the internet
        Within the OSI model, usually associated with layer 3, some operate at layer 7
       
    
VPN (Virtual Private Network): 

    Encrypted connection over the internet from a device to a network.
    VPN extends a corporate network through encrypted connections made over the internet.
    Traffic on the virtual network is sent securely by establishing an encrypted connection 
    known as a tunnel.  

Connection: 

    Refers to pieces of related information that are transferred through a network
    Generally infers that a connection is built before the data transfer by following
    an established protocol and then deconstructed at the end of the data transfer

Protocol:

     A set of rules for formatting and processing data.

MAC (Media Access Control address) address: 

    The physical address, the unique identifier of each host and it associated with the NIC, assigned at time of manufacturing     
    The length of the MAC address is 12 nibbles/6 bytes/ 48 bits
    
IANA (Internet Assigned Numbers Authority): 

    Responsible for global coordination of the Internet Protocol addressing systems.  Users are assigned IP addresses by Internet Service Providers (ISP).
    ISP's obtain IP addresses from a local Internet registry (LIR), or National Internet Registry (NIR)
    or from an appropriate Regional Internet Registry(RIR)

IP Address (Internet Protocol Address): 

    Provides an identity to a networked device on the internet.

Octet: 

    8 bits is one Octet.
    Standard IPv4 addresses are organized into segments of 4 octets (8 bits)
    ie.  205.166.94.16 split into octets is 11001101.10100110.01011110.00010000 

IPv4 (Internet Protocol version 4):

    Deployed January 1, 1983. Most widely used version of the internet protocol.
    Defines and IP address in a 32 bit format xxx.xxx.xxx.xxx

    Each 3 digit section (Octet) can include a number from 0 to 255.
    There are 4 sections/ 4 Octets

    Which means the total number of IPv4 addresses available is 4,294,967,296 or 2^32

    Each device connected to the internet must have a unique IP address in order to communicate with other systems.
    With all the people in the world (roughly 7.9 billion) and with all the devices each person uses 4 billion addresses is not enough

    NAT (Network Address Translation): 
    
        One (WAN) IP address can be shared/translated
        across a local area network.
        The process where a network device, usually a firewall assigns a public address
        to a computer or group of computers inside a private network.
        The goal is to limit the number of public IP addresses an organization or
        company must use for economy and security purposes

    PAT (Port address Translation): An extension to NAT that permits multiple devices
         on a LAN to be mapped to a single public IP address.  The goal is to conserve IP addresses.
         Most home networks use PAT

IPv6 (Internet Protocol version 6): 

    Launched June 6, 2012. Developed to replace IPv4 due
    due to the limitations of addresses.  Uses a 128 bit address. 
    340,282,366,920,938,463,463,374,607,431,768,211,456 available addresses
    or 2^128
    The addresses are complex 
    hhhh.hhhh.hhhh.hhhh.hhhh.hhhh.hhhh.hhhh

Subnet mask: 

    A number that defines a range of IP addresses available within a network.
    Limits the number of valid IPs for a specific network.  Multiple subnet masks
    are used to organize a single network into smaller subnetworks.  Systems within
    the same subnet can communicate directly with each other while systems on different
    subnets must communicate through a router

Default Gateway:  

    The term for a node that provides the outgoing access to data packets
    out of a network.  This is the gateway that is used by default unless another exit off
    of the network is specified. Usually a router.

Windows cmd: 

    ipconfig /all- physical address(MAC), IPv4 IPv6 address subnetmask

Linux cmd: 

    ifconfig -a 

127.0.0.1 (Loop back/Local host/Home):

     Ip address that is a special purpose IPv4 address
    All computers use this address as their own.  It doesn't allow computers to communicate
    with other devices.  Often used for testing

Port:

    Different locations/endpoints reserved for different means to connect to computers.

    Logical:
    At the software level within an operating system a port 
    identifies a specific process or type of network service.  
    Ports are identified for each transport protocol.
    Any Networking process or device uses a specific network port to transmit or receive data
    A port is simply a hole in the processor address space where data can be sent and received
    They are 16 bit unsigned integers that range from 0 to 65535
    
    Well-known port:  numbers are allotted to standard server processes.
    Port numbers 0 - 1023 are "Well-known port numbers"

    Ephemeral ports: port numbers greater than 1024, can be used by anyone
    
    Physical
    Ports that are physical, such as on a router or switch are used for physically connecting media
    between devices.

Socket/sockets: 

    A combination of an IP address and a port number.
    Sockets open a network connection for a program, allowing data to be read or written
    over a network.  Sockets are software.  They make it easy for software developers to create
    network-enabled programs.  Sockets allow programs to use the operating system’s built-in commands
    to handle networking functions.  Because they are used for a number of different network protocols,
    many sockets can be open at one time.

        Socket program flow:
        - Create socket (frequently with IPv4 and TCP)
        - Bind to a host and port
        - Listen or send on that socket
        - Send and receive data
        - Close socket when done

Domain: 

    A network of computers or devices that are controlled by one set authority.
    Within the internet, a domain is controlled by one particular company that has its own
    internet presence and IP address

Collision Domain: 

    Two devices share a network segment and collision of packets can occur if sent simultaneously. For example port to port on a switch.

Broadcast Domain:

    Group of devices on a network segment that can reach each other with Ethernet broadcasts.  Usually separated by a router.
    Every port on a router is in a different broadcast domain. Switches will flood Ethernet broadcast frames out all ports by default.  All ports on a switch, bridge, hub are in the same broadcast domain.

CSMA/CD (arrier Sense, Multiple Access with Collision Detection):

     A network protocol for carrier transmission that operates in the Medium Access Control (MAC) layer. It senses or listens for whether or not the shared channel for transmission is busy and defers transmissions until the channel is free. 

DNS (Domain Name System): 

    Translates domain names, like Google.com to IP addresses, like 8.8.8.8.
    Converts from human friendly to computer friendly.

Packet:

        A container that carries data over a TCP/IP network.  Packets represent the smallest amount of data
        sent over a network.
        A packet contains several pieces of information besides the data it's carrying.
        It includes the source and destination ip addresses and any constraints for quality of 
        service (QoS) handling

        Header: Part of the data packet and contains transparent information about the file or 
        transmission.  
        Supplemental data placed at the beginning of a data block being stored or transmitted.  
        In transmission the data following the header is sometimes called the payload or body.
        The header follows a clear and unambiguous format to allow for parsing.


ICMP (Internet Control Message Protocol): 

    Network Layer protocol used by network devices
    to diagnose network communications issues.  Mainly used to determine whether or not data is reaching its
    intended destination.
    The primary purpose for ICMP is error reporting, such as if a packet is too large for 
    a router and gets dropped.  The router will send an ICMP message back to the source of the data.
    The secondary use is for network diagnostics.  
    Common uses are the commands traceroute and ping.
    Traceroute - displays routing path, the actual physical path.
    Ping - is used to test the speed of connection between devices.

ARP (Address Resolution protocol): 

    Communication protocol used for discovering the link layer address.
    Translates/Maps hardware/MAC address to an IP address.

TCP (Transmission Control Protocol): 

    Connection oriented, guaranteed delivery, reliable
    Fundamental protocol within the Internet Protocol suite.
    (Collection of standards that allow communications over the internet.) Categorized
    as a transport layer protocol.  TCP creates a connection and manages the delivery of packets 
    from one system to another. TCP and IP (TCP/IP) are commonly grouped together.
    Packets can travel over the internet through different routes to the same destination.
    The protocol reorders the packets in the correct sequence on the receiving end.
    It includes error checking (checksums) to ensure integrity of data between server and client.

    Three way Handshake: SYN, SYN-ACK, ACK

        Three way process used in a TCP/IP network to make a connection between the server and client
        Both the client and server exchange synchronization and acknowledgement packets before 
        real data communication process starts.

        TCP Message Types:

        SYN = Synchronization (Initiate and establish connection, synchronize numbers
        between devices)

        ACK = Acknowledge (Confirm to the other side that it received the SYN)

        SYN-ACK = Synchronization Acknowledgement (SYN message from local device and ACK of earlier packet)

        FIN = Finish/Terminate connection

        TCP Process:

            Step 1: This is the first packet from the client to the server. 
            TCP message sets the SYN flag to 1 in the message, so make the TCP message as 
            SYN segment. It has the initial sequence number of the client along with a few more parameters.

            Step 2: After receiving the SYN packet, the server sends the syn ack 
            packet to the client. Not to mention that this is a single TCP 
            packet with syn and ack bit set to 1. The syn sequence number is the 
            initial sequence number of the server accepting the connection.  
            The ack part has the client sequence number plus one. 
            This way server tells that it is ready to accept the packet with 
            the next sequence number from the client.

            Step 3: The final packet for the connection setup is TCP ack. 
            The client sends a TCP ack packet upon receiving TCP syn ack from the 
            server. The packet includes a sequence number from the server plus one.

            Use examples: Email, File sharing, Downloading

UDP (User Datagram Protocol): 

    Connectionless protocol, Best effort
    Limited error checking, no data recovery for lost packets. Benefits applications using
    UDP due to less overhead, (acknowledgements, synchronization traffic)
    Does not offer data recovery for packet loss or retransmission.
    There is some error checking in the form of checksums that go along with the packet
    to verify integrity. There is a pseudo header or small header that includes source and destination ports.
    If the service is not running on a specific machine, UDP returns an error message.
    
    Use examples: Voice streaming, Video streaming, Real-time services.


DHCP (Dynamic Host Configuration Protocol): 

    Protocol that provides quick, automatic and central management
    for the distribution of IP addresses within a network.  Also used to configure the subnet mask,
    default gateway and DNS server information on the device.

HTTP (Hyper Text Transfer Protocol):

    Underlying protocol used by the World Wide Web. 
    Defines how messages are formatted and transmitted and what actions Web servers and browsers 
    should take in response to various commands.

SMTP (Simple Mail Transfer Protocol): 

    Used for sending e-mail over the internet.  
    A set of commands to authenticate and direct the transfer of electronic mail.
    
SSH (Secure Shell): 

    A method of securely communicating with another computer.  All data sent
    via an SSH connection is encrypted

Telnet: 

    Communication protocol that provides specifications for emulating/imitate a remote computer terminal. Not secure


API (Application Programming Interface):  

    Computing interface that defines interactions between multiple software applications
    or mixed hardware-software intermediaries.

BSD Socket: 

    Sockets originated with ARPANET in 1971.  Later became an API in the Berkeley Software Distribution (BSD)
    Operating system released in 1983 called Berkley sockets.
    Berkeley sockets aka BSD sockets are a common type of socket application  for client-server applications
    where one side acts as the server and waits for a connection from a client.
    Also known as Internet sockets.

Encapsulation: 

    Process by which lower-layer protocols receive data from higher layer protocols 
    and places the data into the data portion of its frame.   
    Thus, encapsulation is the process of enclosing one type of packet using another type of packet.
    he lowest levels of the Open Systems Interconnection (OSI) reference model is sometimes referred 
    to as framing. Examples of encapsulation include the following:

    An Ethernet frame that encapsulates an Internet Protocol (IP) packet, which itself encapsulates a 
    Transmission Control Protocol (TCP) packet, which then encapsulates the actual data being transmitted over the network
    An Ethernet frame encapsulated in an Asynchronous Transfer Mode (ATM) frame for transmission over an ATM backbone

    The data-link layer (layer 2) of the OSI model for networking is responsible for encapsulation or
    framing of data for transmission over the physical medium.

    Layer 1 encapsulation: Bit or Data Stream
    Layer 2 encapsulation: Frame
    Layer 3 encapsulation: Packet or Datagram
    Layer 4 encapsulation: Segment
    Layer 5 encapsulation: Data


