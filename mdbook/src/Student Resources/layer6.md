OSI Layer 6 - Presentation Layer

Falls within the Application Layer of the TCP/IP Model

- Serves as the data translator for the network.
- Is sometimes called the syntax layer.

- It is the bridge between the Application Layer (Layer 7) and the Session Layer (Layer 5)
-- It responds to service requests from the application layer and issues service requests to the session layer

- The presentation layer ensures the information that the application layer of one system sends out is readable 
  by the application layer of another system. 
-- On the sending system it is responsible for conversion to standard, transmittable formats.
-- On the receiving system it is responsible for the translation, formatting, and delivery of information for processing or display.

- In theory, it relieves application layer protocols of concern regarding syntactical differences in data representation 
  within the end-user systems.
- An example of a presentation service would be the conversion of an EBCDIC-coded text computer file to an ASCII-coded file.
  (EBCDIC = extended binary coded decimal interchange code) 

- In many widely used applications and protocols no distinction is actually made between the presentation and application layers.  
-- For example, HyperText Transfer Protocol (HTTP), generally regarded as an application-layer protocol, has presentation-layer aspects 
   such as the ability to identify character encoding for proper conversion, which is then done in the application layer.

- Serialization of complex data structures into flat byte-strings can be thought of as the key functionality of the presentation layer. 
- Structure representation is normally standardized at this level, often by using XML. 
- As well as simple pieces of data, like strings, more complicated things are standardized in this layer. 

- Encryption and Decryption are typically done at this level too, although it can be done on the application, session, transport, 
  or network layers, each having its own advantages and disadvantages. For example, when logging on to bank account sites the 
  presentation layer will decrypt the data as it is received

- Services
-- Data conversion
-- Character code translation
-- Compression
-- Encryption and Decryption
-- Serialization

- Protocols
Here are some Protocols sometimes considered at this level (though perhaps not strictly adhering to the OSI model) include:
-- Apple Filing Protocol (AFP)
-- Independent Computing Architecture (ICA), the Citrix system core protocol
-- Lightweight Presentation Protocol (LPP)
-- NetWare Core Protocol (NCP)
-- Network Data Representation (NDR)
-- Tox, The Tox protocol is sometimes regarded as part of both the presentation and application layer
-- eXternal Data Representation (XDR)
-- X.25 Packet Assembler/Disassembler Protocol (PAD)
