print()
text = input("Enter some text: ")
key = input("Enter the key: ")
print()
length = len(text)

#t = text
#k = key
#x = xor value
#char(x) = character value
print("t k x chr(x)")
for i in range(length):
    t = text[i]
    #use modulus so you don't go out of bounds for the key
    k = key[i%len(key)]
    #xor the bytes of each character against the key
    x = ord(k) ^ ord(t)
    print(t,  k,  x,  chr(x))
