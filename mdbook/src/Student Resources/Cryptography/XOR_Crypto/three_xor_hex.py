import binascii

#print the results in hex

print()
text = input("Enter some text: ")
key = input("Enter the key: ")
print()
length = len(text)

#variable to add our coded characters to
cipher = ""

#t = text
#k = key
#x = xor value
#char(x) = character value
print("t k x chr(x)")
for i in range(length):
    t = text[i]
    #use modulus so you don't go out of bounds for the key
    k = key[i%len(key)]
    #xor the bytes of each character against the key
    x = ord(k) ^ ord(t)
    cipher += chr(x)

#close but still in binary
#results in binary representation of hex
# print(text,  key,  (binascii.b2a_hex(cipher.encode()))) 

#executes correctly
#results in binary representation of hex then decodes the binary so it is hex
#print(text,  key,  (binascii.b2a_hex(cipher.encode())).decode())

#NOPE
#doesn't work
#print(text,  key,  cipher.encode("hex")) 

print(text, key, cipher)



