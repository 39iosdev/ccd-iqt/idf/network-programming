""" don't know the key """ 
print()

#get input from the user
text = input("Enter some text: ")
#enter snw{fzs
#get the length of the input 
length = len(text)


#t = text
#k = key
#x = xor value
#char(x) = character value
#use k as the iterator over the string of numbers 0 - 9
for k in "0123456789":
    #create an empty string to add to
    cipher = ""
    #use i to iterate for the count of the length of the user input
    for i in range(length):
        #assign the element at the index in the user input to t
        t = text[i]
        #assign to x the value from xoring the bytes of each character against the key which is iterating through 0 - 9
        x = ord(k) ^ ord(t)
        #concatenate the letter xor'd by casting to a char and adding to the cipher string
        cipher += chr(x)
    #print the current iteration and the current cipher string
    print(k, cipher)


