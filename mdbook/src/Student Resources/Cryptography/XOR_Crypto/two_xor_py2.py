""" python 2 """ 
print()
text = raw_input("Enter some text: ")
key = raw_input("Enter the key: ")
print()
length = len(text)

cipher = ""
print("t k x chr(x)")
for i in range(length):
    t = text[i]
    #use modulus so you don't go out of bounds for the key
    k = key[i%len(key)]
    #xor the bytes of each character against the key
    x = ord(k) ^ ord(t)
    cipher += chr(x)
print(text,  key,  cipher.encode("hex"))#this line may not work in py2, i was unable to test

""" in python 2 raw_input is used to make sure the value returned is a string 
    not using raw_input in python 2 is a security risk
    input() by itself in python2 evaultes before returning
    so a user could enter something such as exit()
    and would cause the program to exit, thus causing unexpected behavior
    The user would also be able to access things within the file this way
    ex:
    >>> input("Enter something else :")
        Enter something else :__import__("os").listdir('.')
        ['.gtkrc-1.2-gnome2', ...]

        This would list out the contents of the current dirctory
        it would also give access to functions such as os.chdir, os.remove, os.removedirs, os.remdir
        very very dangerous!!

"""
