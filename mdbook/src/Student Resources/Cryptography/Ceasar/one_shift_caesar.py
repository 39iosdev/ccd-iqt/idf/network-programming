#The key
alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

#get the string to encrypt from the user
string_input = input("Enter a string: ").upper()
#let the user input how many to shift
shift_input = int(input("Enter a value to shift by: "))


#get the length of the string
input_length = len(string_input)
print("String length is: ", input_length)

#instantiate a string variable to append the encrypted data to
string_output = ""


for i in range(input_length):
    #the character that is at the index of i
    character = string_input[i]
    print("The character is: ", character)

    localchar = alphabets.find(character)
    print("The local character is: ", localchar)
    newloc = localchar + shift_input
    print("The new location is: ", newloc)
    string_output = string_output + alphabets[newloc]
    print(string_output)

print("Encrypted text: ", string_output)


"""  Works fine unless we end up going out of range of our key
    If we enter dog and shift 33 places it will go out of range """


""" 
Python find method: 
    the find() method finds the first occurrence of the specified value.

The find() method returns -1 if the value is not found.

The find() method is almost the same as the index() method, 
the only difference is that the index() method raises an exception 
if the value is not found. (See example below)

syntax: 
    string.find(value, start, end)
        value = required -value searching for
        start = optional - where to start the search, default of 0
        end = optional - default is to the end of the string

"""