alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

#get the string to encrypt from the user
string_input = input("Enter a string: ").upper()
#let the user input how manyc to shift
shift_input = int(input("Enter a value to shift by: "))


#get the length of the string
input_length = len(string_input)
print("String length is: ", input_length)

#instantiate a string variable to append the encrypted data to
string_output = ""


for i in range(input_length):
    #the character that is at the index of i
    character = string_input[i]
    print("The character is: ", character)

    localchar = alphabets.find(character)
    print("The local character is: ", localchar)
    #use modulus to keep the location from being out of range
    newloc = (localchar + shift_input) % 26
    print("The new location is: ", newloc)
    string_output = string_output + alphabets[newloc]
    print(string_output)

print("Encrypted text: ", string_output)

"""  We can use modulus to fix this so it doesn't go out of range, even with absurd numbers """