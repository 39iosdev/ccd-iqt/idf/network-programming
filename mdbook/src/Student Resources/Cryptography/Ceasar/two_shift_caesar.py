#The key
alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

#get the string to encrypt from the user
string_input = input("Enter a string: ").upper()
#let the user input how many to shift
shift_input = int(input("Enter a value to shift by: "))


#get the length of the string
input_length = len(string_input)
print("String length is: ", input_length)

#instantiate a string variable to append the encrypted data to
string_output = ""


for i in range(input_length):
    #the character that is at the index of i
    character = string_input[i]
    print("The character is: ", character)

    localchar = alphabets.find(character)
    print("The local character is: ", localchar)
    newloc = localchar + shift_input
    #check if the new location is within range if not make it in range
    if newloc >= 26:
        newloc -= 26
    print("The new location is: ", newloc)
    string_output = string_output + alphabets[newloc]
    print(string_output)

print("Encrypted text: ", string_output)


"""  Still has the possiblity of being out of range 
    if we enter dog and shift 44 it is out of range
    dog actually goes out of range with a shift of 38 and -30
    """