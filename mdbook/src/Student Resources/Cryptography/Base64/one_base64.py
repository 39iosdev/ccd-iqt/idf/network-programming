import base64

""""""""""""""""encode"""""""""""""""""""""

#stores input string to be encoded
message = "Python is fun"
print(f"This is the original message: {message}")

#convert to bytes like object using strings encode method
message_bytes = message.encode()
print(f"This is the encoded message to bytes: {message_bytes}")

#base64 encode the bytes like object
base64_bytes = base64.b64encode(message_bytes)
print(f"This is the message being encoded into base64: {base64_bytes}")


#decode to get the string representation of the encoded message
base64_message = base64_bytes.decode()
print("This is the encoded message decoded to create a string type message: ", base64_message)
#results UHl0aG9uIGlzIGZ1bg==
#the extra == are used for padding. 
# one equal sign means one byte of padding

""""""""""""""""""""""""""""""""""decode"""""""""""""""""""""""
print("############## Decoding the message ##############################")
new_base64_message = 'UHl0aG9uIGlzIGZ1bg=='

#encode into a bytes like object
new_base64_bytes = new_base64_message.encode()

#decode the base64 bytes
new_message_bytes = base64.b64decode(new_base64_bytes)
print("This is the message decoded from base64 to binary", new_message_bytes)

#decode into a string object
new_message = new_message_bytes.decode()
print("This is the message decoded: ", new_message)


"""
For more information:
 https://docs.python.org/3/library/base64.html
"""