""" encode an image """

import base64

#open the file and read as binary
with open("smile.png", 'rb') as binary_file:
    #get all the data from the file into the variable
    binary_file_data = binary_file.read()
    #encode the bytes with b64
    base64_encoded_data = base64.b64encode(binary_file_data)
    #use decode utf-8 on the b64 encoded data to get human readable charcters
    base64_message = base64_encoded_data.decode('utf-8')

    print(base64_message)



""" 
decode an image

decode_binary_img
When you are base64 decoding a binary file, you must know the
type of data that is being decoded. For example, this data is
only valid as a PNG file and not a MP3 file as it encodes an 
image
"""

# base64_img = 'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAA' \
#             'LEwEAmpwYAAAB1klEQVQ4jY2TTUhUURTHf+fy/HrjhNEX2KRGiyIXg8xgSURuokX' \
#             'LxFW0qDTaSQupkHirthK0qF0WQQQR0UCbwCQyw8KCiDbShEYLJQdmpsk3895p4aS' \
#             'v92ass7pcfv/zP+fcc4U6kXKe2pTY3tjSUHjtnFgB0VqchC/SY8/293S23f+6VEj' \
#             '9KKwCoPDNIJdmr598GOZNJKNWTic7tqb27WwNuuwGvVWrAit84fsmMzE1P1+1TiK' \
#             'MVKvYUjdBvzPZXCwXzyhyWNBgVYkgrIow09VJMznpyebWE+Tdn9cEroBSc1JVPS+' \
#             '6moh5Xyjj65vEgBxafGzWetTh+rr1eE/c/TMYg8hlAOvI6JP4KmwLgJ4qD0TIbli' \
#             'TB+sunjkbeLekKsZ6Zc8V027aBRoBRHVoduDiSypmGFG7CrcBEyDHA0ZNfNphC0D' \
#             '6amYa6ANw3YbWD4Pn3oIc+EdL36V3od0A+MaMAXmA8x2Zyn+IQeQeBDfRcUw3B+2' \
#             'PxwZ/EdtTDpCPQLMh9TKx0k3pXipEVlknsf5KoNzGyOe1sz8nvYtTQT6yyvTjIax' \
#             'smHGB9pFx4n3jIEfDePQvCIrnn0J4B/gA5J4XcRfu4JZuRAw3C51OtOjM3l2bMb8' \
#             'Br5eXCsT/w/EAAAAASUVORK5CYII='

# #convert the base64 string data into a bytes like object
# base64_img_bytes = base64_img.encode('utf-8')
# #open the file as writing with bytes
# with open('decoded_image.png', 'wb') as file_to_save:
#     #decode the data with base64.decodebytes to decode binary
#     decoded_image_data = base64.decodebytes(base64_img_bytes)
#     #write decoded data to file
#     file_to_save.write(decoded_image_data)