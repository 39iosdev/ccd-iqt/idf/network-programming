import json

#list that contains a dictionary
#the dictionary contains three key value pairs, key 'a' = value 'A', key 'b' = value tuple of 2,4, key 'c' = value of float 3.0
data = [{'a': 'A', 'b': (2, 4), 'c': 3.0}]


######################### list before encoding ################################
print("\nThis is the data in the list: \n")
print('DATA   :', data)
print(f"\nThe data is of type: {type(data)}\n")


######################### encode and make json string with dumps() ################################
# json.dumps() creates/encodes a JSON string from the data passed in into a new object. 
# It looks like a Python dictionary with double quotes around it
print("\nEncoding the list with json.dumps()\n")
***Insert Code***
print("\nThis is the endcode list in json\n")
print('ENCODED:', data_string)
print(f"\nThe object created from encoding with json.dumps() is now of type: {type(data_string)}\n")


######################### decode a json string into a python object with loads() ################################
# json.loads() takes a JSON String and decodes into a new object.
print("\nDecoding the encoded object with jsoon.loads()\n")
***Insert Code***
print("\nThis is the decoded object\n")
print('DECODED:', decoded)
print(f"\nThe decoded is of type: {type(decoded)}")


##################### displaying the inner parts of the original list before encoding and decoding #############################

print("Encoding, then re-decoding may not give exactly the same type of object.")
print()
print('ORIGINAL:')
print(f"This is the type for the value of key 'a': {type(data[0]['a'])}")
print(f"This is the type for the value of key 'b': {type(data[0]['b'])}")
print(f"This is the type for the value of key 'c': {type(data[0]['c'])} \n")
print()

##################### displaying the inner parts of the list after encoding and decoding #############################
print('DECODED :')
print(f"This is the type for the value of key 'a': {type(decoded[0]['a'])}")
print(f"This is the type for the value of key 'b': {type(decoded[0]['b'])}")
print(f"This is the type for the value of key 'c': {type(decoded[0]['c'])} \n")
print("\nIn particular, tuples become lists")