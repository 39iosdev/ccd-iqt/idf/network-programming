import json


################################### print the original data and type #########################################
data = [{'a': 'A', 'b': (2, 4), 'c': 3.0}]
print('DATA:', ***Insert Code***)
first_type = type(data)
print(f"\nThe data is of type: {type(data)}\n")

################################### encode with json.dumps and print the new object and type #########################################
# json.dumps() creates/encodes a JSON string from the data passed in. 
# It looks like a Python dictionary with quotes around it
print("\n Encoding with json.dumps()\n")
***Insert Code***
print('JSON:', data_string)

print(f"The new data_string is of type: {type(data_string)}\n")


# The repr() function takes a single parameter:
# The repr() function returns a printable representational string of the given object.

