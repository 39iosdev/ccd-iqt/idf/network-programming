import json

data = [{'a': 'A', 'c': 3.0, 'b': (2, 4)}]
print("\nThis is the original data")
print('\nDATA:',***Insert Code***)
print(f"The data is of type: {type(data)}\n")


#creates the json string

unsorted = ***Insert Code***
print("\nPrinting the encoded list\n")
print(f'JSON:', {unsorted})
print(f"\nThe unsorted list from the json.dump encoding is of type: {type(unsorted)}\n")


#sort_keys argument sorts the keys of a dictionary.
print("Printing the sorted list with json.dumps sort_keys\n")
print('SORT:', ***Insert Code***)

print("Creating two different sorted objects with json dumps from the original data")
first = json.dumps(data, sort_keys=True)
print(f"The type of the first newly created object is: {type(first)}")
second = json.dumps(data, sort_keys=True)
print(f"The type of the second newly created object is: {type(second)}")

print("\nComparing the first newly created object of 'first' with the data")
print('UNSORTED MATCH:', unsorted == first)
#returns false because of differnt types, list vs string and in different order


print("\nComparing the first newly created object 'first' with the second newly created object 'second'")
print('SORTED MATCH  :', first == second)
#returns true because of the same type and contain the same info in the same order