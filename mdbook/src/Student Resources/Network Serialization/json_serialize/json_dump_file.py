import io
import json

#create a list that contains a dictionary
data = [{'a': 'A', 'b': (2, 4), 'c': 3.0}]
print(f"\nThis is the data before encoding: {data}\n")


#create a string stream
***Insert Code***

#encode the data into the string stream to simulate a file
***Insert Code***

#print the values of the stream
print("Printing the values after encoding with json.dump")
***Insert Code***

# dump = encode
# The convenience functions load() and dump() accept references to a file-like object to use for reading or writing.
# A socket or normal file handle would work the same way as the StringIO buffer used in this example.

"""
The StringIO module is an in-memory file-like object. This object can be used as 
input or output to most functions that would expect a standard file object. 
When the StringIO object is created it is initialized by passing a string to the constructer. 
If no string is passed the StringIO will start empty. In both cases, the initial cursor on the file starts at zero

Use io.StringIO for handling unicode objects
Use io.BytesIO for handling bytes objects
"""
    