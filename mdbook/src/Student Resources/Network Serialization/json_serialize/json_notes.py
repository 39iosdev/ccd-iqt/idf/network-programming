"""
JSON is 
JavaScript Object Notation. It's a series of key-value pairs. 
The key-value pairs may be nested.

JSON is used for data encoding and decoding. 
It is commonly used on the web when interacting with Javascript 
but unlike Pickle, it has the benefit of having implementation 
in other languages wich makes it suitable for inter application communication.

Both JSON and XML can be used to receive data from a webserver.
JSON is sometimes preferred over XML because it's less verbose and faster to parse.
Syntax is almost identical to a Python dict

The json module provides an API similar to pickle for converting in-memory Python objects 
to a serialized representation 

It is most widely used for communicating between the web server and client in a REST API.  

"""