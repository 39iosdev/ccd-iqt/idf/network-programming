"""
Pickle - 
Pickle is used commonly for persistance because it is integrated with 
some other standard libraries that store serialized data
Python specific

The pickle module implements an algorithm for turning an arbitrary Python object into a series of bytes. 
This process is also called serializing the object. 
The byte stream representing the object can then be transmitted or stored, and later reconstructed 
to create a new object with the same characteristics.

Just to be clear.  pickling offers no sequrity guarantees.  Unpickling data can execute aritrary code.  Do NOT trust data that cannot be verified as secure.

pickle module: implements binary protocols for serializing and de-serializing 
a python object tructure
    pickling = converts python object hierarchy into a byte stream
    unpickling = byte stram from binary file or bytes like object is converted back 
    into an object hierarchy

dump vs dumps:
used during pickling process

dump() - creates a file containing the serialized result
dumps() - is used to encode the data structure as a string. then prints the string to the console *s in dumps is for string*.
 
load vs loads:
used during the unpickling process
load() - reads a file to start the unpickling process
loads() - operates on a string *The s in loads is for string*




"""