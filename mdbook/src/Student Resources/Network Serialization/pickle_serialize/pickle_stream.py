import io
import pickle
import pprint


class SimpleObject:

    def __init__(self, name):
        self.name = name
        self.name_backwards = name[::-1]
        return


data = []
data.append(SimpleObject('pickle'))
data.append(SimpleObject('preserve'))
data.append(SimpleObject('last'))

# Simulate a file.
out_s = io.BytesIO()

# Write to the stream
for o in data:
    print('WRITING : {} ({})'.format(o.name, o.name_backwards))
    ***Insert Code***(o, out_s)
    out_s.flush()

# Set up a read-able stream
***Insert Code***

# Read the data
while True:
    try:
       ***Insert Code***
    except EOFError:
        break
    else:
        print('READ    : {} ({})'.format(
           ***Insert Code***