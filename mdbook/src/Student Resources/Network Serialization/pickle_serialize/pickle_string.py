import pickle
import pprint

# dumps() is used to encode the data structure as a string. then prints the string to the console.
data = [{'a': 'A', 'b': 2, 'c': 3.0}]
print('DATA:', end=' ')
pprint.pprint(data)
#print the type of data before encoding with dump

print(f"data is of type: {type(data)}\n")


***Insert Code***
print('PICKLE: {!r}'.format(data_string))
print()
#print the type after encoding with dump

print(f"data_string is of type: {type(data_string)}\n")


# Return value from repr()
# The repr() function returns a printable representational string of the given object.
# !r chooses repr to format the value.

# The pprint module provides a capability to “pretty-print” arbitrary Python data structures
# in a form which can be used as input to the interpreter. 
# If the formatted structures include objects which are not fundamental Python types, 
# the representation may not be loadable. This may be the case if objects such as files, 
# sockets or classes are included, as well as many other objects which are not representable 
# as Python literals.
"""
The pickle module provides the following functions to make the pickling process more convenient:

pickle.dump(obj, file, protocol=None, *, fix_imports=True, buffer_callback=None)
Write the pickled representation of the object obj to the open file object file. This is equivalent to Pickler(file, protocol).dump(obj).

Arguments file, protocol, fix_imports and buffer_callback have the same meaning as in the Pickler constructor.

Changed in version 3.8: The buffer_callback argument was added.

pickle.dumps(obj, protocol=None, *, fix_imports=True, buffer_callback=None)
Return the pickled representation of the object obj as a bytes object, instead of writing it to a file.

Arguments protocol, fix_imports and buffer_callback have the same meaning as in the Pickler constructor.
"""