import pickle
import pprint

data1 = [{'a': 'A', 'b': 2, 'c': 3.0}]
print('BEFORE: ', end=' ')
pprint.pprint(data1)

data1_string = ***Insert Code***
#print the type after pickling/encoding

print(f"data1_string type is: {type(data1_string)}\n")


data2 = ***Insert Code***
print('AFTER : ', end=' ')
pprint.pprint(data2)

#print the type after unpickling/loads/decode
print(f"data2 type is: {type(data2)}\n")


print('SAME? :', (***Insert Code***))
print('EQUAL?:', (***Insert Code***))


"""
pickle.load(file, *, fix_imports=True, encoding="ASCII", errors="strict", buffers=None)
Read the pickled representation of an object from the open file object file and return the reconstituted object hierarchy specified therein. This is equivalent to Unpickler(file).load().

The protocol version of the pickle is detected automatically, so no protocol argument is needed. Bytes past the pickled representation of the object are ignored.

Arguments file, fix_imports, encoding, errors, strict and buffers have the same meaning as in the Unpickler constructor.

Changed in version 3.8: The buffers argument was added.

pickle.loads(data, /, *, fix_imports=True, encoding="ASCII", errors="strict", buffers=None)
Return the reconstituted object hierarchy of the pickled representation data of an object. data must be a bytes-like object.

The protocol version of the pickle is detected automatically, so no protocol argument is needed. Bytes past the pickled representation of the object are ignored.

Arguments file, fix_imports, encoding, errors, strict and buffers have the same meaning as in the Unpickler constructor.

"""