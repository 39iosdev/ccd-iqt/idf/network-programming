import struct
import binascii

values = (***Insert Code***)
print ('Original values:', values)

endianness =  [
    ('@', 'native, native'),
    ('=', 'native, standard'),
    ('<', 'little-endian'),
    ('>', 'big-endian'),
    ('!', 'network'),
    ]

for code, name in endianness:
    s = struct.Struct(***Insert Code***)
    packed_data = s.pack(***Insert Code***)
    print()
    print ('Format string  :',***Insert Code***)
    print ('Uses           :', ***Insert Code***)
    print ('Packed Value in hex representation  :',***Insert Code***)
    print ('Packed Value in binary  :', ***Insert Code***)
    print ('Unpacked Value :', ***Insert Code***)