import socket

def convert_integer():
    data = 1234
    #32bit
    print()
    print("Originial: %s => Network to host long byte order: %s,  Host to network long byte order: %s" %(***Insert Code***) 
    #16bit
    print("Original: %s =>  Network to host short byte order: %s, Host to network short byte order: %s" %(***Insert Code***) 
    print()
if __name__ == '__main__':
    convert_integer()
"""

Endianness be used to describe the order in which the bits are transmitted over a communication channel
Big-endianness is the dominant ordering in networking protocols, such as in the internet protocol suite, 
where it is referred to as network order, transmitting the most significant byte first.
https://docs.python.org/3/library/socket.html
                                      
https://pythontic.com/modules/socket/byteordering-coversion-functions

    ntohl = network to host long
        socket.ntohl(Int32bit_postive)
        socket.ntohl(x)
        Convert 32-bit positive integers from network to host byte order. 
        On machines where the host byte order is the same as network byte order, 
        this is a no-op; otherwise, it performs a 4-byte swap operation.
        Big Endian to Little Endian

     
    htonl = host to network long
        socket.htonl(Int32bit_postive)
        socket.htonl(x)
        Convert 32-bit positive integers from host to network byte order. 
        On machines where the host byte order is the same as network byte order, 
        this is a no-op; otherwise, it performs a 4-byte swap operation
        Little Endian to Big Endian

    
    ntohs = network to host short
        socket.ntohs(Int32bit_postive)
        socket.ntohs(x)
        Convert 16-bit positive integers from network to host byte order. 
        On machines where the host byte order is the same as network byte order, 
        this is a no-op; otherwise, it performs a 2-byte swap operation.

        Deprecated since version 3.7: In case x does not fit in 16-bit unsigned integer, 
        but does fit in a positive C int, it is silently truncated to 16-bit unsigned integer. 
        This silent truncation feature is deprecated, and will raise an exception in future versions of Python.
        Big Endian to Little Endian

    htons = host to network short
        socket.htons(Int16bit_postive)
        socket.htons(x)
        Convert 16-bit positive integers from host to network byte order. 
        On machines where the host byte order is the same as network byte order, 
        this is a no-op; otherwise, it performs a 2-byte swap operation.

        Deprecated since version 3.7: In case x does not fit in 16-bit unsigned integer, 
        but does fit in a positive C int, it is silently truncated to 16-bit unsigned integer. 
        This silent truncation feature is deprecated, and will raise an exception in future versions of Python.
        Little Endian to Big Endian

"""