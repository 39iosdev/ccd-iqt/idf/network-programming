import struct 
import binascii

packed_data = ***Insert Code***('0100000061620000cdcc2c40')

s = struct.Struct(***Insert Code***)
unpacked_data = ***Insert Code***
print('Unpacked Values:', unpacked_data)

# If we pass the packed value to unpack(),
#  we get basically the same values back 
# (note the discrepancy in the floating point value, it previously was 2.7)