import struct
import binascii
# In this example, the format specifier calls for an integer or long value,
# a two character string, and a floating point number. The spaces between 
# the format specifiers are included here for clarity, and are ignored when the format is compiled.

values = (***Insert Code***)
#tells how to format the struct
#https://docs.python.org/3/library/struct.html
s = struct.Struct(***Insert Code***)
packed_data = s.pack(***Insert Code***)

print( 'Original values:', ***Insert Code***)
print ('Format string  :', ***Insert Code***)
print ('Uses           :', ***Insert Code***)
# converts the packed value to a sequence of hex bytes for printing with binascii.hexlify(), since some of the characters are nulls.
print ('Packed Value   :', ***Insert Code***(packed_data))
