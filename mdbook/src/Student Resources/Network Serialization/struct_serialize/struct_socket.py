"""  client """ 

import binascii
import socket
import struct
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('localhost', 10000)
sock.connect(server_address)

values = (1, 'ab', 2.7)
packer = struct.Struct('I 2s f')
packed_data = packer.pack(*values)

try:
    
    # Send data
    print('sending "%s"' % binascii.hexlify(packed_data), values)
    sock.sendall(packed_data)

finally:
    print('closing socket')
    sock.close()

"""  server """

import binascii
import socket
import struct
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('localhost', 10000)
sock.bind(server_address)
sock.listen(1)

unpacker = struct.Struct('I 2s f')

while True:
    print("Waiting for connection") 
    connection, client_address = sock.accept()
    try:
        data = connection.recv(unpacker.size)
        print('received "%s"' % binascii.hexlify(data))

        unpacked_data = unpacker.unpack(data)
        print('unpacked:', unpacked_data)
        
    finally:
        connection.close()