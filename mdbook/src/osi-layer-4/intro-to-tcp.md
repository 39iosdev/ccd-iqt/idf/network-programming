
# Intro to TCP

[![TCP/IP Model](../assets/tcpippacket.png)](https://www.computerhope.com/jargon/t/tcpip.htm)


The Transmission Control Protocol is connection oriented, provides error checking, and reliability of communication.

Most protocols commonly used on the internet use TCP including: HTTP, SMTP, and SSH.

TCP connections open with a handshake, data is transmitted, and then the connection is closed with a tear down.

TCP provides reliable transfer via:

* Correctly ordering packets received in arbitrary order.
* Validating received packets were not corrupt.
* Re-requesting packets that were corrupt or not received at all.
* Flow and congestion control.
* Requires positive acknowledgement before next data transmission.

---

---

## TCP Header and Flags

![TCP Header](../assets/MJB-TCP-Header-800x564.png)

**Source Port** – sending port

**Destination Port** – Receiving port

**Sequence Number** – Initially random. Each new transmission adds the size of the data

**Acknowledgment Number** – The next byte expected to be received.

**Data offset** - Size of TCP header in 32bit words

**Reserved** – 0's

**Flags** – Bit mask of all TCP flags

**Window size** – Max number of bytes receiver can handle

**Checksum** – Checksum of header and data

**Urgent Pointer** – Only valid if URG flag set

**Options** – Allows for expanded uses

![](../assets/tcphead.PNG)

### TCP uses a bit field to represent flags.

**NS - Nonce Sum**, used to counter an old method for an attacker attempting to "hide" traffic.

**CWR - Congestion Window Reduced**, an acknowledgement of congestion notification \(ECE\).

**ECE - Explicit Congestion Notification**, notifies sender of network congestion, or that the remote side is ENC capable \(if sent along with SYN\).

**URG - Urgent field**, is valid in this transmission.

**ACK** - **Acknowledgement field**, is on almost all transmissions except for initial SYN.

**PSH** - Requests data be pushed to application.

**RST** - Resets the connection.

**SYN** - Requests a Seq No sync.

**FIN** - No more data to send.

## PSH vs URG

**PSH** - Force the receiver to push all data to the process immediately. Used for interactive things where keystrokes should be processed relatively quickly by Layer 7. Netcat does this to force writing to the terminal. Telnet is another example.

**URG** - Not well documented or implemented, but the urgent pointer indicates how much data is urgent at the beginning of the segment. Only the urgent data should be provided to the process in an out-of-band manner.

Neither has an API to actually set the value.

---

---

## SYN and ACK

Sequence numbers indicate the most recent piece of data sent.

Acknowledgement numbers indicate the next byte expected.

**During the initial handshake:**

* Host A randomly generates a sequence number.
* Host B will acknowledge A's seq number, and generate a randomseqno of it's own.
* Host A will acknowledge B's seq number and the connection is established.

**With NO DATA transmitted \(e.g. keep alive packet, or a one-sided series of transactions\):**

* A sender that is simply ACKing and not sending data of it's own, will re-use the same Sequence Number.
* Sequence Numbers may be re-acked until other side actually sends some data.

**During normal data transmission:**

* Sequence numbers mark the beginning of most recent data sent. It starts at the random number chosen during the handshake and is cumulative.
* Acknowledgement numbers =seqnumber + datarecv'd since last ACK + 1.

**Errors will result in the Acknowledgement number being set to the last successfully received CONTIGUOUS block of data + 1.**

* A sends 10 data, with as eq of 1, but B only receives 1-7, the Ack number will be 8.
* A sends 10 data, with as eq of 1, but B only receives 1 and 4-10, the Ack number will be 2.

---

## SACK

RFCs 1017 and 2018 introduced/refined selective acknowledgements. It allows all segments received properly to be ACKed, resulting in only the missing/wrong data packet being re-sent.

To facilitate this, a SACK TCP Option header must be created and appended to the TCP header. Negotiation for the use of SACK is done at the beginning of the TCP connection.

It uses a "left edge" and "right edge" to pinpoint the missing data.

The ACKno will be the same as a non-SACK connection. The appended header will also include the SACK option for TCP indicating the other data it received. The sender may then re-transmit the missing data.

---

## TCP Handshake

![](../assets/handshake.png)

---

## TCP Teardown

![](../assets/teardown.png)

---

## TCP State

TCP sockets have numerous states. Many of these can be seen in the output of a properly timed `netstat` command.

## **Handshake states:**

* LISTEN: Awaiting a SYN
* SYN-Sent: A client has sent the SYN, and is awaiting the SYNACK
* SYN-RECIEVED: A server has received and a SYN, sent a SYNACK, and is awaiting the ACK

## **Data states:**

* **ESTABLISHED**: Normal data transmission occurring.
* **FIN-WAIT-1**: Client FIN sent, awaiting acknowledgement from Server. May still receive data.
* **FIN-WAIT-2**: Client FIN acknowledged by Server \(half closed\), awaiting Server FIN. May still receive data.
* **CLOSE\_WAIT**: Client FIN received and acknowledged by Server. Waiting for Server to send it's own FIN \(This is the server view of FIN-WAIT-1 & 2\).
* **LAST-ACK**: Server sends FIN. Awaiting Client to acknowledge it for teardown.
* **TIME-WAIT**: There is a "cooldown" period before the socket can be closed for good.
* **CLOSED**: No more connection.

**TIME-WAIT** is why you occasionally get errors that a port is in use when rapidly re-running code that creates network socket.

You can use socket options to solve this.

---

## RST

Both RSTs and FINs \(eventually\) result in a teardown, but the previous state listing did not reference RST at all.

An RST can be thought of as aborting the connection \(i.e. ESTABLISHED directly to TIME-WAIT/CLOSED\).

For those with Linux experience, a good analogy is how 'kill -9' terminates a process regardless of what the process is currently doing.

In TCP, a RST usually takes the place of an ICMP Port Unreachable message for closed ports.

---


## Traffic Control

Window scaling is determined during the handshake and it determines the maximum data to be received before being ACKed.

Flow control in TCP is accomplished using a sliding window. Receivers specify the amount of data that they are willing to receive, and the sender will only send that much before waiting for an ACK. If the window is 0, transmission stops for a timeout to allow the receiver to ACK with a new window.

Congestion is controlled by the TCP stack using timers and various algorithms to estimate round trip time, and will speed up or slow down data based upon that information.

In depth analysis of these are out of the scope of this class.

---
