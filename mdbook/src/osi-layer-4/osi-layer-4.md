## OSI Layer 4
    - KSATS(K0064, K0065)

### Overview

* Introduction to TCP
* Layer 4 Devices
* TCP Header and Flags
* Synchronize and Acknowledgements
* SACK
* TCP Handshake
* TCP Teardown
* TCP State
* Reset
* Traffic Control
* UDP
* UDP Header

### Objectives

* Identify Network Devices at each layer of the OSI model \(Layer 1-4\)
* Send and receive Unicast/Broadcast/Multicast/Anycast traffic
* Use struct to unpack dynamically sized input \(ie use a sequence of unpacks with values depending on previous unpacks\)

---
#### To access the OSI layer 4 slides please click [here](slides)

