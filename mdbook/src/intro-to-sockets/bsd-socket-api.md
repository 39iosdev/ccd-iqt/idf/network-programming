## BSD Socket API

* **Internet Protocol Suite**

![](../assets/ip_stack_connections.svg.png)

[https://en.wikipedia.org/wiki/Internet\_protocol\_suite](https://en.wikipedia.org/wiki/Internet_protocol_suite)

![](../assets/ineternet-socket.png)

Sockets are the abstraction that allow applications to communicate with each other. Socket programming allows us to control the functionality of the interface between ports and applications. Another way to think of them, is they're a way to speak to other programs using standard Unix file descriptors. Since everything in Unix/Linux is a file.

A file descriptor is simply an integer associated with an open file. But, that file can be a network connection, a F/IO, a pipe, a terminal, a real on-the-disk file, or just about anything else. Everything in Unix is a file! So when you want to communicate with another program over the Internet you're gonna do it through a file descriptor.

Just like using **read\(\)** and **write\(\) calls** to communicate with a file. You make a call to the  **socket\(\)** system routine. It returns the socket descriptor, and you communicate through it using the specialized **send\(\)** and **recv\(\)** socket calls.

**Berkeley sockets**  are an[ application programming interface](https://en.wikipedia.org/wiki/Application_programming_interface) \(API\) for [Internet sockets](https://en.wikipedia.org/wiki/Internet_socket) and[ Unix domain sockets](https://en.wikipedia.org/wiki/Unix_domain_socket), used for [inter-process communication](https://en.wikipedia.org/wiki/Inter-process_communication) \(IPC\). It is commonly implemented as a[ library](https://en.wikipedia.org/wiki/Library_%28computing%29) of linkable modules. It originated with the 4.2 BSD [Unix](https://en.wikipedia.org/wiki/Unix) released in 1983.

Berkeley sockets evolved with little modification from a [de facto standard](https://en.wikipedia.org/wiki/De_facto_standard) into a component of the[ POSIX](https://en.wikipedia.org/wiki/POSIX) specification. Therefore, the term  **POSIX sockets**  is essentially synonymous with **Berkeley sockets**. They are also known as **BSD sockets**, acknowledging the first implementation in the [Berkeley Software Distribution](https://en.wikipedia.org/wiki/Berkeley_Software_Distribution).

[https://en.wikipedia.org/wiki/Berkeley\_sockets](https://en.wikipedia.org/wiki/Berkeley_sockets)

* **We will be using the BSD Socket API for networking**
* **Knowing BSD sockets will provide a foundation to do networking in almost any language/environment**
* **Default socket API is a string-based protocol**

\(Even Windows' Winsock library still follows the call pattern, even if Microsoft makes their function calls take a dozen more parameters\)

Part of the trouble with understanding network programming is that a “socket” can mean a number of subtly different things, depending on context. So first, let’s make a distinction between a “client” socket - an endpoint of a conversation, and a “server” socket, which is more like a switchboard operator. The client application \(your browser, for example\) uses “client” sockets exclusively; the web server it’s talking to uses both “server” sockets and “client” sockets.

---

## Socket Types

**There are two basic types of communication** 

* Streams \(TCP\):  Computers establish a connection with each other and read/write data in a continuous stream of bytes---like a ﬁle.  This is the most common. 
* Datagrams \(UDP\): Computers send discrete packets \(or messages\) to each other.   Each packet contains a collection of bytes, but each packet is separate and self-contained. 

**The socket type**

* STREAM \(TCP\)
* DATAGRAM \(UDP\)
* RAW \(Used for raw access to the wire\)

The data transmission determines your layer 4 protocol.

If your message is "fire and forget", you do not need error handling, or the whole communication is a small request with a small response \(e.g. DNS lookup\), you will use UDP.

If you are transferring a large amount of data, you require error handling and correction, or you don't know what to choose, use TCP.

In python, the socket module defines the constants as **SOCK\_STREAM** and **SOCK\_DGRAM** respectively. They handle layers 1-4 for you. If you need to control the lower layers, you will use RAW sockets.

---

## Connections

### In order to connect to another host, you need to make a socket. To do so, you need to know 2 things:

* The type of address.
* The type of data being transmitted.

  ![](../assets/sock_con.PNG)

### Each endpoint of a network connection is always represented by a host and port

* In Python you write it out as a tuple \(host,port\)

\("www.python.org",80\)

\("205.172.13.4",443\)

* In almost all of the network programs you’ll write, you use this convention to specify a network address

## **The Address Family**

The type of address determines the "address family". We typically use IPv4 or IPv6, though many others exist. For IPv4 and IPv6 we will use the following constants:

* AF\_INET
* AF\_INET6

In Python, these are constants defined as part of the socket module.

---

---

## Client/Server Model

* Servers wait for incoming connections and provide a service \(e.g., web, mail, etc.\) 
* Clients make connections to servers
* Each endpoint is a running program

  ![](../assets/client_server.PNG)

## Request/Response Cycle

**Most network programs use a request/ response model based on messages** 

* Client sends a request message \(e.g., HTTP\)

### _GET /index.html HTTP/1.0_ 

* Server sends back a response message

### _HTTP/1.0 200 OK_ 

### _Content-type: text/html_ 

### _Content-length: 48823 &lt;HTML&gt;_

### _..._ 

* The exact format depends on the application
* Close the connection \(of course the server continues to listen for more clients\)

---
---

# Major System Calls

![](../assets/systemcalls.PNG)

![](../assets/calls.PNG)

A **CONNECTION** is a unique combination of:

* Client IP
* Client Port
* Server IP
* Server Port

If any one of these changes, it must be a different connection.

A connection is established between a TCP client and TCP server when the client uses **connect\(\)** to establish a connection to a TCP server listening. The result of that connection is the socket returned by **accept\(\)**.

---

---

## Socket Basics

**Reference:** [https://docs.python.org/2/library/socket.html?highlight=pton\#module-socket](https://docs.python.org/2/library/socket.html?highlight=pton#module-socket)

## **To create a socket:**

#### _import socket_ 

### _s = socket.socket\(addr\_family, type\)_

* **Address Families:**

### _socket.AF\_INET      Internet protocol \(IPv4\)_ 

### _socket.AF\_INET6     Internet protocol \(IPv6\)_ 

* **Socket Types:**

### _socket.SOCK\_STREAM  Connection based stream \(TCP\)_ 

### _socket.SOCK\_DGRAM   Datagrams \(UDP\)_

## Creating a socket is only the ﬁrst step

### _s = socket\(AF\_INET, SOCK\_STREAM\)_ 

**Further use depends on application** 

* **Server** - Listen for incoming connections 
* **Client** - Make an outgoing connection

**TCP Server code will deal with TWO \(or more\) sockets**

* The first socket is created by socket\(\) and is used to LISTEN
* The other sockets are returned by accept\(\) after a client connects to the listening socket. This new socket is used for data transfer to that specific client
* Accept\(\) returns two values, the socket, and the IP of the other side.

**Keep in mind**

* Calls to accept\(\) will NOT alter the LISTENING socket
* By default, accept\(\) will block until a connection is received

---
