## TCP Client/Server

![Differences between TCP and UDP connections](../assets/tcpvudp.PNG)

**Connection-oriented** 

* Two endpoints of a virtual circuit 

**Reliable** 

* Application needs no error checking 

**Stream-based** 

* No predefined block size 

**Processes identified by port numbers** 

**TCP Services live at specific ports**

---


## Order of Operations

## TCP Client Order of Operations

* **socket\(\)** – Get a socket descriptor
* **bind\(\)** – Specify SOURCE port \(Optional\)
* **connect\(\)** – Connect to destination IP/PORT
* **send\(\)/recv\(\)** – Data transfer
* **close\(\)** – Close the socket

## TCP Server Order of Operations

* **socket\(\)** – Get a socket descriptor
* **bind\(\)** – Specify SOURCE port to listen on \(Mandatory\)
* **listen\(\)** – Wait for a client to connect on the port
* **accept\(\)** – Returns a tuple containing the NEW socket to communicate with a single client, and a tuple of remote ip/port 
* **send/recv\(\)** – Data Transfer
* **close\(\)** – Close the socket

### Multiple Assignment:

`ret = sock.accept()`

`ret[0] # The socket`

`ret[1] # tuple of remote ip/port`

`accept_socket, remote = sock.accept`

---

## sendall\(\)

![](../assets/sendall1.PNG)

![](../assets/sendall2.PNG)

![](../assets/sendall3.PNG)


---


## Data Reassembly

![](../assets/chunk1.PNG)

![](../assets/chunk.PNG)

## If there is no more data being transmitted the _**while** \_loop will_ **break**\_**.**

---
