---

# Review

* Describe the different layers of OSI and TCP/IP Models
* Discuss String-based Protocol
* Perform socket programming in Python
* Write a TCP Client
* Write a TCP Server
* Write a UDP Sender
* Write a UDP Receiver
* Use the bind\(\) function in TCP/UDP
* Programmatically access web pages

---

#### OSI Model {#osi-model}

* OSI Layers

#### **BSD Socket API** {#bsd-socket-api}

* Sockets Types
* Connections
* Client/Server Model
* Major System Calls
* Socket Basics

#### UDP Client/Server

* Order of Operations

#### TCP Client/Server

* Order of Operations
* sendall\(\)
* Data Reassembly

---

**Complete Performance Lab:** 1

|[Sockets Lab 1](lab-1.md)|