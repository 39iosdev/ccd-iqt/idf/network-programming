# Challenge Labs

1. Create a programming script that tests if an html page is found or absent from the server.

2. Create a programming script to download and display the content of robots.txt for wikipedia.org.

3. Now refactor the program in exercise 2 so that it accepts a website as an argument. 

4. Create a programming script to extract h1 tag from example.com.

5. Create a programming script to extract and display all the image links from https://en.wikipedia.org/wiki/United_States_Air_Force.

6. Write a Python program to download and display the content of an rfc2616.txt for https://www.w3.org/Protocols/. 

7. Write a Python script to see if a selected page is found or not on the website. 

8. Write a Python script to extract and display all the header tags from https://www.howtogeek.com/. 

9. Write a Python script to extract and display all the image links from https://en.wikipedia.org/wiki/George_S._Patton. 

10. Using Python BeautifulSoup: Find all the link tags and list the first ten from the webpage python.org 

11. Using Python BeautifulSoup: Print content of elements that contain a specified string of a given web page.

12. Write a Python program to print content of elements that contain a specified string of a given web page. 

13. Write a Python program to find all the link tags and list the first ten from the webpage python.org. 


